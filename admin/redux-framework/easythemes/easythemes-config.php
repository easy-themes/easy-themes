<?php

/**
 * ReduxFramework Sample Config File
 */

if ( ! class_exists( 'Easy_Themes_Config' ) ) {

	class Easy_Themes_Config {

		public $args        = array();
		public $sections    = array();
		public $theme;
		public $ReduxFramework;

		public function __construct() {

			if ( ! class_exists( 'ReduxFramework' ) ) {
				return;
			}
				// This is needed. Bah WordPress bugs.  ;)
			if ( true == Redux_Helpers::isTheme( __FILE__ ) ) {
				$this->initSettings();
			} else {
				add_action( 'plugins_loaded', array( $this, 'initSettings' ), 0 );
			}

		}

		public function initSettings()
		{

			// Just for demo purposes. Not needed per say.
			$this->theme = wp_get_theme();

			// Set the default arguments
			$this->setArguments();

			// Set a few help tabs so you can see how it's done
			$this->setHelpTabs();

			// Create the sections and fields
			$this->setSections();

			if ( ! isset( $this->args['opt_name'] ) ) { // No errors please
					return;
			}

			// If Redux is running as a plugin, this will remove the demo notice and links
			add_action( 'redux/loaded', array( $this, 'remove_demo' ) );

			// Function to test the compiler hook and demo CSS output.
			// Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
			//add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 3);

			// Change the arguments after they've been declared, but before the panel is created
			// add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );

			// Change the default value of a field after it's been set, but before it's been useds
			// add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );

			// Dynamically add a section. Can be also used to modify sections/fields
			// add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));

			$this->ReduxFramework = new ReduxFramework( $this->sections, $this->args );
		}

		/**
		 * This is a test function that will let you see when the compiler hook occurs.
		 * It only runs if a field	set with compiler=>true is changed.
		 * */

		function compiler_action( $options, $css, $changed_values ) {
				//print_r($options); //Option values
				//print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )

				/*
					// Demo of how to use the dynamic CSS and write your own static CSS file
					$filename = dirname(__FILE__) . '/style' . '.css';
					global $wp_filesystem;
					if( empty( $wp_filesystem ) ) {
						require_once( ABSPATH .'/wp-admin/includes/file.php' );
					WP_Filesystem();
					}

					if( $wp_filesystem ) {
						$wp_filesystem->put_contents(
								$filename,
								$css,
								FS_CHMOD_FILE // predefined mode settings for WP files
						);
					}
				 */
		}

		/**

			Custom function for filtering the sections array. Good for child themes to override or add to the sections.
			Simply include this function in the child themes functions.php file.

			NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
			so you must use get_template_directory_uri() if you want to use any of the built in icons

		 * */
		function dynamic_section( $sections ) {
				//$sections = array();
				$sections[] = array(
						'title' => __('Section via hook', 'easythemes'),
						'desc' => __('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'easythemes'),
						'icon' => 'el-icon-paper-clip',
						// Leave this as a blank section, no options just some intro text set above.
						'fields' => array()
				);

				return $sections;
		}

		/**

			Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.

		 * */
		function change_arguments($args) {
				//$args['dev_mode'] = true;
				return $args;
		}

		/**

			Filter hook for filtering the default value of any given field. Very useful in development mode.

		 * */
		function change_defaults($defaults) {
				// $defaults['str_replace'] = 'Testing filter hook!';

				return $defaults;
		}

		// Remove the demo link and the notice of integrated demo from the redux-framework plugin
		function remove_demo() {

				// Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
				if (class_exists('ReduxFrameworkPlugin')) {
						remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

						// Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
						remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
				}
		}

		public function setSections() {

				ob_start();

				$ct             = wp_get_theme();
				$this->theme    = $ct;
				$item_name      = $this->theme->get('Name');
				$tags           = $this->theme->Tags;
				$screenshot     = $this->theme->get_screenshot();
				$class          = $screenshot ? 'has-screenshot' : '';

				$this->sections[] = array(
						'title'     => __('使い方', 'easythemes'),
						'desc'      => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'easythemes'),
						'icon'      => 'el-icon-home',
						// 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
						'fields'    => array(

								array(
										'id'        => 'welcome',
										'type'      => '',
										'title'     => __('', 'easythemes'),
										'compiler'  => 'true',
										'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
										'desc'      => '<div class="welcomebox"><p>この度は当wordpress用テーマをご購入いただきありがとうございます。<br />
				            このテーマでは、主に「<a href="'.get_bloginfo('url').'/wp-admin/themes.php?page=optionsframework">テーマオプション</a>」,「<a href="'.get_bloginfo('url').'/wp-admin/widgets.php">ウィジェット</a>」を使用してのサイト構築を行います。<br />
				            </p>
				            </div>',
										'subtitle'  => __('Upload any media using the WordPress native uploader', 'easythemes'),
										'hint'      => array(
												//'title'     => '',
												'content'   => 'This is a <b>hint</b> tool-tip for the webFonts field.<br/><br/>Add any HTML based text you like here.',
										)
								),
						)
				);
				$this->sections[] = array(
						'type' => 'divide',
				);

				// ACTUAL DECLARATION OF SECTIONS
				$this->sections[] = array(
						'title'     => __('Home Settings', 'easythemes'),
						'desc'      => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'easythemes'),
						'icon'      => 'el-icon-home',
						// 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
						'fields'    => array(

								array(
										'id'        => 'opt-web-fonts',
										'type'      => 'media',
										'title'     => __('Web Fonts', 'easythemes'),
										'compiler'  => 'true',
										'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
										'desc'      => __('Basic media uploader with disabled URL input field.', 'easythemes'),
										'subtitle'  => __('Upload any media using the WordPress native uploader', 'easythemes'),
										'hint'      => array(
												//'title'     => '',
												'content'   => 'This is a <b>hint</b> tool-tip for the webFonts field.<br/><br/>Add any HTML based text you like here.',
										)
								),
								array(
										'id'        => 'section-media-checkbox',
										'type'      => 'switch',
										'title'     => __('Section Show', 'easythemes'),
										'subtitle'  => __('With the "section" field you can create indent option sections.', 'easythemes'),

								),
								array(
										'id'        => 'section-media-start',
										'type'      => 'section',
										'title'     => __('Media Options', 'easythemes'),
										'subtitle'  => __('With the "section" field you can create indent option sections.', 'easythemes'),
										'indent'    => true, // Indent all options below until the next 'section' option is set.
										'required'  => array('section-media-checkbox', "=", 1),
								),
								array(
										'id'        => 'opt-media',
										'type'      => 'media',
										'url'       => true,
										'title'     => __('Media w/ URL', 'easythemes'),
										'compiler'  => 'true',
										//'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
										'desc'      => __('Basic media uploader with disabled URL input field.', 'easythemes'),
										'subtitle'  => __('Upload any media using the WordPress native uploader', 'easythemes'),
										'default'   => array('url' => 'http://s.wordpress.org/style/images/codeispoetry.png'),
										//'hint'      => array(
										//    'title'     => 'Hint Title',
										//    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
										//)
								),
								array(
										'id'        => 'section-media-end',
										'type'      => 'section',
										'indent'    => false, // Indent all options below until the next 'section' option is set.
										'required'  => array('section-media-checkbox', "=", 1),
								),
								array(
										'id'        => 'media-no-url',
										'type'      => 'media',
										'title'     => __('Media w/o URL', 'easythemes'),
										'desc'      => __('This represents the minimalistic view. It does not have the preview box or the display URL in an input box. ', 'easythemes'),
										'subtitle'  => __('Upload any media using the WordPress native uploader', 'easythemes'),
								),
								array(
										'id'        => 'media-no-preview',
										'type'      => 'media',
										'preview'   => false,
										'title'     => __('Media No Preview', 'easythemes'),
										'desc'      => __('This represents the minimalistic view. It does not have the preview box or the display URL in an input box. ', 'easythemes'),
										'subtitle'  => __('Upload any media using the WordPress native uploader', 'easythemes'),
								),
								array(
										'id'        => 'opt-gallery',
										'type'      => 'gallery',
										'title'     => __('Add/Edit Gallery', 'so-panels'),
										'subtitle'  => __('Create a new Gallery by selecting existing or uploading new images using the WordPress native uploader', 'so-panels'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
								),
								array(
										'id'            => 'opt-slider-label',
										'type'          => 'slider',
										'title'         => __('Slider Example 1', 'easythemes'),
										'subtitle'      => __('This slider displays the value as a label.', 'easythemes'),
										'desc'          => __('Slider description. Min: 1, max: 500, step: 1, default value: 250', 'easythemes'),
										'default'       => 250,
										'min'           => 1,
										'step'          => 1,
										'max'           => 500,
										'display_value' => 'label'
								),
								array(
										'id'            => 'opt-slider-text',
										'type'          => 'slider',
										'title'         => __('Slider Example 2 with Steps (5)', 'easythemes'),
										'subtitle'      => __('This example displays the value in a text box', 'easythemes'),
										'desc'          => __('Slider description. Min: 0, max: 300, step: 5, default value: 75', 'easythemes'),
										'default'       => 75,
										'min'           => 0,
										'step'          => 5,
										'max'           => 300,
										'display_value' => 'text'
								),
								array(
										'id'            => 'opt-slider-select',
										'type'          => 'slider',
										'title'         => __('Slider Example 3 with two sliders', 'easythemes'),
										'subtitle'      => __('This example displays the values in select boxes', 'easythemes'),
										'desc'          => __('Slider description. Min: 0, max: 500, step: 5, slider 1 default value: 100, slider 2 default value: 300', 'easythemes'),
										'default'       => array(
												1 => 100,
												2 => 300,
										),
										'min'           => 0,
										'step'          => 5,
										'max'           => '500',
										'display_value' => 'select',
										'handles'       => 2,
								),
								array(
										'id'            => 'opt-slider-float',
										'type'          => 'slider',
										'title'         => __('Slider Example 4 with float values', 'easythemes'),
										'subtitle'      => __('This example displays float values', 'easythemes'),
										'desc'          => __('Slider description. Min: 0, max: 1, step: .1, default value: .5', 'easythemes'),
										'default'       => .5,
										'min'           => 0,
										'step'          => .1,
										'max'           => 1,
										'resolution'    => 0.1,
										'display_value' => 'text'
								),
								array(
										'id'        => 'opt-spinner',
										'type'      => 'spinner',
										'title'     => __('JQuery UI Spinner Example 1', 'easythemes'),
										'desc'      => __('JQuery UI spinner description. Min:20, max: 100, step:20, default value: 40', 'easythemes'),
										'default'   => '40',
										'min'       => '20',
										'step'      => '20',
										'max'       => '100',
								),
								array(
										'id'        => 'switch-on',
										'type'      => 'switch',
										'title'     => __('Switch On', 'easythemes'),
										'subtitle'  => __('Look, it\'s on!', 'easythemes'),
										'default'   => true,
								),
								array(
										'id'        => 'switch-off',
										'type'      => 'switch',
										'title'     => __('Switch Off', 'easythemes'),
										'subtitle'  => __('Look, it\'s on!', 'easythemes'),
										//'options' => array('on', 'off'),
										'default'   => false,
								),
								array(
										'id'        => 'switch-parent',
										'type'      => 'switch',
										'title'     => __('Switch - Nested Children, Enable to show', 'easythemes'),
										'subtitle'  => __('Look, it\'s on! Also hidden child elements!', 'easythemes'),
										'default'   => 0,
										'on'        => 'Enabled',
										'off'       => 'Disabled',
								),
								array(
										'id'        => 'switch-child1',
										'type'      => 'switch',
										'required'  => array('switch-parent', '=', '1'),
										'title'     => __('Switch - This and the next switch required for patterns to show', 'easythemes'),
										'subtitle'  => __('Also called a "fold" parent.', 'easythemes'),
										'desc'      => __('Items set with a fold to this ID will hide unless this is set to the appropriate value.', 'easythemes'),
										'default'   => false,
								),
								array(
										'id'        => 'switch-child2',
										'type'      => 'switch',
										'required'  => array('switch-parent', '=', '1'),
										'title'     => __('Switch2 - Enable the above switch and this one for patterns to show', 'easythemes'),
										'subtitle'  => __('Also called a "fold" parent.', 'easythemes'),
										'desc'      => __('Items set with a fold to this ID will hide unless this is set to the appropriate value.', 'easythemes'),
										'default'   => false,
								),
								array(
										'id'        => 'opt-patterns',
										'type'      => 'image_select',
										'tiles'     => true,
										'required'  => array(
																				array('switch-child1', 'equals', 1),
																				array('switch-child2', 'equals', 1),
																	 ),
										'title'     => __('Images Option (with pattern=>true)', 'easythemes'),
										'subtitle'  => __('Select a background pattern.', 'easythemes'),
										'default'   => 0,
										'options'   => $sample_patterns
								,
								),
								array(
										'id'        => 'opt-homepage-layout',
										'type'      => 'sorter',
										'title'     => 'Layout Manager Advanced',
										'subtitle'  => 'You can add multiple drop areas or columns.',
										'compiler'  => 'true',
										'options'   => array(
												'enabled'   => array(
														'highlights'    => 'Highlights',
														'slider'        => 'Slider',
														'staticpage'    => 'Static Page',
														'services'      => 'Services'
												),
												'disabled'  => array(
												),
												'backup'    => array(
												),
										),
										'limits' => array(
												'disabled'  => 1,
												'backup'    => 2,
										),
								),

								array(
										'id'        => 'opt-homepage-layout-2',
										'type'      => 'sorter',
										'title'     => 'Homepage Layout Manager',
										'desc'      => 'Organize how you want the layout to appear on the homepage',
										'compiler'  => 'true',
										'options'   => array(
												'disabled'  => array(
														'highlights'    => 'Highlights',
														'slider'        => 'Slider',
												),
												'enabled'   => array(
														'staticpage'    => 'Static Page',
														'services'      => 'Services'
												),
										),
								),
								array(
										'id'        => 'opt-slides',
										'type'      => 'slides',
										'title'     => __('Slides Options', 'easythemes'),
										'subtitle'  => __('Unlimited slides with drag and drop sortings.', 'easythemes'),
										'desc'      => __('This field will store all slides values into a multidimensional array to use into a foreach loop.', 'easythemes'),
										'placeholder'   => array(
												'title'         => __('This is a title', 'easythemes'),
												'description'   => __('Description Here', 'easythemes'),
												'url'           => __('Give us a link!', 'easythemes'),
										),
								),
								array(
										'id'        => 'opt-presets',
										'type'      => 'image_select',
										'presets'   => true,
										'title'     => __('Preset', 'easythemes'),
										'subtitle'  => __('This allows you to set a json string or array to override multiple preferences in your theme.', 'easythemes'),
										'default'   => 0,
										'desc'      => __('This allows you to set a json string or array to override multiple preferences in your theme.', 'easythemes'),
										'options'   => array(
												'1'         => array('alt' => 'Preset 1', 'img' => ReduxFramework::$_url . '../sample/presets/preset1.png', 'presets' => array('switch-on' => 1, 'switch-off' => 1, 'switch-parent' => 1)),
												'2'         => array('alt' => 'Preset 2', 'img' => ReduxFramework::$_url . '../sample/presets/preset2.png', 'presets' => '{"opt-slider-label":"1", "opt-slider-text":"10"}'),
										),
								),
								array(
										'id'            => 'opt-typography',
										'type'          => 'typography',
										'title'         => __('Typography', 'easythemes'),
										//'compiler'      => true,  // Use if you want to hook in your own CSS compiler
										'google'        => true,    // Disable google fonts. Won't work if you haven't defined your google api key
										'font-backup'   => true,    // Select a backup non-google font in addition to a google font
										//'font-style'    => false, // Includes font-style and weight. Can use font-style or font-weight to declare
										//'subsets'       => false, // Only appears if google is true and subsets not set to false
										//'font-size'     => false,
										//'line-height'   => false,
										//'word-spacing'  => true,  // Defaults to false
										//'letter-spacing'=> true,  // Defaults to false
										//'color'         => false,
										//'preview'       => false, // Disable the previewer
										'all_styles'    => true,    // Enable all Google Font style/weight variations to be added to the page
										'output'        => array('h2.site-description'), // An array of CSS selectors to apply this font style to dynamically
										'compiler'      => array('h2.site-description-compiler'), // An array of CSS selectors to apply this font style to dynamically
										'units'         => 'px', // Defaults to px
										'subtitle'      => __('Typography option with each property can be called individually.', 'easythemes'),
										'default'       => array(
												'color'         => '#333',
												'font-style'    => '700',
												'font-family'   => 'Abel',
												'google'        => true,
												'font-size'     => '33px',
												'line-height'   => '40px'),
								),
						),
				);

				$this->sections[] = array(
						'type' => 'divide',
				);

				$this->sections[] = array(
						'icon'      => 'el-icon-cogs',
						'title'     => __('General Settings', 'easythemes'),
						'fields'    => array(
								array(
										'id'        => 'opt-layout',
										'type'      => 'image_select',
										'compiler'  => true,
										'title'     => __('Main Layout', 'easythemes'),
										'subtitle'  => __('Select main content and sidebar alignment. Choose between 1, 2 or 3 column layout.', 'easythemes'),
										'options'   => array(
												'1' => array('alt' => '1 Column',       'img' => ReduxFramework::$_url . 'assets/img/1col.png'),
												'2' => array('alt' => '2 Column Left',  'img' => ReduxFramework::$_url . 'assets/img/2cl.png'),
												'3' => array('alt' => '2 Column Right', 'img' => ReduxFramework::$_url . 'assets/img/2cr.png'),
												'4' => array('alt' => '3 Column Middle','img' => ReduxFramework::$_url . 'assets/img/3cm.png'),
												'5' => array('alt' => '3 Column Left',  'img' => ReduxFramework::$_url . 'assets/img/3cl.png'),
												'6' => array('alt' => '3 Column Right', 'img' => ReduxFramework::$_url . 'assets/img/3cr.png')
										),
										'default'   => '2'
								),
								array(
										'id'        => 'opt-textarea',
										'type'      => 'textarea',
										'required'  => array('layout', 'equals', '1'),
										'title'     => __('Tracking Code', 'easythemes'),
										'subtitle'  => __('Paste your Google Analytics (or other) tracking code here. This will be added into the footer template of your theme.', 'easythemes'),
										'validate'  => 'js',
										'desc'      => 'Validate that it\'s javascript!',
								),
								array(
										'id'        => 'opt-ace-editor-css',
										'type'      => 'ace_editor',
										'title'     => __('CSS Code', 'easythemes'),
										'subtitle'  => __('Paste your CSS code here.', 'easythemes'),
										'mode'      => 'css',
										'theme'     => 'monokai',
										'desc'      => 'Possible modes can be found at <a href="http://ace.c9.io" target="_blank">http://ace.c9.io/</a>.',
										'default'   => "#header{\nmargin: 0 auto;\n}"
								),
								/*
								array(
										'id'        => 'opt-ace-editor-js',
										'type'      => 'ace_editor',
										'title'     => __('JS Code', 'easythemes'),
										'subtitle'  => __('Paste your JS code here.', 'easythemes'),
										'mode'      => 'javascript',
										'theme'     => 'chrome',
										'desc'      => 'Possible modes can be found at <a href="http://ace.c9.io" target="_blank">http://ace.c9.io/</a>.',
										'default'   => "jQuery(document).ready(function(){\n\n});"
								),
								array(
										'id'        => 'opt-ace-editor-php',
										'type'      => 'ace_editor',
										'title'     => __('PHP Code', 'easythemes'),
										'subtitle'  => __('Paste your PHP code here.', 'easythemes'),
										'mode'      => 'php',
										'theme'     => 'chrome',
										'desc'      => 'Possible modes can be found at <a href="http://ace.c9.io" target="_blank">http://ace.c9.io/</a>.',
										'default'   => '<?php\nisset ( $redux ) ? true : false;\n?>'
								),
								*/
								array(
										'id'        => 'opt-editor',
										'type'      => 'editor',
										'title'     => __('Footer Text', 'easythemes'),
										'subtitle'  => __('You can use the following shortcodes in your footer text: [wp-url] [site-url] [theme-url] [login-url] [logout-url] [site-title] [site-tagline] [current-year]', 'easythemes'),
										'default'   => 'Powered by Redux Framework.',
								),
								array(
										'id'        => 'password',
										'type'      => 'password',
										'username'  => true,
										'title'     => 'SMTP Account',
										//'placeholder' => array('username' => 'Enter your Username')
								)
						)
				);

				$this->sections[] = array(
						'icon'      => 'el-icon-website',
						'title'     => __('Styling Options', 'easythemes'),
						'subsection' => true,
						'fields'    => array(
								array(
										'id'        => 'opt-select-stylesheet',
										'type'      => 'select',
										'title'     => __('Theme Stylesheet', 'easythemes'),
										'subtitle'  => __('Select your themes alternative color scheme.', 'easythemes'),
										'options'   => array('default.css' => 'default.css', 'color1.css' => 'color1.css'),
										'default'   => 'default.css',
								),
								array(
										'id'        => 'opt-color-background',
										'type'      => 'color',
										'output'    => array('.site-title'),
										'title'     => __('Body Background Color', 'easythemes'),
										'subtitle'  => __('Pick a background color for the theme (default: #fff).', 'easythemes'),
										'default'   => '#FFFFFF',
										'validate'  => 'color',
								),
								array(
										'id'        => 'opt-background',
										'type'      => 'background',
										'output'    => array('body'),
										'title'     => __('Body Background', 'easythemes'),
										'subtitle'  => __('Body background with image, color, etc.', 'easythemes'),
										//'default'   => '#FFFFFF',
								),
								array(
										'id'        => 'opt-color-footer',
										'type'      => 'color',
										'title'     => __('Footer Background Color', 'easythemes'),
										'subtitle'  => __('Pick a background color for the footer (default: #dd9933).', 'easythemes'),
										'default'   => '#dd9933',
										'validate'  => 'color',
								),
								array(
										'id'        => 'opt-color-rgba',
										'type'      => 'color_rgba',
										'title'     => __('Color RGBA - BETA', 'easythemes'),
										'subtitle'  => __('Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'easythemes'),
										'default'   => array('color' => '#dd9933', 'alpha' => '1.0'),
										'output'    => array('body'),
										'mode'      => 'background',
										'validate'  => 'colorrgba',
								),
								array(
										'id'        => 'opt-color-header',
										'type'      => 'color_gradient',
										'title'     => __('Header Gradient Color Option', 'easythemes'),
										'subtitle'  => __('Only color validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'default'   => array(
												'from'      => '#1e73be',
												'to'        => '#00897e'
										)
								),
								array(
										'id'        => 'opt-link-color',
										'type'      => 'link_color',
										'title'     => __('Links Color Option', 'easythemes'),
										'subtitle'  => __('Only color validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										//'regular'   => false, // Disable Regular Color
										//'hover'     => false, // Disable Hover Color
										//'active'    => false, // Disable Active Color
										//'visited'   => true,  // Enable Visited Color
										'default'   => array(
												'regular'   => '#aaa',
												'hover'     => '#bbb',
												'active'    => '#ccc',
										)
								),
								array(
										'id'        => 'opt-header-border',
										'type'      => 'border',
										'title'     => __('Header Border Option', 'easythemes'),
										'subtitle'  => __('Only color validation can be done on this field type', 'easythemes'),
										'output'    => array('.site-header'), // An array of CSS selectors to apply this font style to
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'default'   => array(
												'border-color'  => '#1e73be',
												'border-style'  => 'solid',
												'border-top'    => '3px',
												'border-right'  => '3px',
												'border-bottom' => '3px',
												'border-left'   => '3px'
										)
								),
								array(
										'id'            => 'opt-spacing',
										'type'          => 'spacing',
										'output'        => array('.site-header'), // An array of CSS selectors to apply this font style to
										'mode'          => 'margin',    // absolute, padding, margin, defaults to padding
										'all'           => true,        // Have one field that applies to all
										//'top'           => false,     // Disable the top
										//'right'         => false,     // Disable the right
										//'bottom'        => false,     // Disable the bottom
										//'left'          => false,     // Disable the left
										//'units'         => 'em',      // You can specify a unit value. Possible: px, em, %
										//'units_extended'=> 'true',    // Allow users to select any type of unit
										//'display_units' => 'false',   // Set to false to hide the units if the units are specified
										'title'         => __('Padding/Margin Option', 'easythemes'),
										'subtitle'      => __('Allow your users to choose the spacing or margin they want.', 'easythemes'),
										'desc'          => __('You can enable or disable any piece of this field. Top, Right, Bottom, Left, or Units.', 'easythemes'),
										'default'       => array(
												'margin-top'    => '1px',
												'margin-right'  => '2px',
												'margin-bottom' => '3px',
												'margin-left'   => '4px'
										)
								),
								array(
										'id'                => 'opt-dimensions',
										'type'              => 'dimensions',
										'units'             => 'em',    // You can specify a unit value. Possible: px, em, %
										'units_extended'    => 'true',  // Allow users to select any type of unit
										'title'             => __('Dimensions (Width/Height) Option', 'easythemes'),
										'subtitle'          => __('Allow your users to choose width, height, and/or unit.', 'easythemes'),
										'desc'              => __('You can enable or disable any piece of this field. Width, Height, or Units.', 'easythemes'),
										'default'           => array(
												'width'     => 200,
												'height'    => 100,
										)
								),
								array(
										'id'        => 'opt-typography-body',
										'type'      => 'typography',
										'title'     => __('Body Font', 'easythemes'),
										'subtitle'  => __('Specify the body font properties.', 'easythemes'),
										'google'    => true,
										'default'   => array(
												'color'         => '#dd9933',
												'font-size'     => '30px',
												'font-family'   => 'Arial,Helvetica,sans-serif',
												'font-weight'   => 'Normal',
										),
								),
								array(
										'id'        => 'opt-custom-css',
										'type'      => 'textarea',
										'title'     => __('Custom CSS', 'easythemes'),
										'subtitle'  => __('Quickly add some CSS to your theme by adding it to this block.', 'easythemes'),
										'desc'      => __('This field is even CSS validated!', 'easythemes'),
										'validate'  => 'css',
								),
								array(
										'id'        => 'opt-custom-html',
										'type'      => 'textarea',
										'title'     => __('Custom HTML', 'easythemes'),
										'subtitle'  => __('Just like a text box widget.', 'easythemes'),
										'desc'      => __('This field is even HTML validated!', 'easythemes'),
										'validate'  => 'html',
								),
						)
				);

				/**
				 *  Note here I used a 'heading' in the sections array construct
				 *  This allows you to use a different title on your options page
				 * instead of reusing the 'title' value.  This can be done on any
				 * section - kp
				 */
				$this->sections[] = array(
						'icon'      => 'el-icon-bullhorn',
						'title'     => __('Field Validation', 'easythemes'),
						'heading'   => __('Validate ALL fields within Redux.', 'easythemes'),
						'desc'      => __('<p class="description">This is the Description. Again HTML is allowed2</p>', 'easythemes'),
						'fields'    => array(
								array(
										'id'        => 'opt-text-email',
										'type'      => 'text',
										'title'     => __('Text Option - Email Validated', 'easythemes'),
										'subtitle'  => __('This is a little space under the Field Title in the Options table, additional info is good in here.', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'validate'  => 'email',
										'msg'       => 'custom error message',
										'default'   => 'test@test.com',
//                        'text_hint' => array(
//                            'title'     => 'Valid Email Required!',
//                            'content'   => 'This field required a valid email address.'
//                        )
								),
								array(
										'id'        => 'opt-text-post-type',
										'type'      => 'text',
										'title'     => __('Text Option with Data Attributes', 'easythemes'),
										'subtitle'  => __('You can also pass an options array if you want. Set the default to whatever you like.', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'data'      => 'post_type',
								),
								array(
										'id'        => 'opt-multi-text',
										'type'      => 'multi_text',
										'title'     => __('Multi Text Option - Color Validated', 'easythemes'),
										'validate'  => 'color',
										'subtitle'  => __('If you enter an invalid color it will be removed. Try using the text "blue" as a color.  ;)', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes')
								),
								array(
										'id'        => 'opt-text-url',
										'type'      => 'text',
										'title'     => __('Text Option - URL Validated', 'easythemes'),
										'subtitle'  => __('This must be a URL.', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'validate'  => 'url',
										'default'   => 'http://reduxframework.com',
//                        'text_hint' => array(
//                            'title'     => '',
//                            'content'   => 'Please enter a valid <strong>URL</strong> in this field.'
//                        )
								),
								array(
										'id'        => 'opt-text-numeric',
										'type'      => 'text',
										'title'     => __('Text Option - Numeric Validated', 'easythemes'),
										'subtitle'  => __('This must be numeric.', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'validate'  => 'numeric',
										'default'   => '0',
								),
								array(
										'id'        => 'opt-text-comma-numeric',
										'type'      => 'text',
										'title'     => __('Text Option - Comma Numeric Validated', 'easythemes'),
										'subtitle'  => __('This must be a comma separated string of numerical values.', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'validate'  => 'comma_numeric',
										'default'   => '0',
								),
								array(
										'id'        => 'opt-text-no-special-chars',
										'type'      => 'text',
										'title'     => __('Text Option - No Special Chars Validated', 'easythemes'),
										'subtitle'  => __('This must be a alpha numeric only.', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'validate'  => 'no_special_chars',
										'default'   => '0'
								),
								array(
										'id'        => 'opt-text-str_replace',
										'type'      => 'text',
										'title'     => __('Text Option - Str Replace Validated', 'easythemes'),
										'subtitle'  => __('You decide.', 'easythemes'),
										'desc'      => __('This field\'s default value was changed by a filter hook!', 'easythemes'),
										'validate'  => 'str_replace',
										'str'       => array(
												'search'        => ' ',
												'replacement'   => 'thisisaspace'
										),
										'default'   => 'This is the default.'
								),
								array(
										'id'        => 'opt-text-preg_replace',
										'type'      => 'text',
										'title'     => __('Text Option - Preg Replace Validated', 'easythemes'),
										'subtitle'  => __('You decide.', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'validate'  => 'preg_replace',
										'preg'      => array(
												'pattern'       => '/[^a-zA-Z_ -]/s',
												'replacement'   => 'no numbers'
										 ),
										'default'   => '0'
								),
								array(
										'id'                => 'opt-text-custom_validate',
										'type'              => 'text',
										'title'             => __('Text Option - Custom Callback Validated', 'easythemes'),
										'subtitle'          => __('You decide.', 'easythemes'),
										'desc'              => __('This is the description field, again good for additional info.', 'easythemes'),
										'validate_callback' => 'redux_validate_callback_function',
										'default'           => '0'
								),
								array(
										'id'        => 'opt-textarea-no-html',
										'type'      => 'textarea',
										'title'     => __('Textarea Option - No HTML Validated', 'easythemes'),
										'subtitle'  => __('All HTML will be stripped', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'validate'  => 'no_html',
										'default'   => 'No HTML is allowed in here.'
								),
								array(
										'id'        => 'opt-textarea-html',
										'type'      => 'textarea',
										'title'     => __('Textarea Option - HTML Validated', 'easythemes'),
										'subtitle'  => __('HTML Allowed (wp_kses)', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'validate'  => 'html', //see http://codex.wordpress.org/Function_Reference/wp_kses_post
										'default'   => 'HTML is allowed in here.'
								),
								array(
										'id'            => 'opt-textarea-some-html',
										'type'          => 'textarea',
										'title'         => __('Textarea Option - HTML Validated Custom', 'easythemes'),
										'subtitle'      => __('Custom HTML Allowed (wp_kses)', 'easythemes'),
										'desc'          => __('This is the description field, again good for additional info.', 'easythemes'),
										'validate'      => 'html_custom',
										'default'       => '<p>Some HTML is allowed in here.</p>',
										'allowed_html'  => array('') //see http://codex.wordpress.org/Function_Reference/wp_kses
								),
								array(
										'id'        => 'opt-textarea-js',
										'type'      => 'textarea',
										'title'     => __('Textarea Option - JS Validated', 'easythemes'),
										'subtitle'  => __('JS will be escaped', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'validate'  => 'js'
								),
						)
				);

				$this->sections[] = array(
						'icon'      => 'el-icon-check',
						'title'     => __('Radio/Checkbox Fields', 'easythemes'),
						'desc'      => __('<p class="description">This is the Description. Again HTML is allowed</p>', 'easythemes'),
						'fields'    => array(
								array(
										'id'        => 'opt-checkbox',
										'type'      => 'checkbox',
										'title'     => __('Checkbox Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'default'   => '1'// 1 = on | 0 = off
								),
								array(
										'id'        => 'opt-multi-check',
										'type'      => 'checkbox',
										'title'     => __('Multi Checkbox Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),

										//Must provide key => value pairs for multi checkbox options
										'options'   => array(
												'1' => 'Opt 1',
												'2' => 'Opt 2',
												'3' => 'Opt 3'
										),

										//See how std has changed? you also don't need to specify opts that are 0.
										'default'   => array(
												'1' => '1',
												'2' => '0',
												'3' => '0'
										)
								),
								array(
										'id'        => 'opt-checkbox-data',
										'type'      => 'checkbox',
										'title'     => __('Multi Checkbox Option (with menu data)', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'data'      => 'menu'
								),
								array(
										'id'        => 'opt-checkbox-sidebar',
										'type'      => 'checkbox',
										'title'     => __('Multi Checkbox Option (with sidebar data)', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'data'      => 'sidebars'
								),
								array(
										'id'        => 'opt-radio',
										'type'      => 'radio',
										'title'     => __('Radio Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),

										 //Must provide key => value pairs for radio options
										'options'   => array(
												'1' => 'Opt 1',
												'2' => 'Opt 2',
												'3' => 'Opt 3'
										),
										'default'   => '2'
								),
								array(
										'id'        => 'opt-radio-data',
										'type'      => 'radio',
										'title'     => __('Multi Checkbox Option (with menu data)', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'data'      => 'menu'
								),
								array(
										'id'        => 'opt-image-select',
										'type'      => 'image_select',
										'title'     => __('Images Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),

										//Must provide key => value(array:title|img) pairs for radio options
										'options'   => array(
												'1' => array('title' => 'Opt 1', 'img' => 'images/align-none.png'),
												'2' => array('title' => 'Opt 2', 'img' => 'images/align-left.png'),
												'3' => array('title' => 'Opt 3', 'img' => 'images/align-center.png'),
												'4' => array('title' => 'Opt 4', 'img' => 'images/align-right.png')
										),
										'default'   => '2'
								),
								array(
										'id'        => 'opt-image-select-layout',
										'type'      => 'image_select',
										'title'     => __('Images Option for Layout', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This uses some of the built in images, you can use them for layout options.', 'easythemes'),

										//Must provide key => value(array:title|img) pairs for radio options
										'options'   => array(
												'1' => array('alt' => '1 Column',        'img' => ReduxFramework::$_url . 'assets/img/1col.png'),
												'2' => array('alt' => '2 Column Left',   'img' => ReduxFramework::$_url . 'assets/img/2cl.png'),
												'3' => array('alt' => '2 Column Right',  'img' => ReduxFramework::$_url . 'assets/img/2cr.png'),
												'4' => array('alt' => '3 Column Middle', 'img' => ReduxFramework::$_url . 'assets/img/3cm.png'),
												'5' => array('alt' => '3 Column Left',   'img' => ReduxFramework::$_url . 'assets/img/3cl.png'),
												'6' => array('alt' => '3 Column Right',  'img' => ReduxFramework::$_url . 'assets/img/3cr.png')
										),
										'default' => '2'
								),
								array(
										'id'        => 'opt-sortable',
										'type'      => 'sortable',
										'title'     => __('Sortable Text Option', 'easythemes'),
										'subtitle'  => __('Define and reorder these however you want.', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'options'   => array(
												'si1' => 'Item 1',
												'si2' => 'Item 2',
												'si3' => 'Item 3',
										)
								),
								array(
										'id'        => 'opt-check-sortable',
										'type'      => 'sortable',
										'mode'      => 'checkbox', // checkbox or text
										'title'     => __('Sortable Text Option', 'easythemes'),
										'subtitle'  => __('Define and reorder these however you want.', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'options'   => array(
												'si1' => false,
												'si2' => true,
												'si3' => false,
										)
								),
						)
				);

				$this->sections[] = array(
						'icon'      => 'el-icon-list-alt',
						'title'     => __('Select Fields', 'easythemes'),
						'desc'      => __('<p class="description">This is the Description. Again HTML is allowed</p>', 'easythemes'),
						'fields'    => array(
								array(
										'id'        => 'opt-select',
										'type'      => 'select',
										'title'     => __('Select Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),

										//Must provide key => value pairs for select options
										'options'   => array(
												'1' => 'Opt 1',
												'2' => 'Opt 2',
												'3' => 'Opt 3'
										),
										'default'   => '2'
								),
								array(
										'id'        => 'opt-multi-select',
										'type'      => 'select',
										'multi'     => true,
										'title'     => __('Multi Select Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),

										//Must provide key => value pairs for radio options
										'options'   => array(
												'1' => 'Opt 1',
												'2' => 'Opt 2',
												'3' => 'Opt 3'
										),
										'required'  => array('select', 'equals', array('1', '3')),
										'default'   => array('2', '3')
								),
								array(
										'id'        => 'opt-select-image',
										'type'      => 'select_image',
										'title'     => __('Select Image', 'easythemes'),
										'subtitle'  => __('A preview of the selected image will appear underneath the select box.', 'easythemes'),
										'options'   => $sample_patterns,
										// Alternatively
										//'options'   => Array(
										//                'img_name' => 'img_path'
										//             )
										'default' => 'tree_bark.png',
								),
								array(
										'id'    => 'opt-info',
										'type'  => 'info',
										'desc'  => __('You can easily add a variety of data from WordPress.', 'easythemes'),
								),
								array(
										'id'        => 'opt-select-categories',
										'type'      => 'select',
										'data'      => 'categories',
										'title'     => __('Categories Select Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
								),
								array(
										'id'        => 'opt-select-categories-multi',
										'type'      => 'select',
										'data'      => 'categories',
										'multi'     => true,
										'title'     => __('Categories Multi Select Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
								),
								array(
										'id'        => 'opt-select-pages',
										'type'      => 'select',
										'data'      => 'pages',
										'title'     => __('Pages Select Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
								),
								array(
										'id'        => 'opt-multi-select-pages',
										'type'      => 'select',
										'data'      => 'pages',
										'multi'     => true,
										'title'     => __('Pages Multi Select Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
								),
								array(
										'id'        => 'opt-select-tags',
										'type'      => 'select',
										'data'      => 'tags',
										'title'     => __('Tags Select Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
								),
								array(
										'id'        => 'opt-multi-select-tags',
										'type'      => 'select',
										'data'      => 'tags',
										'multi'     => true,
										'title'     => __('Tags Multi Select Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
								),
								array(
										'id'        => 'opt-select-menus',
										'type'      => 'select',
										'data'      => 'menus',
										'title'     => __('Menus Select Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
								),
								array(
										'id'        => 'opt-multi-select-menus',
										'type'      => 'select',
										'data'      => 'menu',
										'multi'     => true,
										'title'     => __('Menus Multi Select Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
								),
								array(
										'id'        => 'opt-select-post-type',
										'type'      => 'select',
										'data'      => 'post_type',
										'title'     => __('Post Type Select Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
								),
								array(
										'id'        => 'opt-multi-select-post-type',
										'type'      => 'select',
										'data'      => 'post_type',
										'multi'     => true,
										'title'     => __('Post Type Multi Select Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
								),
								array(
										'id'        => 'opt-multi-select-sortable',
										'type'      => 'select',
										'data'      => 'post_type',
										'multi'     => true,
										'sortable'  => true,
										'title'     => __('Post Type Multi Select Option + Sortable', 'easythemes'),
										'subtitle'  => __('This field also has sortable enabled!', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
								),
								array(
										'id'        => 'opt-select-posts',
										'type'      => 'select',
										'data'      => 'post',
										'title'     => __('Posts Select Option2', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
								),
								array(
										'id'        => 'opt-multi-select-posts',
										'type'      => 'select',
										'data'      => 'post',
										'multi'     => true,
										'title'     => __('Posts Multi Select Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
								),
								array(
										'id'        => 'opt-select-roles',
										'type'      => 'select',
										'data'      => 'roles',
										'title'     => __('User Role Select Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
								),
								array(
										'id'        => 'opt-select-capabilities',
										'type'      => 'select',
										'data'      => 'capabilities',
										'multi'     => true,
										'title'     => __('Capabilities Select Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
								),
								array(
										'id'        => 'opt-select-elusive',
										'type'      => 'select',
										'data'      => 'elusive-icons',
										'title'     => __('Elusive Icons Select Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('Here\'s a list of all the elusive icons by name and icon.', 'easythemes'),
								),
						)
				);

				$theme_info  = '<div class="redux-framework-section-desc">';
				$theme_info .= '<p class="redux-framework-theme-data description theme-uri">' . __('<strong>Theme URL:</strong> ', 'easythemes') . '<a href="' . $this->theme->get('ThemeURI') . '" target="_blank">' . $this->theme->get('ThemeURI') . '</a></p>';
				$theme_info .= '<p class="redux-framework-theme-data description theme-author">' . __('<strong>Author:</strong> ', 'easythemes') . $this->theme->get('Author') . '</p>';
				$theme_info .= '<p class="redux-framework-theme-data description theme-version">' . __('<strong>Version:</strong> ', 'easythemes') . $this->theme->get('Version') . '</p>';
				$theme_info .= '<p class="redux-framework-theme-data description theme-description">' . $this->theme->get('Description') . '</p>';
				$tabs = $this->theme->get('Tags');
				if (!empty($tabs)) {
						$theme_info .= '<p class="redux-framework-theme-data description theme-tags">' . __('<strong>Tags:</strong> ', 'easythemes') . implode(', ', $tabs) . '</p>';
				}
				$theme_info .= '</div>';

				if (file_exists(dirname(__FILE__) . '/../README.md')) {
						$this->sections['theme_docs'] = array(
								'icon'      => 'el-icon-list-alt',
								'title'     => __('Documentation', 'easythemes'),
								'fields'    => array(
										array(
												'id'        => '17',
												'type'      => 'raw',
												'markdown'  => true,
												'content'   => file_get_contents(dirname(__FILE__) . '/../README.md')
										),
								),
						);
				}

				// You can append a new section at any time.
				$this->sections[] = array(
						'icon'      => 'el-icon-eye-open',
						'title'     => __('Additional Fields', 'easythemes'),
						'desc'      => __('<p class="description">This is the Description. Again HTML is allowed</p>', 'easythemes'),
						'fields'    => array(
								array(
										'id'        => 'opt-datepicker',
										'type'      => 'date',
										'title'     => __('Date Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes')
								),
								array(
										'id'    => 'opt-divide',
										'type'  => 'divide'
								),
								array(
										'id'        => 'opt-button-set',
										'type'      => 'button_set',
										'title'     => __('Button Set Option', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),

										//Must provide key => value pairs for radio options
										'options'   => array(
												'1' => 'Opt 1',
												'2' => 'Opt 2',
												'3' => 'Opt 3'
										),
										'default'   => '2'
								),
								array(
										'id'        => 'opt-button-set-multi',
										'type'      => 'button_set',
										'title'     => __('Button Set, Multi Select', 'easythemes'),
										'subtitle'  => __('No validation can be done on this field type', 'easythemes'),
										'desc'      => __('This is the description field, again good for additional info.', 'easythemes'),
										'multi'     => true,

										//Must provide key => value pairs for radio options
										'options'   => array(
												'1' => 'Opt 1',
												'2' => 'Opt 2',
												'3' => 'Opt 3'
										),
										'default'   => array('2', '3')
								),
								array(
										'id'        => 'opt-info-field',
										'type'      => 'info',
										'desc'      => __('This is the info field, if you want to break sections up.', 'easythemes')
								),
								array(
										'id'    => 'opt-info-warning',
										'type'  => 'info',
										'style' => 'warning',
										'title' => __('This is a title.', 'easythemes'),
										'desc'  => __('This is an info field with the warning style applied and a header.', 'easythemes')
								),
								array(
										'id'    => 'opt-info-success',
										'type'  => 'info',
										'style' => 'success',
										'icon'  => 'el-icon-info-sign',
										'title' => __('This is a title.', 'easythemes'),
										'desc'  => __('This is an info field with the success style applied, a header and an icon.', 'easythemes')
								),
								array(
										'id'    => 'opt-info-critical',
										'type'  => 'info',
										'style' => 'critical',
										'icon'  => 'el-icon-info-sign',
										'title' => __('This is a title.', 'easythemes'),
										'desc'  => __('This is an info field with the critical style applied, a header and an icon.', 'easythemes')
								),
								array(
										'id'        => 'opt-raw_info',
										'type'      => 'info',
										'required'  => array('18', 'equals', array('1', '2')),
										'raw_html'  => true,
										'desc'      => $sampleHTML,
								),
								array(
										'id'        => 'opt-info-normal',
										'type'      => 'info',
										'notice'    => true,
										'title'     => __('This is a title.', 'easythemes'),
										'desc'      => __('This is an info notice field with the normal style applied, a header and an icon.', 'easythemes')
								),
								array(
										'id'        => 'opt-notice-info',
										'type'      => 'info',
										'notice'    => true,
										'style'     => 'info',
										'title'     => __('This is a title.', 'easythemes'),
										'desc'      => __('This is an info notice field with the info style applied, a header and an icon.', 'easythemes')
								),
								array(
										'id'        => 'opt-notice-warning',
										'type'      => 'info',
										'notice'    => true,
										'style'     => 'warning',
										'icon'      => 'el-icon-info-sign',
										'title'     => __('This is a title.', 'easythemes'),
										'desc'      => __('This is an info notice field with the warning style applied, a header and an icon.', 'easythemes')
								),
								array(
										'id'        => 'opt-notice-success',
										'type'      => 'info',
										'notice'    => true,
										'style'     => 'success',
										'icon'      => 'el-icon-info-sign',
										'title'     => __('This is a title.', 'easythemes'),
										'desc'      => __('This is an info notice field with the success style applied, a header and an icon.', 'easythemes')
								),
								array(
										'id'        => 'opt-notice-critical',
										'type'      => 'info',
										'notice'    => true,
										'style'     => 'critical',
										'icon'      => 'el-icon-info-sign',
										'title'     => __('This is a title.', 'easythemes'),
										'desc'      => __('This is an notice field with the critical style applied, a header and an icon.', 'easythemes')
								),
								array(
										'id'        => 'opt-custom-callback',
										'type'      => 'callback',
										'title'     => __('Custom Field Callback', 'easythemes'),
										'subtitle'  => __('This is a completely unique field type', 'easythemes'),
										'desc'      => __('This is created with a callback function, so anything goes in this field. Make sure to define the function though.', 'easythemes'),
										'callback'  => 'redux_my_custom_field'
								),

								array(
										'id'        => 'opt-customizer-only-in-section',
										'type'      => 'select',
										'title'     => __('Customizer Only Option', 'easythemes'),
										'subtitle'  => __('The subtitle is NOT visible in customizer', 'easythemes'),
										'desc'      => __('The field desc is NOT visible in customizer.', 'easythemes'),
										'customizer_only'   => true,

										//Must provide key => value pairs for select options
										'options'   => array(
												'1' => 'Opt 1',
												'2' => 'Opt 2',
												'3' => 'Opt 3'
										),
										'default'   => '2'
								),
						)
				);

				$this->sections[] = array(
						'icon'              => 'el-icon-list-alt',
						'title'             => __('Customizer Only', 'easythemes'),
						'desc'              => __('<p class="description">This Section should be visible only in Customizer</p>', 'easythemes'),
						'customizer_only'   => true,
						'fields'    => array(
								array(
										'id'        => 'opt-customizer-only',
										'type'      => 'select',
										'title'     => __('Customizer Only Option', 'easythemes'),
										'subtitle'  => __('The subtitle is NOT visible in customizer', 'easythemes'),
										'desc'      => __('The field desc is NOT visible in customizer.', 'easythemes'),
										'customizer_only'   => true,

										//Must provide key => value pairs for select options
										'options'   => array(
												'1' => 'Opt 1',
												'2' => 'Opt 2',
												'3' => 'Opt 3'
										),
										'default'   => '2'
								),
						)
				);

				$this->sections[] = array(
						'title'     => __('Import / Export', 'easythemes'),
						'desc'      => __('Import and Export your Redux Framework settings from file, text or URL.', 'easythemes'),
						'icon'      => 'el-icon-refresh',
						'fields'    => array(
								array(
										'id'            => 'opt-import-export',
										'type'          => 'import_export',
										'title'         => 'Import Export',
										'subtitle'      => 'Save and restore your Redux options',
										'full_width'    => false,
								),
						),
				);

				$this->sections[] = array(
						'type' => 'divide',
				);

				$this->sections[] = array(
						'icon'      => 'el-icon-info-sign',
						'title'     => __('Theme Information', 'easythemes'),
						'desc'      => __('<p class="description">This is the Description. Again HTML is allowed</p>', 'easythemes'),
						'fields'    => array(
								array(
										'id'        => 'opt-raw-info',
										'type'      => 'raw',
										'content'   => $item_info,
								)
						),
				);

				if (file_exists(trailingslashit(dirname(__FILE__)) . 'README.html')) {
						$tabs['docs'] = array(
								'icon'      => 'el-icon-book',
								'title'     => __('Documentation', 'easythemes'),
								'content'   => nl2br(file_get_contents(trailingslashit(dirname(__FILE__)) . 'README.html'))
						);
				}
		}

		public function setHelpTabs() {

				// Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
				$this->args['help_tabs'][] = array(
						'id'        => 'redux-help-tab-1',
						'title'     => __('Theme Information 1', 'easythemes'),
						'content'   => __('<p>This is the tab content, HTML is allowed.</p>', 'easythemes')
				);

				$this->args['help_tabs'][] = array(
						'id'        => 'redux-help-tab-2',
						'title'     => __('Theme Information 2', 'easythemes'),
						'content'   => __('<p>This is the tab content, HTML is allowed.</p>', 'easythemes')
				);

				// Set the help sidebar
				$this->args['help_sidebar'] = __('<p>This is the sidebar content, HTML is allowed.</p>', 'easythemes');
		}

		/**

			All the possible arguments for Redux.
			For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments

		 * */
		public function setArguments() {

				$theme = wp_get_theme(); // For use with some settings. Not necessary.

				$this->args = array(
						// TYPICAL -> Change these values as you need/desire
						'opt_name'          => 'redux_demo',            // This is where your data is stored in the database and also becomes your global variable name.
						'display_name'      => $theme->get('Name'),     // Name that appears at the top of your panel
						'display_version'   => $theme->get('Version'),  // Version that appears at the top of your panel
						'menu_type'         => 'menu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
						'allow_sub_menu'    => true,                    // Show the sections below the admin menu item or not
						'menu_title'        => 'テーマ設定',
						'page_title'        => 'テーマ設定',

						// You will need to generate a Google API key to use this feature.
						// Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
						'google_api_key' => '', // Must be defined to add google fonts to the typography module

						'async_typography'  => false,                    // Use a asynchronous font on the front end or font string
						'admin_bar'         => true,                    // Show the panel pages on the admin bar
						'global_variable'   => '',                      // Set a different name for your global variable other than the opt_name
						'dev_mode'          => flase,                    // Show the time the page took to load, etc
						'customizer'        => true,                    // Enable basic customizer support
						//'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
						//'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

						// OPTIONAL -> Give you extra features
						'page_priority'     => null,                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
						'page_parent'       => 'themes.php',            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
						'page_permissions'  => 'manage_options',        // Permissions needed to access the options panel.
						'menu_icon'         => '',                      // Specify a custom URL to an icon
						'last_tab'          => '',                      // Force your panel to always open to a specific tab (by id)
						'page_icon'         => 'icon-themes',           // Icon displayed in the admin panel next to your menu_title
						'page_slug'         => '_options',              // Page slug used to denote the panel
						'save_defaults'     => true,                    // On load save the defaults to DB before user clicks save or not
						'default_show'      => false,                   // If true, shows the default value next to each field that is not the default value.
						'default_mark'      => '',                      // What to print by the field's title if the value shown is default. Suggested: *
						'show_import_export' => true,                   // Shows the Import/Export panel when not used as a field.

						// CAREFUL -> These options are for advanced use only
						'transient_time'    => 60 * MINUTE_IN_SECONDS,
						'output'            => true,                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
						'output_tag'        => true,                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
						// 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

						// FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
						'database'              => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
						'system_info'           => false, // REMOVE

						// HINTS
						'hints' => array(
								'icon'          => 'icon-question-sign',
								'icon_position' => 'right',
								'icon_color'    => 'lightgray',
								'icon_size'     => 'normal',
								'tip_style'     => array(
										'color'         => 'light',
										'shadow'        => true,
										'rounded'       => false,
										'style'         => '',
								),
								'tip_position'  => array(
										'my' => 'top left',
										'at' => 'bottom right',
								),
								'tip_effect'    => array(
										'show'          => array(
												'effect'        => 'slide',
												'duration'      => '500',
												'event'         => 'mouseover',
										),
										'hide'      => array(
												'effect'    => 'slide',
												'duration'  => '500',
												'event'     => 'click mouseleave',
										),
								),
						)
				);


				// SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
				$this->args['share_icons'][] = array(
						'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
						'title' => 'Visit us on GitHub',
						'icon'  => 'el-icon-github'
						//'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
				);
				$this->args['share_icons'][] = array(
						'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
						'title' => 'Like us on Facebook',
						'icon'  => 'el-icon-facebook'
				);
				$this->args['share_icons'][] = array(
						'url'   => 'http://twitter.com/reduxframework',
						'title' => 'Follow us on Twitter',
						'icon'  => 'el-icon-twitter'
				);
				$this->args['share_icons'][] = array(
						'url'   => 'http://www.linkedin.com/company/redux-framework',
						'title' => 'Find us on LinkedIn',
						'icon'  => 'el-icon-linkedin'
				);

				// Panel Intro text -> before the form
				if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false) {
						if (!empty($this->args['global_variable'])) {
								$v = $this->args['global_variable'];
						} else {
								$v = str_replace('-', '_', $this->args['opt_name']);
						}
						$this->args['intro_text'] = sprintf(__('', 'easythemes'), $v);
				}

				$this->args['intro_text'] = __('', 'easythemes');
				// // Add content after the form.
				$this->args['footer_text'] = __('', 'easythemes');
		}

	}

	global $reduxConfig;
	$reduxConfig = new Easy_Themes_Config();
}


/**
Custom function for the callback validation referenced above
* */
if (!function_exists('redux_validate_callback_function')):
function redux_validate_callback_function($field, $value, $existing_value) {
		$error = false;
		$value = 'just testing';

		/*
			do your validation

			if(something) {
				$value = $value;
			} elseif(something else) {
				$error = true;
				$value = $existing_value;
				$field['msg'] = 'your custom error message';
			}
		 */

		$return['value'] = $value;
		if ($error == true) {
				$return['error'] = $field;
		}
		return $return;
}
endif;
