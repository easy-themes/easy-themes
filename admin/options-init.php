<?php

/**
 * ReduxFramework Sample Config File
 */

if ( ! class_exists( 'Easy_Themes_Config' ) ) {

	class Easy_Themes_Config {

		public $args        = array();
		public $sections    = array();
		public $theme;
		public $ReduxFramework;

		public function __construct() {

			if ( ! class_exists( 'ReduxFramework' ) ) {
				return;
			}
				// This is needed. Bah WordPress bugs.  ;)
			if ( true == Redux_Helpers::isTheme( __FILE__ ) ) {
				$this->initSettings();
			} else {
				add_action( 'plugins_loaded', array( $this, 'initSettings' ), 0 );
			}

		}

		public function initSettings()
		{

			// Just for demo purposes. Not needed per say.
			$this->theme = wp_get_theme();

			// Set the default arguments
			$this->setArguments();

			// Set a few help tabs so you can see how it's done
			$this->setHelpTabs();

			// Create the sections and fields
			$this->setSections();

			if ( ! isset( $this->args['opt_name'] ) ) { // No errors please
					return;
			}

			// If Redux is running as a plugin, this will remove the demo notice and links
			add_action( 'redux/loaded', array( $this, 'remove_demo' ) );

			// Function to test the compiler hook and demo CSS output.
			// Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
			add_filter( 'redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'main_colmn_width_css' ), 10, 3 );

			// Change the arguments after they've been declared, but before the panel is created
			// add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );

			// Change the default value of a field after it's been set, but before it's been useds
			// add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );

			// Dynamically add a section. Can be also used to modify sections/fields
			// add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));

			$this->ReduxFramework = new ReduxFramework( $this->sections, $this->args );
		}

		public function main_colmn_width_css( $options, $css, $changed_values )
		{

			// echo '<h1>The compiler hook has run!</h1>';


		}

		/**
		 * This is a test function that will let you see when the compiler hook occurs.
		 * It only runs if a field	set with compiler=>true is changed.
		 * */

		function compiler_action( $options, $css, $changed_values ) {
				//print_r($options); //Option values
				//print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )

				/*
					// Demo of how to use the dynamic CSS and write your own static CSS file
					$filename = dirname(__FILE__) . '/style' . '.css';
					global $wp_filesystem;
					if( empty( $wp_filesystem ) ) {
						require_once( ABSPATH .'/wp-admin/includes/file.php' );
					WP_Filesystem();
					}

					if( $wp_filesystem ) {
						$wp_filesystem->put_contents(
								$filename,
								$css,
								FS_CHMOD_FILE // predefined mode settings for WP files
						);
					}
				 */
		}

		/**
		 *
		 * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
		 * Simply include this function in the child themes functions.php file.
		 *
		 * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
		 * so you must use get_template_directory_uri() if you want to use any of the built in icons
		 *
		 * */
		function dynamic_section( $sections ) {
				//$sections = array();
				$sections[] = array(
						'title' => __( 'Section via hook', 'easythemes' ),
						'desc' => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'easythemes' ),
						'icon' => 'el-icon-paper-clip',
						// Leave this as a blank section, no options just some intro text set above.
						'fields' => array()
				);

				return $sections;
		}

		/**
		 *
		 * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
		 * *
		 * */
		public function change_arguments( $args )
		{
				//$args['dev_mode'] = true;
				return $args;
		}

		/**
		 *
		 * Filter hook for filtering the default value of any given field. Very useful in development mode.
		 *
		 * */
		public function change_defaults( $defaults )
		{
				// $defaults['str_replace'] = 'Testing filter hook!';
				return $defaults;
		}

		// Remove the demo link and the notice of integrated demo from the redux-framework plugin
		function remove_demo() {

			// Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
			if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
				remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::instance(), 'plugin_metalinks' ), null, 2 );
				// Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
				remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
			}
		}

		public function setSections()
		{

				ob_start();

				$ct             = wp_get_theme();
				$this->theme    = $ct;
				$item_name      = $this->theme->get( 'Name' );
				$tags           = $this->theme->Tags;
				$screenshot     = $this->theme->get_screenshot();
				$class          = $screenshot ? 'has-screenshot' : '';

				/**
				 * ==========================================
				 * 0. Getting Started.
				 * ==========================================
				 */
				// $this->sections[] = array(
				// 		'title'     => __( '使い方', 'easythemes' ),
				// 		'desc'      => __( '<iframe src="' . get_template_directory_uri() . '/readme.php" frameborder="0" width="800" height="2000"></iframe>', 'easythemes' ),
				// 		'icon'      => 'el-icon-home',
				// );

				/**
				 * ==========================================
				 * 1. General
				 * ==========================================
				 */
				$this->sections[] = array(
						'id'        => 'general_heading',
						'title'     => __( '基本的な設定', 'easythemes' ),
						'desc'      => __( 'サイトに関する基本的な設定を行ってください。', 'easythemes'),
						'icon'      => 'el-icon-wrench-alt',
						'fields'    => array(



								/**
								 * custom_favicon
								 */
								array(
										'id'        => 'custom_favicon',
										'type'      => 'media',
										'title'     => __( 'ファビコンのアップロード', 'easythemes' ),
										'url'      => true,
										'desc'      => __( '16px x 16px のpngかgifの画像をファビコンとしてアップロードできます。
										                  <br><small>※ ファビコンはブラウザタブやブックマーク等に表示されるサイトのアイコンのことです。</small>', 'easythemes' ),
								),

								/**
								 * custom_appicon
								 */
								array(
										'id'        => 'custom_appicon',
										'type'      => 'media',
										'title'     => __( 'スマートフォン ブックマーク アイコンのアップロード', 'easythemes' ),
										'url'      => true,
										'desc'      => __( '160px x 160px のpngかgifの画像をスマートフォンでのホームへ追加した際のアイコンとして設定することができます。', 'easythemes' ),
								),


								/**
								 * google_analytics
								 */
								array(
										'id'        => 'google_analytics',
										'type'      => 'ace_editor',
										'title'     => __( 'Google Analytics トラッキングコード', 'easythemes' ),
										'compiler'  => 'true',
										'mode'      => 'html',
										'desc'      => __( 'Google Analyticsのトラッキングコードを入力してください。<br>
										                  <small>※ アクセス解析をご使用でない方は空のままで問題ありません。</small>
										                  <small>※ ログイン時には出力されない設定となっています。</small>', 'easythemes' ),
								),
								/**
								 * seo setting
								 */
								array(
										'id'        => 'seo_settings',
										'type'      => 'info',
										'title'     => __( 'SEOの設定', 'easythemes' ),
										'url'      => true,
										'desc'      => __( '', 'easythemes' ),
								),
								/**
								 * seo_disp
								 */
								array(
										'id'        => 'seo_disp',
										'type'      => 'radio',
										'title'     => __( 'SEO : 出力設定 ', 'easythemes' ),
										'desc'      => __( '他のプラグインなどで対応する際には、出力をしないように設定してください。', 'easythemes' ),
										'default'   => 'true',
										'options'  => array(
											'true' => '出力する',
											'false' => '出力しない',
										),
								),
								/**
								 * seo_description
								 */
								array(
										'id'        => 'seo_description',
										'type'      => 'text',
										'title'     => __( 'SEO : 説明文 ', 'easythemes' ),
										'desc'      => __( 'サイトの説明文を入力してください。<small>※ meta description タグに反映されます。</small>', 'easythemes' ),
										'default'   => get_bloginfo( 'description' ),
								),

								/**
								 * seo_keyword
								 */
								array(
										'id'        => 'seo_keyword',
										'type'      => 'text',
										'title'     => __( 'SEO : キーワード ', 'easythemes' ),
										'desc'      => __( '検索エンジンに対するキーワードをカンマ区切りで入力してください。<small>※ meta keywords タグに反映されます。</small>', 'easythemes' ),
										'default'   => get_bloginfo( 'name' ),
								),

								/**
								 * ogp_description
								 */
								array(
										'id'        => 'ogp_description',
										'type'      => 'text',
										'title'     => __( 'OGP : 説明文 ', 'easythemes' ),
										'desc'      => __( 'シェアの際に表示される説明文を入力してください。', 'easythemes' ),
										'default'   => get_bloginfo( 'description' ),
								),
								/**
								 * ogp_image
								 */
								array(
										'id'        => 'ogp_image',
										'type'      => 'media',
										'title'     => __( 'OGP : サムネイル画像 ', 'easythemes' ),
										'desc'      => __( 'SNSシェアの際に表示される画像をアップロードしてください。', 'easythemes' ),
										// 'default'   => get_bloginfo( 'description' ),
								),

								/**
								 * custom_favicon
								 */
								array(
										'id'        => 'free_image_import',
										'type'      => 'info',
										'title'     => __( '画像のインポート', 'easythemes' ),
										'url'      => true,
										'desc'      => __( 'テーマに同梱されている画像をWordPress にインポートすることができます。<a class="button button-primary" href="' . admin_url( '/admin.php?page=_options&tab=1&free_image_import=on' ) . '">インポート</a>', 'easythemes' ),
								),
								/**
								 * custom_favicon
								 */
								array(
										'id'        => 'theme_tour_start',
										'type'      => 'info',
										'title'     => __( 'テーマの使い方ツアーのスタート', 'easythemes' ),
										'url'      => true,
										'desc'      => __( 'テーマの使い方ツアーをもう一度再開することができます。<a class="button button-primary" href="' . admin_url( '/themes.php?default_tour_start=on' ) . '">ツアーをスタート</a>', 'easythemes' ),
								),
						)
				);

				/**
				 * ==========================================
				 * 2. Styling Option
				 * ==========================================
				 */
				$this->sections[] = array(
						'id'        => 'styling_option',
						'title'     => __( 'スタイリング設定', 'easythemes' ),
						'icon'      => 'el-icon-magic',
						'fields'    => array(

								/**
								 * theme_colorpicker
								 */
								array(
										'id'        => 'theme_colorpicker',
										'type'      => 'color',
										'title'     => __( 'テーマカラー', 'easythemes' ),
										'compiler'  => 'true',
										'default'   => '#002f60',
										'mode'      => true,
										'desc'      => __( 'テーマカラーを選んでください。', 'easythemes' ),
										'hint'      => array(
											//'title'     => '',
											'content'   => '( ウィジェットタイトル、説明文背景に適応されます。 )',
										)
								),


								/**
								 * theme_color_force
								 */
								array(
									'id'          => 'theme_color_force',
									'type'        => 'radio',
									'title'       => __( 'テーマカラーの優先', 'easythemes' ),
									'subtitle'    => __( '優先を選択すると、ナビゲーション、見出し背景、リンク色などがテーマカラーに合わせて設定されます。', 'easythemes' ),
									'options'  => array(
										'true' => '優先',
										'false' => '各設定を優先',
									),
									'default' => 'false',
								),

								/**
								 * body_background
								 */
								array(
										'id'        => 'body_background',
										'type'      => 'background',
										'title'     => __( 'サイト : 背景', 'easythemes' ),
										'output'    => array( 'body' ),
										'default'   => '#FFF',
										'mode'      => true,
										'desc'      => __( 'サイトの背景を設定してください。', 'easythemes' ),
										'hint'      => array(
											//'title'     => '',
											'content'   => 'body の background プロパティに適応されます。',
										)

								),

								/**
								 * theme_radius
								 */
								array(
										'id'        => 'theme_radius',
										'type'      => 'slider',
										'title'     => __( '角丸', 'easythemes' ),
										'default'   => 0,
										'desc'      => __( 'サイト全体の角丸を指定していただくことができます。', 'easythemes' ),
										'min'       => 0,
										'step'      => 1,
										'max'       => 10,
										'display_value' => 0,
								),

						)
				);

				/**
				 * ==========================================
				 * 3. Font Option
				 * ==========================================
				 */
				$this->sections[] = array(
						'id'        => 'font_options',
						'title'     => __( 'フォント設定', 'easythemes' ),
						'icon'      => 'el-icon-fontsize',
						'fields'    => array(

								/**
								 * font_body
								 */
								array(
									'id'          => 'font_body',
									'type'        => 'typography',
									'title'       => __( 'ボディ : フォント', 'easythemes' ),
									'google'      => true,
									'output'      => array( 'body' ),
									'units'       => 'px',
									'subtitle'    => __( 'サイトコンテンツ内のフォントを設定してください。', 'easythemes' ),
									'default'     => array(
										'color'       => '#252525',
										'font-style'  => '300',
										'font-family' => 'Abel',
										'google'      => true,
										'font-size'   => '14px',
										'line-height' => '40px',
									),
									'word-spacing'    => true,
									'letter-spacing'  => true,
								),

								/**
								 * font_link
								 */
								array(
									'id'              => 'font_link',
									'type'            => 'link_color',
									'title'           => __( 'リンク : 文字色', 'easythemes' ),
									'google'          => true,
									'output'          => array( 'a' ),
									'units'           => 'px',
									'word-spacing'    => true,
									'letter-spacing'  => true,
									'subtitle'        => __( 'サイトコンテンツ内のリンクを設定してください。', 'easythemes' ),
									'default'         => array(
										'regular'  => '#1e73be', // blue
										'hover'    => '#dd3333', // red
										'active'   => '#BC1A1A',  // purple
										'visited'  => '#BC1A1A',  // purple
									),
								),


								/**
								 * font_link
								 */
								array(
									'id'              => 'font_shadow',
									'type'            => 'slider',
									'title'           => __( 'テキストシャドウ : 文字影', 'easythemes' ),
									'type'     => 'slider',
									'subtitle' => __( 'テキストシャドウを指定してください。', 'easythemes' ),
									'default' => 0,
									'output' => array( 'width' => '.header'  ),
									'compiler' => true,
									'min' => 0,
									'step' => 1,
									'max' => 10,
									'display_value' => 0,
								),
						)
				);
				/**
				 * ==========================================
				 * 4. Heddings Option
				 * ==========================================
				 */
				$this->sections[] = array(
						'id'        => 'headdings_option',
						'title'     => __( '見出し設定', 'easythemes' ),
						'icon'      => 'el-icon-bold',
						'fields'    => array(

							/**
							 * font_widget--title
							 */

							array(
								'id'   => 'widgettitle_info',
								'type' => 'info',
								'desc' => __( 'ウィジェットタイトル の設定', 'easythemes')
							),

							array(
								'id'          => 'headding_widgettitle_font',
								'type'        => 'typography',
								'title'       => __( 'ウィジェットタイトル : フォント', 'easythemes' ),
								'google'      => true,
								'output'      => array( '.widget--title' ),
								'units'       => 'px',
								'subtitle'    => __( 'ウィジェットタイトル のフォント設定をしてください。', 'easythemes' ),
								'default'     => array(
									'color'       => '#252525',
									'font-style'  => '700',
									'font-family' => 'Abel',
									'google'      => true,
									'font-size'   => '20px',
									'line-height' => '46px',
								),
								'word-spacing'    => true,
								'letter-spacing'  => true,
							),

							/**
							 * headding_widgettitle_background
							 */
							array(
								'id'          => 'headding_widgettitle_background',
								'type'        => 'background',
								'title'       => __( 'ウィジェットタイトル : 背景', 'easythemes' ),
								'output'      => array( '.widget--title' ),
								'subtitle'    => __( 'ウィジェットタイトル タグの背景を設定してください。', 'easythemes' ),
								'default'     => array(
									'background-color' => '#EEEEEE',
								)
							),

							/**
							 * headding_h1_border
							 */
							array(
								'id'       => 'headding_widgettitle_border',
								'type'     => 'border',
								'title'    => __( 'ウィジェットタイトル : ボーダー', 'easythemes' ),
								'output'   => array( '.widget--title' ),
								'desc'     => __( '下線のカラーを選択してください。', 'easythemes' ),
								'top' => false,
								'left' => false,
								'right' => false,
								'default'  => array(
									'border-color'  => '#1e73be',
									'border-style'  => 'solid',
									'border-top'    => '0px',
									'border-right'  => '0px',
									'border-bottom' => '0px',
									'border-left'   => '0px',
								)
							),

							array(
								'id'          => 'divide_widgettitle',
								'type'        => 'divide',
							),

							/**
							 * font_archive--title
							 */

							array(
								'id'   => 'archivetitle_info',
								'type' => 'info',
								'desc' => __( '投稿一覧タイトル の設定', 'easythemes' ),
							),

							array(
								'id'          => 'headding_archivetitle_font',
								'type'        => 'typography',
								'title'       => __( '投稿一覧タイトル : フォント', 'easythemes' ),
								'google'      => true,
								'output'      => array( '.entry-title, .entry-title a' ),
								'units'       => 'px',
								'subtitle'    => __( '投稿一覧タイトル のフォント設定をしてください。', 'easythemes' ),
								'default'     => array(
									'color'       => '#252525',
									'font-style'  => '700',
									'font-family' => 'Abel',
									'google'      => true,
									'font-size'   => '20px',
									'line-height' => '40px',
								),
								'word-spacing'    => true,
								'letter-spacing'  => true,
							),

							/**
							 * headding_widgettitle_background
							 */
							array(
								'id'          => 'headding_archivetitle_background',
								'type'        => 'background',
								'title'       => __( '投稿一覧タイトル : 背景', 'easythemes' ),
								'output'      => array( '.entry-title' ),
								'subtitle'    => __( '投稿一覧タイトル タグの背景を設定してください。', 'easythemes' ),
								'default'     => array(
									'background-color' => '#EEEEEE',
								)
							),

							/**
							 * headding_h1_border
							 */
							array(
								'id'       => 'headding_archivetitle_border',
								'type'     => 'border',
								'title'    => __( '投稿一覧タイトル : ボーダー', 'easythemes' ),
								'output'   => array( '.entry-title' ),
								'desc'     => __( '下線のカラーを選択してください。', 'easythemes' ),
								'top' => false,
								'left' => false,
								'right' => false,
								'default'  => array(
									'border-color'  => '#1e73be',
									'border-style'  => 'solid',
									'border-top'    => '0px',
									'border-right'  => '0px',
									'border-bottom' => '0px',
									'border-left'   => '0px',
								)
							),

							array(
								'id'          => 'divide_widgettitle',
								'type'        => 'divide',
							),



							/**
							 * font_h1
							 */

							array(
								'id'   => 'h1_info',
								'type' => 'info',
								'desc' => __( '見出し H1 の設定', 'easythemes')
							),

							array(
								'id'          => 'headding_h1_font',
								'type'        => 'typography',
								'title'       => __( '見出し H1 : フォント', 'easythemes' ),
								'google'      => true,
								'output'      => array( 'h1, h1 a' ),
								'units'       => 'em',
								'subtitle'    => __( '見出しタグ H1 のフォント設定をしてください。', 'easythemes' ),
								'default'     => array(
									'color'       => '#252525',
									'font-style'  => '700',
									'font-family' => 'Abel',
									'google'      => true,
									'font-size'   => '3.0',
									'line-height' => '1.6',
								),
								'word-spacing'    => true,
								'letter-spacing'  => true,
							),

							/**
							 * headding_h1_background
							 */
							array(
								'id'          => 'headding_h1_background',
								'type'        => 'background',
								'title'       => __( '見出し H1 : 背景', 'easythemes' ),
								'font-backup' => true,
								'output'      => array( 'h1' ),
								'subtitle'    => __( 'H1タグの背景を設定してください。', 'easythemes' ),
							),

							/**
							 * headding_h1_border
							 */
							array(
								'id'       => 'headding_h1_border',
								'type'     => 'border',
								'title'    => __( '見出し h1 : ボーダー', 'easythemes' ),
								'output'   => array( 'h1' ),
								'desc'     => __( '下線のカラーを選択してください。', 'easythemes' ),
								'default'  => array(
									'border-color'  => '#1e73be',
									'border-style'  => 'solid',
									'border-top'    => '0px',
									'border-right'  => '0px',
									'border-bottom' => '0px',
									'border-left'   => '0px',
								)
							),

							array(
								'id'          => 'divide_h1',
								'type'        => 'divide',
							),

							/**
							 * font_h2
							 */

							array(
								'id'   => 'h2_info',
								'type' => 'info',
								'desc' => __( '見出し H2 の設定', 'easythemes')
							),

							array(
								'id'          => 'headding_h2_font',
								'type'        => 'typography',
								'title'       => __( '見出し H2 : フォント', 'easythemes' ),
								'google'      => true,
								'output'      => array( 'h2, h2 a' ),
								'units'       => 'em',
								'subtitle'    => __( '見出しタグ H2 のフォント設定をしてください。', 'easythemes' ),
								'default'     => array(
									'color'       => '#252525',
									'font-style'  => '700',
									'font-family' => 'Abel',
									'google'      => true,
									'font-size'   => '2.6',
									'line-height' => '1.7',
								),
								'word-spacing'    => true,
								'letter-spacing'  => true,
							),

							/**
							 * headding_h2_background
							 */
							array(
								'id'          => 'headding_h2_background',
								'type'        => 'background',
								'title'       => __( '見出し H2 : 背景', 'easythemes' ),
								'font-backup' => true,
								'output'      => array( 'h2' ),
								'subtitle'    => __( 'H1タグの背景を設定してください。', 'easythemes' ),
							),

							/**
							 * headding_h2_border
							 */
							array(
								'id'       => 'headding_h2_border',
								'type'     => 'border',
								'title'    => __( '見出し h2 : ボーダー', 'easythemes' ),
								'output'   => array( 'h2' ),
								'desc'     => __( '下線のカラーを選択してください。', 'easythemes' ),
								'default'  => array(
									'border-color'  => '#1e73be',
									'border-style'  => 'solid',
									'border-top'    => '0px',
									'border-right'  => '0px',
									'border-bottom' => '0px',
									'border-left'   => '0px',
								)
							),

							array(
								'id'          => 'divide_h2',
								'type'        => 'divide',
							),



							/**
							 * font_h3
							 */

							array(
								'id'   => 'h3_info',
								'type' => 'info',
								'desc' => __( '見出し H3 の設定', 'easythemes')
							),

							array(
								'id'          => 'headding_h3_font',
								'type'        => 'typography',
								'title'       => __( '見出し H3 : フォント', 'easythemes' ),
								'google'      => true,
								'output'      => array( 'h3, h3 a' ),
								'units'       => 'em',
								'subtitle'    => __( '見出しタグ H3 のフォント設定をしてください。', 'easythemes' ),
								'default'     => array(
									'color'       => '#252525',
									'font-style'  => '700',
									'font-family' => 'Abel',
									'google'      => true,
									'font-size'   => '2.0',
									'line-height' => '1.7',
								),
								'word-spacing'    => true,
								'letter-spacing'  => true,
							),


							/**
							 * headding_h3_background
							 */
							array(
								'id'          => 'headding_h3_background',
								'type'        => 'background',
								'title'       => __( '見出し H3 : 背景', 'easythemes' ),
								'font-backup' => true,
								'output'      => array( 'h3' ),
								'subtitle'    => __( 'H1タグの背景を設定してください。', 'easythemes' ),
							),

							/**
							 * headding_h3_border
							 */
							array(
								'id'       => 'headding_h3_border',
								'type'     => 'border',
								'title'    => __( '見出し h3 : ボーダー', 'easythemes' ),
								'output'   => array( 'h3' ),
								'desc'     => __( '下線のカラーを選択してください。', 'easythemes' ),
								'default'  => array(
									'border-color'  => '#1e73be',
									'border-style'  => 'solid',
									'border-top'    => '0px',
									'border-right'  => '0px',
									'border-bottom' => '0px',
									'border-left'   => '0px',
								)
							),

							array(
								'id'          => 'divide_h3',
								'type'        => 'divide',
							),

							/**
							 * font_h4
							 */

							array(
								'id'   => 'h4_info',
								'type' => 'info',
								'desc' => __( '見出し H4 の設定', 'easythemes')
							),

							array(
								'id'          => 'headding_h4_font',
								'type'        => 'typography',
								'title'       => __( '見出し H4 : フォント', 'easythemes' ),
								'google'      => true,
								'output'      => array( 'h4, h4 a' ),
								'units'       => 'em',
								'subtitle'    => __( '見出しタグ H4 のフォント設定をしてください。', 'easythemes' ),
								'default'     => array(
									'color'       => '#252525',
									'font-style'  => '700',
									'font-family' => 'Abel',
									'google'      => true,
									'font-size'   => '1.6',
									'line-height' => '1.7',
								),
								'word-spacing'    => true,
								'letter-spacing'  => true,
							),


							/**
							 * headding_h4_background
							 */
							array(
								'id'          => 'headding_h4_background',
								'type'        => 'background',
								'title'       => __( '見出し H4 : 背景', 'easythemes' ),
								'font-backup' => true,
								'output'      => array( 'h4' ),
								'subtitle'    => __( 'H1タグの背景を設定してください。', 'easythemes' ),
							),

							/**
							 * headding_h4_border
							 */
							array(
								'id'       => 'headding_h4_border',
								'type'     => 'border',
								'title'    => __( '見出し h4 : ボーダー', 'easythemes' ),
								'output'   => array( 'h4' ),
								'desc'     => __( '下線のカラーを選択してください。', 'easythemes' ),
								'default'  => array(
									'border-color'  => '#1e73be',
									'border-style'  => 'solid',
									'border-top'    => '0px',
									'border-right'  => '0px',
									'border-bottom' => '0px',
									'border-left'   => '0px',
								)
							),

							array(
								'id'          => 'divide_h4',
								'type'        => 'divide',
							),

							/**
							 * font_h5
							 */

							array(
								'id'   => 'h5_info',
								'type' => 'info',
								'desc' => __( '見出し H5 の設定', 'easythemes')
							),

							array(
								'id'          => 'headding_h5_font',
								'type'        => 'typography',
								'title'       => __( '見出し H5 : フォント', 'easythemes' ),
								'google'      => true,
								'output'      => array( 'h5, h5 a' ),
								'units'       => 'em',
								'subtitle'    => __( '見出しタグ H5 のフォント設定をしてください。', 'easythemes' ),
								'default'     => array(
									'color'       => '#252525',
									'font-style'  => '700',
									'font-family' => 'Abel',
									'google'      => true,
									'font-size'   => '1.4',
									'line-height' => '1.7',
								),
								'word-spacing'    => true,
								'letter-spacing'  => true,
							),

							/**
							 * headding_h5_background
							 */
							array(
								'id'          => 'headding_h5_background',
								'type'        => 'background',
								'title'       => __( '見出し H5 : 背景', 'easythemes' ),
								'font-backup' => true,
								'output'      => array( 'h5' ),
								'subtitle'    => __( 'H6タグの背景を設定してください。', 'easythemes' ),
							),

							array(
								'id'          => 'divide_h5',
								'type'        => 'divide',
							),

							/**
							 * headding_h5_border
							 */
							array(
								'id'       => 'headding_h5_border',
								'type'     => 'border',
								'title'    => __( '見出し h5 : ボーダー', 'easythemes' ),
								'output'   => array( 'h5' ),
								'desc'     => __( '下線のカラーを選択してください。', 'easythemes' ),
								'default'  => array(
									'border-color'  => '#1e73be',
									'border-style'  => 'solid',
									'border-top'    => '0px',
									'border-right'  => '0px',
									'border-bottom' => '0px',
									'border-left'   => '0px',
								)
							),

							/**
							 * font_h6
							 */

							array(
								'id'   => 'h6_info',
								'type' => 'info',
								'desc' => __( '見出し H6 の設定', 'easythemes')
							),

							array(
								'id'          => 'headding_h6_font',
								'type'        => 'typography',
								'title'       => __( '見出し H6 : フォント', 'easythemes' ),
								'google'      => true,
								'output'      => array( 'h6, h6 a' ),
								'units'       => 'em',
								'subtitle'    => __( '見出しタグ H6 のフォント設定をしてください。', 'easythemes' ),
								'default'     => array(
									'color'       => '#252525',
									'font-style'  => '700',
									'font-family' => 'Abel',
									'google'      => true,
									'font-size'   => '1.2',
									'line-height' => '1.7',
								),
								'word-spacing'    => true,
								'letter-spacing'  => true,
							),

							/**
							 * headding_h6_background
							 */
							array(
								'id'          => 'headding_h6_background',
								'type'        => 'background',
								'title'       => __( '見出し H6 : 背景', 'easythemes' ),
								'font-backup' => true,
								'output'      => array( 'h6' ),
								'subtitle'    => __( 'H6タグの背景を設定してください。', 'easythemes' ),
							),

							/**
							 *
							 * headding_h6_border
							 */
							array(
								'id'       => 'headding_h6_border',
								'type'     => 'border',
								'title'    => __( '見出し h6 : ボーダー', 'easythemes' ),
								'output'   => array( 'h6' ),
								'desc'     => __( '下線のカラーを選択してください。', 'easythemes' ),
								'default'  => array(
									'border-color'  => '#1e73be',
									'border-style'  => 'solid',
									'border-top'    => '0px',
									'border-right'  => '0px',
									'border-bottom' => '0px',
									'border-left'   => '0px',
								)
							),
						)
				);

				/**
				 * ==========================================
				 * 5. Header Option
				 * ==========================================
				 */
				$this->sections[] = array(
						'id'        => 'header_option',
						'title'     => __( 'ヘッダー設定', 'easythemes' ),
						'icon'      => 'el-icon-adjust-alt',
						'fields'    => array(

								/**
								 * header_description
								 */
								array(
									'id'          => 'header_description',
									'type'        => 'text',
									'title'       => __( ' ヘッダー説明文 : テキスト', 'easythemes' ),
									'output'      => array( 'body' ),
									'units'       =>'em',
									'subtitle'    => __( 'ヘッダー上部のテキストを入力してください。', 'easythemes' ),
									'default'     => get_bloginfo( 'description' ),
									'hint'      => array(
											'content'   => 'デフォルトでは、サイトの説明文がが設定されています。',
									),
								),

								/**
								 * header_desc_background
								 */
								array(
										'id'        => 'header_desc_background',
										'type'      => 'background',
										'title'     => __( 'ヘッダー説明文 : 背景', 'easythemes' ),
										'output'    => array( '.header .header-description' ),
										'desc'      => __( 'ヘッダー説明文の背景を設定してください。', 'easythemes' ),
										'hint'      => array(
											'content'   => 'サイト最上部のテキストの背景を指定してください。',
										),
								),

								/**
								 * header_height
								 */
								array(
									'id'       => 'header_height',
									'type'     => 'slider',
									'title'    => __( 'ヘッダー : 高さ', 'easythemes' ),
									'subtitle' => __( 'ヘッダーの高さを指定してください。( % )', 'easythemes' ),
									'default' => 197,
									'output' => array( 'width' => '.header'  ),
									'compiler' => true,
									'min' => 80,
									'step' => 1,
									'max' => 1000,
									'display_value' => 197,
								),

								/**
								 * header_logo_text
								 */
								array(
									'id'          => 'header_logo_text',
									'type'        => 'text',
									'title'       => __( 'ロゴ : テキスト', 'easythemes' ),
									'units'       =>'px',
									'subtitle'    => __( 'ロゴテキストを入力して下さい。', 'easythemes' ),
									'default'     => '',
								),

								/**
								 * header_logo_text_style
								 */
								array(
									'id'          => 'header_logo_text_style',
									'type'        => 'typography',
									'title'       => __( 'ロゴ : フォント', 'easythemes' ),
									'google'      => true,
									'output'      => array( '#logo' ),
									'units'       => 'px',
									'subtitle'    => __( 'ロゴのフォント設定をしてください。', 'easythemes' ),
									'default'     => array(
										'color'       => '#252525',
										'font-style'  => '700',
										'font-family' => 'Abel',
										'google'      => true,
										'font-size'   => '46px',
										'line-height' => '340px',
									),
									'word-spacing'    => true,
									'letter-spacing'  => true,
									'hint'      => array(
											'content'   => 'ロゴの高さ位置を決める際には、「行間」の値を調整してください。',
									),

								),

								/**
								 * header_logo_media_upload
								 */
								array(
									'id'          => 'header_logo_media_upload',
									'type'        => 'background',
									'output'      => array( '.header' ),
									'title'       => __( 'ロゴ : 背景', 'easythemes' ),
									'units'       =>'px',
									'subtitle'    => __( 'ヘッダー背景画像を設定します。メディアアップローダーを使用して画像をアップロードするか、URLを直接入力してください', 'easythemes' ),
									'default'  => array(
										'background-color' => '#1e73be',
										'background-image' => get_template_directory_uri() . '/assets/images/header-bg.jpg',
									)
								),

								/**
								 * header_type
								 */
								array(
									'id'          => 'header_type',
									'type'        => 'radio',
									'output'      => array( '.header' ),
									'title'       => __( 'ヘッダー : タイプ', 'easythemes' ),
									'subtitle'    => __( 'ヘッダー部の形状を指定してください。', 'easythemes' ),
									'options'  => array(
										'1' => '通常',
										'2' => '画面に合わせる',
									),
									'default' => '1',
								),

						)
				);

				/**
				 * ==========================================
				 * 5. Navigation Option
				 * ==========================================
				 */
				$this->sections[] = array(
						'id'        => 'navigation_option',
						'title'     => __( 'ナビゲーション設定', 'easythemes' ),
						'icon'      => 'el-icon-compass-alt',
						'fields'    => array(

								/**
								 * navigation_font
								 */
								array(
									'id'          => 'navigation_font',
									'type'        => 'typography',
									'title'       => __( 'ナビゲーション : フォント', 'easythemes' ),
									'google'      => true,
									'font-backup' => true,
									'output'      => array( '.navbar ul li a' ),
									'units'       => 'px',
									'subtitle'    => __( 'ナビゲーションのフォント設定をしてください。', 'easythemes' ),
									'default'     => array(
										'color'       => '#FFFFFF',
										'font-style'  => '400',
										'font-family' => 'Abel',
										'google'      => true,
										'font-size'   => '14px',
										'line-height' => '40px',
									),
								),

								/**
								 * navigation_link
								 */
								array(
									'id'          => 'navigation_color',
									'type'        => 'link_color',
									'title'       => __( 'ナビゲーション : 背景色', 'easythemes' ),
									'google'      => true,
									'font-backup' => true,
									// 'output'      => array( '.navbar ul.nav li a' ),
									'units'       => 'px',
									'subtitle'    => __( 'ナビゲーションのカラーを設定してください。', 'easythemes' ),
									'default'  => array(
										'regular'  => '#252525', // blue
										'hover'    => '#dd3333', // red
										'active'   => '#BC1A1A',  // purple
										'visited'  => '#BC1A1A',  // purple
									),
								),


								/**
								 * navigation_link
								 */
								array(
									'id'          => 'navigation_background',
									'type'        => 'background',
									'title'       => __( 'ナビゲーション : 背景', 'easythemes' ),
									'font-backup' => true,
									// 'output'      => array( '.navbar ul.nav li a' ),
									'units'       => 'px',
									'subtitle'    => __( 'ナビゲーションの背景を指定してください。', 'easythemes' ),
									'default'  => array(
										'regular'  => '#252525', // blue
										'hover'    => '#dd3333', // red
										'active'   => '#BC1A1A',  // purple
										'visited'  => '#BC1A1A',  // purple
									),
								),

								/**
								 * navigation_dropdown
								 */
								array(
									'id'          => 'navigation_dropdown',
									'type'        => 'link_color',
									'title'       => __( 'ナビゲーション : ドロップダウンカラー', 'easythemes' ),
									'google'      => true,
									'font-backup' => true,
									// 'output'      => array( '.navbar .dropdown' ),
									'units'       => 'px',
									'subtitle'    => __( 'ナビゲーションのドロップダウンカラーを設定してください。', 'easythemes' ),
									'default'  => array(
										'regular'  => '#252525', // blue
										'hover'    => '#dd3333', // red
										'active'   => '#BC1A1A',  // purple
										'visited'  => '#BC1A1A',  // purple
									),
								),

								/**
								 * navigation_dropdown
								 */
								array(
									'id'          => 'navigation_margin',
									'type'        => 'dimensions',
									'title'       => __( 'ナビゲーション : 大きさ', 'easythemes' ),
									'output'      => array( '.nav-collapse > ul > li > a' ),
									'units'       => 'px',
									'subtitle'    => __( 'ナビゲーションの大きさを設定してください。', 'easythemes' ),
									'default'  => array(
										'Width'   => '200',
										'Height'  => '100',
									),
								),

						)
				);

				/**
				 * ==========================================
				 * 6. information Option
				 * ==========================================
				 */
				$this->sections[] = array(
						'id'        => 'information_option',
						'title'     => __( 'インフォメーションバーの設定', 'easythemes' ),
						'icon'      => 'el-icon-eye-open',
						'fields'    => array(

								/**
								 * infobar display
								 */
								array(
									'id'       => 'infobar_display',
									'type'     => 'radio',
									'title'    => __( 'インフォメーションバーの表示', 'easythemes' ),
									'subtitle' => __( 'インフォメーションバーに表示するコンテンツを設定してください。', 'easythemes' ),
									'options'  => array(
										'true' => '表示',
										'false' => '非表示',
									),
									'default' => 'false',
								),

								/**
								 * infobar display
								 */
								array(
									'id'       => 'info_bar_news',
									'type'     => 'multi_text',
									'title'    => __( '新着情報', 'easythemes' ),
									'subtitle' => __( 'インフォメーションバーに表示するコンテンツを設定してください。', 'easythemes' ),
									'desc'     => 'ニュースティッカーのように、流れる文字として表示されます。',
									'default'  => '',
									'add_text' => '追加',
								),


						)
				);

				/**
				 * ==========================================
				 * 7. footer Option
				 * ==========================================
				 */
				$this->sections[] = array(
						'id'        => 'footer_option',
						'title'     => __( 'フッター設定', 'easythemes' ),
						'icon'      => 'el-icon-screen-alt',
						'fields'    => array(

								/**
								 * footer_type
								 */
								array(
									'id'          => 'footer_type',
									'type'        => 'radio',
									'output'      => array( '.header' ),
									'title'       => __( 'フッター : タイプ', 'easythemes' ),
									'subtitle'    => __( 'フッター部の形状を指定してください。', 'easythemes' ),
									'options'  => array(
										'1' => '通常',
										'2' => '画面に合わせる',
									),
									'default' => '1',
								),

								/**
								 * footer_background
								 */
								array(
										'id'        => 'footer_background',
										'type'      => 'background',
										'title'     => __( 'フッター : 背景', 'easythemes' ),
										'compiler'  => 'true',
										'output'    => array( '.footer' ),
										'mode'      => true,
										'desc'      => __( 'フッター背景を設定してください。', 'easythemes' ),
										// 'hint'      => array(
										// 		'content'   => '#footer の background プロパティに適応されます。',
										// ),
										'default' => array(
											'background-color' => '#f5f5f5',
										),
								),


								/**
								 * footer_copyright
								 */
								array(
									'id'       => 'footer_copyright',
									'type'     => 'textarea',
									'title'    => __( 'フッターコピーライト : テキスト', 'easythemes' ),
									'subtitle' => __( 'サイト下部に表示するコピーライトを設定してください。', 'easythemes' ),
									'default' => 'Copyright © '. get_the_date( 'Y' ) . '<a href="' . site_url() . '">'. get_bloginfo( 'name' ) .'. All Rights Reserved.',

								),

								array(
									'id'          => 'footer_copyright_font',
									'type'        => 'typography',
									'title'       => __( 'フッターコピーライト : フォント', 'easythemes' ),
									'google'      => true,
									'output'      => array( '.copyright' ),
									'units'       => 'px',
									'subtitle'    => __( 'フッターコピーライトのフォント設定をしてください。', 'easythemes' ),
									'default'     => array(
										'color'       => '#252525',
										'font-style'  => '700',
										'font-family' => 'Abel',
										'google'      => true,
										'font-size'   => '15px',
										'line-height' => '46px',
									),
									'word-spacing'    => true,
									'letter-spacing'  => true,
								),

								/**
								 * copyright_background
								 */
								array(
										'id'        => 'copyright_background',
										'type'      => 'background',
										'title'     => __( 'フッターコピーライト : 背景', 'easythemes' ),
										'compiler'  => 'true',
										'default'   => '#FFF',
										'mode'      => true,
										'desc'      => __( 'コピーライト背景を設定してください。', 'easythemes' ),
										// 'hint'      => array(
										// 		'content'   => '#footer の background プロパティに適応されます。',
										// ),
								),

								/**
								 * page_top
								 */
								array(
									'id'       => 'page_top',
									'type'     => 'radio',
									'title'    => __( 'トップに戻るボタンの表示', 'easythemes' ),
									'subtitle' => __( 'トップに戻るボタンの表意設定をしてください。', 'easythemes' ),
									'options'  => array(
										'true' => '表示',
										'false' => '非表示',
									),
									'default' => 'true',
								),


						)
				);

				/**
				 * ==========================================
				 * 8. Layout Option
				 * ==========================================
				 */
				$this->sections[] = array(
						'id'        => 'layout_option',
						'title'     => __( 'レイアウト設定', 'easythemes' ),
						'icon'      => 'el-icon-th-list',
						'fields'    => array(

								/**
								 * top_layout
								 */
								array(
									'id'       => 'top_layout',
									'type'     => 'image_select',
									'title'    => __( 'トップページ : レイアウト', 'easythemes' ),
									'subtitle' => __( 'トップページのレイアウトを選択してください。', 'easythemes' ),
									'options'  => array(
										'1col' => get_template_directory_uri() . '/assets/images/admin/1col.png',
										'2cl'  => get_template_directory_uri() . '/assets/images/admin/2cl.png',
										'2cr'  => get_template_directory_uri() . '/assets/images/admin/2cr.png',
										'3col' => get_template_directory_uri() . '/assets/images/admin/3cm.png',
									),
									'default'  => '2cl',
								),

								/**
								 * archive_layout
								 *
								 */
								array(
									'id'       => 'archive_layout',
									'type'     => 'image_select',
									'title'    => __( 'アーカイブページ : レイアウト', 'easythemes' ),
									'subtitle' => __( 'アーカイブページのレイアウトを選択してください。', 'easythemes' ),
									'options'  => array(
										'1col' => get_template_directory_uri() . '/assets/images/admin/1col.png',
										'2cl'  => get_template_directory_uri() . '/assets/images/admin/2cl.png',
										'2cr'  => get_template_directory_uri() . '/assets/images/admin/2cr.png',
										'3col' => get_template_directory_uri() . '/assets/images/admin/3cm.png',
									),
									'default'  => '2cl',
								),

								/**
								 * page_layout
								 *
								 */
								array(
									'id'       => 'page_layout',
									'type'     => 'image_select',
									'title'    => __( '固定ページ : レイアウト', 'easythemes' ),
									'subtitle' => __( '固定ページのレイアウトを選択してください。', 'easythemes' ),
									'options'  => array(
										'1col' => get_template_directory_uri() . '/assets/images/admin/1col.png',
										'2cl'  => get_template_directory_uri() . '/assets/images/admin/2cl.png',
										'2cr'  => get_template_directory_uri() . '/assets/images/admin/2cr.png',
										'3col' => get_template_directory_uri() . '/assets/images/admin/3cm.png',
									),
									'default'  => '2cl',
								),

								/**
								 * single layout
								 */
								array(
									'id'       => 'single_layout',
									'type'     => 'image_select',
									'title'    => __( '投稿ページ : レイアウト', 'easythemes' ),
									'subtitle' => __( '投稿ページのレイアウトを選択してください。', 'easythemes' ),
									'options'  => array(
										'1col' => get_template_directory_uri() . '/assets/images/admin/1col.png',
										'2cl'  => get_template_directory_uri() . '/assets/images/admin/2cl.png',
										'2cr'  => get_template_directory_uri() . '/assets/images/admin/2cr.png',
										'3col' => get_template_directory_uri() . '/assets/images/admin/3cm.png',
									),
									'default'  => '2cl',
								),

								array(
									'id'       => 'wrap_colmn_width',
									'type'     => 'slider',
									'title'    => __( 'サイト全体 : 幅', 'easythemes' ),
									'subtitle' => __( 'サイト全体の幅を指定してください。( % )', 'easythemes' ),
									'compiler' => array( '#wrapper' ),
									'default' => 950,
									'min' => 700,
									'step' => 10,
									'max' => 1980,
									'display_value' => 950,
								),

								array(
								    'id'   => 'two_column_info',
								    'type' => 'info',
								    'desc' => __( '２カラム時のコンテンツの幅について指定してください。※ メインカラムとサイドバーの値の合計が100になるように設定してください。 ', 'easythemes' )
								),

								/**
								 * main column width
								 */

								array(
									'id'       => 'main_colmn_width',
									'type'     => 'slider',
									'title'    => __( 'メインカラム : 幅', 'easythemes' ),
									'subtitle' => __( 'メインカラムの幅を指定してください。( % )', 'easythemes' ),
									'compiler' => true,
									'default' => 71,
									'min' => 0,
									'step' => 1,
									'max' => 100,
									'display_value' => 71
								),

								/**
								 * main column width
								 */

								array(
									'id'       => 'sidebar_width',
									'type'     => 'slider',
									'title'    => __( 'サイドバー : 幅', 'easythemes' ),
									'subtitle' => __( 'サイドバーの幅を指定してください。( % )', 'easythemes' ),
									'default' => 29,
									'output' => array( 'width' => '.sidebar'  ),
									'compiler' => true,
									'min' => 0,
									'step' => 1,
									'max' => 100,
									'display_value' => '20'
								),

								array(
								    'id'   => 'three_column_info',
								    'type' => 'info',
								    'desc' => __( '3カラム時のコンテンツの幅について指定してください。※ メインカラム、サイドバー左、サイドバー右の値の合計が100になるように設定してください。', 'easythemes' )
								),


								/**
								 * main column width
								 */

								array(
									'id'       => 'three_main_colmn_width',
									'type'     => 'slider',
									'title'    => __( 'メインカラム : 幅', 'easythemes' ),
									'subtitle' => __( 'メインカラムの幅を指定してください。( % )', 'easythemes' ),
									'compiler' => true,
									'default' => 53,
									'min' => 0,
									'step' => 0.5,
									'max' => 100,
								),

								/**
								 * main column width
								 */

								array(
									'id'       => 'three_left_sidebar_width',
									'type'     => 'slider',
									'title'    => __( 'サイドバー左 : 幅', 'easythemes' ),
									'subtitle' => __( 'サイドバーの幅を指定してください。( % )', 'easythemes' ),
									'default' => 23.5,
									'output' => array( 'width' => '.sidebar'  ),
									'compiler' => true,
									'min' => 0,
									'step' => 0.5,
									'max' => 100,
									'display_value' => '20'
								),

								/**
								 * main column width
								 */

								array(
									'id'       => 'three_right_sidebar_width',
									'type'     => 'slider',
									'title'    => __( 'サイドバー右 : 幅', 'easythemes' ),
									'subtitle' => __( '右サイドバーの幅を指定してください。( % )', 'easythemes' ),
									'default' => 23.5,
									'compiler' => true,
									'min' => 0,
									'step' => 0.5,
									'max' => 100,
								),
						),
				);

				/**
				 * ==========================================
				 * 9. template option
				 * ==========================================
				 */
				$this->sections[] = array(
						'id'        => 'template_option',
						'title'     => __( '定型文設定', 'easythemes' ),
	          'desc' => '定型文を設定することができます。<br>ここで設定した定型文は、投稿時のレイアウトブロック設定 → ブロックを追加 → 定型文ブロック から使用することができます。<br /> ※ HTML使用化',
						'icon'      => 'el-icon-file-new-alt',
						'fields'    => array(

								/**
								 * template_text_one
								 */
								array(
									'id'       => 'template_text_one',
									'type'     => 'editor',
									'title'    => __( '定型文1', 'easythemes' ),
									'default'  => '',
								),

								/**
								 * template_text_two
								 */
								array(
									'id'       => 'template_text_two',
									'type'     => 'editor',
									'title'    => __( '定型文2', 'easythemes' ),
									'default'  => '',
								),

								/**
								 * template_text_three
								 */
								array(
									'id'       => 'template_text_three',
									'type'     => 'editor',
									'title'    => __( '定型文3', 'easythemes' ),
									'default'  => '',
								),

								/**
								 * template_text_four
								 */
								array(
									'id'       => 'template_text_four',
									'type'     => 'editor',
									'title'    => __( '定型文4', 'easythemes' ),
									'default'  => '',
								),

								/**
								 * template_text_five
								 */
								array(
									'id'       => 'template_text_five',
									'type'     => 'editor',
									'title'    => __( '定型文5', 'easythemes' ),
									'default'  => '',
								),

								/**
								 * template_text_six
								 */
								array(
									'id'       => 'template_text_six',
									'type'     => 'editor',
									'title'    => __( '定型文6', 'easythemes' ),
									'default'  => '',
								),

								/**
								 * template_text_seven
								 */
								array(
									'id'       => 'template_text_seven',
									'type'     => 'editor',
									'title'    => __( '定型文7', 'easythemes' ),
									'default'  => '',
								),

								/**
								 * template_text_eight
								 */
								array(
									'id'       => 'template_text_eight',
									'type'     => 'editor',
									'title'    => __( '定型文8', 'easythemes' ),
									'default'  => '',
								),

								/**
								 * template_text_nine
								 */
								array(
									'id'       => 'template_text_nine',
									'type'     => 'editor',
									'title'    => __( '定型文9', 'easythemes' ),
									'default'  => '',
								),

								/**
								 * template_text_ten
								 */
								array(
									'id'       => 'template_text_ten',
									'type'     => 'editor',
									'title'    => __( '定型文10', 'easythemes' ),
									'default'  => '',
								),

						),
				);

		}

		public function setHelpTabs() {

				// Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
				$this->args['help_tabs'][] = array(
						'id'        => 'redux-help-tab-1',
						'title'     => __('Theme Information 1', 'easythemes'),
						'content'   => __('<p>This is the tab content, HTML is allowed.</p>', 'easythemes')
				);

				$this->args['help_tabs'][] = array(
						'id'        => 'redux-help-tab-2',
						'title'     => __('Theme Information 2', 'easythemes'),
						'content'   => __('<p>This is the tab content, HTML is allowed.</p>', 'easythemes')
				);

				// Set the help sidebar
				$this->args['help_sidebar'] = __('<p>This is the sidebar content, HTML is allowed.</p>', 'easythemes');

		}

		/**
		 *
		 * All the possible arguments for Redux.
		 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
		 *
		 * */
		public function setArguments() {

				$theme = wp_get_theme(); // For use with some settings. Not necessary.

				$this->args = array(
						// TYPICAL -> Change these values as you need/desire
						'opt_name'          => 'easy_themes',            // This is where your data is stored in the database and also becomes your global variable name.
						'display_name'      => $theme->get('Name'),     // Name that appears at the top of your panel
						'display_version'   => $theme->get('Version'),  // Version that appears at the top of your panel
						'menu_type'         => 'menu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
						'allow_sub_menu'    => true,                    // Show the sections below the admin menu item or not
						'menu_title'        => 'テーマ設定',
						'page_title'        => 'テーマ設定',

						// You will need to generate a Google API key to use this feature.
						// Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
						'google_api_key' => '', // Must be defined to add google fonts to the typography module

						'async_typography'  => false,                    // Use a asynchronous font on the front end or font string
						'admin_bar'         => true,                    // Show the panel pages on the admin bar
						'global_variable'   => 'easy_themes',           // Set a different name for your global variable other than the opt_name
						'dev_mode'          => false,                    // Show the time the page took to load, etc
						'customizer'        => true,                    // Enable basic customizer support
						//'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
						//'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

						// OPTIONAL -> Give you extra features
						'page_priority'     => null,                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
						'page_parent'       => 'themes.php',            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
						'page_permissions'  => 'manage_options',        // Permissions needed to access the options panel.
						'menu_icon'         => '',                      // Specify a custom URL to an icon
						'last_tab'          => '',                      // Force your panel to always open to a specific tab (by id)
						'page_icon'         => 'icon-themes',           // Icon displayed in the admin panel next to your menu_title
						'page_slug'         => '_options',              // Page slug used to denote the panel
						'save_defaults'     => true,                    // On load save the defaults to DB before user clicks save or not
						'default_show'      => false,                   // If true, shows the default value next to each field that is not the default value.
						'default_mark'      => '',                      // What to print by the field's title if the value shown is default. Suggested: *
						'show_import_export' => true,                   // Shows the Import/Export panel when not used as a field.

						// CAREFUL -> These options are for advanced use only
						'transient_time'    => 60 * MINUTE_IN_SECONDS,
						'output'            => true,                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
						'output_tag'        => true,                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
						// 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

						// FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
						'database'              => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
						'system_info'           => false, // REMOVE

						// HINTS
						'hints' => array(
								'icon'          => 'icon-question-sign',
								'icon_position' => 'right',
								'icon_color'    => 'lightgray',
								'icon_size'     => 'normal',
								'tip_style'     => array(
										'color'         => 'light',
										'shadow'        => true,
										'rounded'       => false,
										'style'         => '',
								),
								'tip_position'  => array(
										'my' => 'top left',
										'at' => 'bottom right',
								),
								'tip_effect'    => array(
										'show'          => array(
												'effect'        => 'slide',
												'duration'      => '500',
												'event'         => 'mouseover',
										),
										'hide'      => array(
												'effect'    => 'slide',
												'duration'  => '500',
												'event'     => 'click mouseleave',
										),
								),
						)
				);


				// SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
				// $this->args['share_icons'][] = array(
				// 		'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
				// 		'title' => 'Visit us on GitHub',
				// 		'icon'  => 'el-icon-github'
				// 		//'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
				// );
				// $this->args['share_icons'][] = array(
				// 		'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
				// 		'title' => 'Like us on Facebook',
				// 		'icon'  => 'el-icon-facebook'
				// );
				// $this->args['share_icons'][] = array(
				// 		'url'   => 'http://twitter.com/reduxframework',
				// 		'title' => 'Follow us on Twitter',
				// 		'icon'  => 'el-icon-twitter'
				// );
				// $this->args['share_icons'][] = array(
				// 		'url'   => 'http://www.linkedin.com/company/redux-framework',
				// 		'title' => 'Find us on LinkedIn',
				// 		'icon'  => 'el-icon-linkedin'
				// );

				// Panel Intro text -> before the form
				if ( ! isset( $this->args['global_variable']) || $this->args['global_variable'] !== false) {
						if (!empty($this->args['global_variable'])) {
								$v = $this->args['global_variable'];
						} else {
								$v = str_replace('-', '_', $this->args['opt_name']);
						}
						$this->args['intro_text'] = sprintf(__('', 'easythemes'), $v);
				}

				$this->args['intro_text'] = __('', 'easythemes');
				// // Add content after the form.
				$this->args['footer_text'] = __('', 'easythemes');
				$this->args['footer_credit'] = ' ';
				// update args
				$this->args['update_notice'] = false;
		}

	}

	global $easy_themes;
	$easy_themes_object = new Easy_Themes_Config();
	$easy_themes = $easy_themes_object->ReduxFramework->options;
}
