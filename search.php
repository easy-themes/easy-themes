<?php
/**
 * Search template.
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */

global $easy_themes,$easythemes_layout;

get_template_part( 'modules/head' );

easythemes_header_path();

do_action( 'get_header' );

dynamic_sidebar( 'main-visual' );

get_template_part( 'modules/sidebar' ); ?>

<div class="contents <?php echo $easythemes_layout->get_main_class(); ?>">
<?php if ( have_posts() ) : ?>
  <div class="headline">
		<h2 class="custom-title">検索結果</h2>
	</div>

	<p>"<?php the_search_query() ?>"の検索結果は以下の通りです:</p>

	<?php
	while ( have_posts() ) :
		the_post(); ?>

		<?php get_template_part( 'templates/content-archive' ) ?>

	<?php
	endwhile; ?>

			<div class="float-left"><?php next_posts_link('&laquo; 古いエントリー') ?></div>
			<div class="float-right"><?php previous_posts_link('新しいエントリー &raquo;') ?></div>

	<?php else : ?>

		  <div class="headline">
				<h2 class="custom-title">検索では見つかりませんでした。</h2>
			</div>
		<p>別のキーワードで試して下さい。</p>

	<?php endif; ?>



		<!-- main ends -->
		</div>

<?php

get_template_part( 'modules/sidebar-right' );

easythemes_footer_path();
