'use strict';

$( function () {

	/**
	 * navbar element height auto
	 * @type {[type]}
	 */
	var navbarElement = $( '.nav-collapse > ul > li > a' );
	var elementHeight,
			maxNavitemsHeight,
			oldHeight;

	navbarElement.each( function(){
		elementHeight = $( this ).height();
		if ( ! oldHeight || elementHeight > oldHeight) {
			maxNavitemsHeight = elementHeight;
		};

		oldHeight = maxNavitemsHeight;
	} );

	navbarElement.css( 'height', maxNavitemsHeight );

} );



var $window, $document, $body, $html, $bodhtml;

// Initialize the defaults
window.AA_CONFIG = window.AA_CONFIG || {};
window.AA_CONFIG = $.extend({
	animationLength:  300,
	easingFunction:   'linear',
	scrollOffset:     0
}, window.AA_CONFIG );

// Document ready?
	// Well then do the magic.
$(document).ready(function(){

	// Grab our elements.
	$window   = $(window);
	$document = $(this);
	$body     = $(document.body);
	$html     = $(document.documentElement);
	$bodhtml  = $body.add( $html );

	scrollToHash();
	$document.find('a[href^="#"], a[href^="."]').on('click', function(){
		var href = $(this).attr('href');

		if( href.charAt(0) === '.' ){
			href = href.split('#');
			href.shift();
			href = '#' + href.join('#');
		};

		if ( href === location.hash ){
			scrollToHash( href );
		}

	});

	$window.on('hashchange', function(){
		scrollToHash();
	});

	// Cancel scroll if user interacts with page.
	$window.on('mousewheel DOMMouseScroll touchstart mousedown MSPointerDown', function(ev){
		$bodhtml.stop(true, false);
	});


	var $body         = $('body');
	var $html         = $('html');
	var $scrollToTop  = $('.scroll-to-top');

	var scrollTimeout;
	$(window).on('scroll', function(){
		var $this = $(this);
		if ( scrollTimeout ){
			clearTimeout(scrollTimeout);
		}
		scrollTimeout = setTimeout(function(){
			if ( $this.scrollTop() > 100 ) {
				$scrollToTop.addClass('shown');
			} else {
				$scrollToTop.removeClass('shown');
			}
		}, 500);
	});

	$scrollToTop.on('click', function(){
		$body.add( $html ).animate({ scrollTop: 0 }, 100);
		$(this).removeClass('shown');
	});


});

function scrollToHash(rawHash){
	var rawHash       = rawHash || location.hash;
	var anchorTuple   = rawHash.substring(1).split("|");

	var hash          = anchorTuple[0];
	var animationTime = anchorTuple[1] || window.AA_CONFIG.animationLength;

	if ( hash.charAt(0).search(/[A-Za-z]/) > -1 ){
		var $actualID = $document.find( "#" + hash );
	}

	var $actualAnchor = $document.find('a[name="'+ hash +'"]');

	if ( ( $actualAnchor && $actualAnchor.length ) || ( $actualID && $actualID.length ) ){
		return;
	}

	// Store the arbitrary anchor element.
	var $arbitraryAnchor  = $(hash).first();
	if ( $arbitraryAnchor && $arbitraryAnchor.length ) {
		var $el = $arbitraryAnchor;
	} else {
		return;
	}

	// Scroll to $el.
	if ( $el && $el.length ) {
		var top = $el.offset().top - window.AA_CONFIG.scrollOffset;
		$bodhtml.stop(true, false).animate({ scrollTop: top },  parseInt(animationTime), window.AA_CONFIG.easingFunction );
	}

}
