<?php
global $easythemes_layout;
get_template_part( 'modules/head' );

easythemes_header_path();

do_action( 'get_header' );
dynamic_sidebar( 'main-visual' );
get_template_part( 'modules/sidebar' ); ?>

<div class="contents <?php echo $easythemes_layout->get_main_class(); ?>" >

<?php
if ( have_posts() ) :
		the_post();
	get_template_part( 'templates/content-page' );
else : ?>
	<h2>ページが見つかりません。</h2>
	<p>間違ったところを見ているようです。</p>
<?php
endif;

dynamic_sidebar( 'main-secondary' ); ?>

  </div>
<?php
get_template_part( 'modules/sidebar-right' );

easythemes_footer_path();
