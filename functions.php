<?php
/**
 * Function for theme.
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */

/**
 * include admin panel file
 */

require_once get_stylesheet_directory() . '/admin/admin-init.php';

/**
 * include theme function file
 */

require_once get_stylesheet_directory() . '/inc/functions-squeeze.php';
require_once get_stylesheet_directory() . '/inc/add-css.php';
require_once get_stylesheet_directory() . '/inc/setup.php';
require_once get_stylesheet_directory() . '/inc/widgets.php';
require_once get_stylesheet_directory() . '/inc/navigation.php';
require_once get_stylesheet_directory() . '/inc/sidebar.php';
require_once get_stylesheet_directory() . '/inc/comments.php';
require_once get_stylesheet_directory() . '/inc/scripts.php';
require_once get_stylesheet_directory() . '/inc/template-tags.php';
require_once get_stylesheet_directory() . '/inc/theme-modifield.php';
require_once get_stylesheet_directory() . '/inc/tour.php';

/**
 * include meta box
 */

require_once get_stylesheet_directory() . '/inc/includes/metabox/smart_meta_box/SmartMetaBox.php';
require_once get_stylesheet_directory() . '/inc/includes/metabox/add_meta.php';

/**
 * include plugin file
 */

require_once get_stylesheet_directory() . '/plugins/plugin-activation.php';
require_once get_stylesheet_directory() . '/plugins/plugin-install.php';
require_once get_stylesheet_directory() . '/plugins/video-sidebar-widgets/video-sidebar-widgets.php';
require_once get_stylesheet_directory() . '/plugins/image-widget/image-widget.php';
require_once get_stylesheet_directory() . '/plugins/image-widget-two/image-widget-two.php';
require_once get_stylesheet_directory() . '/plugins/image-widget-three/image-widget-three.php';
require_once get_stylesheet_directory() . '/plugins/facebook/facebook.php';
require_once get_stylesheet_directory() . '/plugins/github-updater/github-updater.php';
