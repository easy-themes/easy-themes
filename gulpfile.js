'use strict';

// Load plugins
var gulp = require('gulp');
var plugins = require('gulp-load-plugins')({ camelize: true });
var browserSync = require('browser-sync');
var reload = browserSync.reload;



// Browser Sync
gulp.task('browser-sync', function() {
    browserSync.init(null, {
		    notify: true,
        proxy: {
            host: "easy-themes.dev",
        }
    });
});

// Less
gulp.task('less', function(){
	return gulp.src('./assets/less/custom/custom.less')
	.pipe(plugins.plumber())
	.pipe(plugins.less())
	.pipe(plugins.autoprefixer('last 2 versions', 'ie 9', 'ios 6', 'android 4'))
	.pipe(gulp.dest('./assets/css'))
	.pipe(plugins.minifyCss({ keepSpecialComments: 1 }))
	.pipe(plugins.rename({ suffix: '.min' }))
	.pipe(gulp.dest('./assets/css'))
	.pipe(plugins.notify({ message: 'Styles task complete' }))
	.pipe(reload( {stream:true} ));
});

gulp.task('bootstrapless', function(){
	return gulp.src('./assets/less/bootstrap/plusstrap.less')
	.pipe(plugins.plumber())
	.pipe(plugins.less())
	.pipe(plugins.autoprefixer('last 2 versions', 'ie 9', 'ios 6', 'android 4'))
	.pipe(gulp.dest('./assets/css'))
	.pipe(plugins.minifyCss({ keepSpecialComments: 1 }))
	.pipe(plugins.rename({ suffix: '.min' }))
	.pipe(gulp.dest('./assets/css'))
	.pipe(plugins.notify({ message: 'Styles task complete' }))
	.pipe(reload( {stream:true} ));
});

gulp.task('php', function(){
	return gulp.src('./**/*.php')
	.pipe(reload( {stream:true} ));
});


// Gulp wp rev
// var rev = require('gulp-wp-rev');
// 	gulp.task( 'rev', function () {
// 	  gulp.src('inc/script.php')
//     .pipe(rev({
//       css: "./assets/css/main.min.css",
//       cssHandle: "easy_themes__main",
//       js: "./assets/js/scripts.min.js",
//       jsHandle: "epigone_scripts"
// 	  }))
//     .pipe(gulp.dest('lib'))
// 		.pipe(reload({stream:true}));
// });

// Vendor Plugin Scripts
// gulp.task('plugins', function() {
//   return gulp.src(['assets/js/_*.js', 'assets/js/bootstrap/*.js'])
// 	.pipe(plugins.concat('scripts.js'))
// 	.pipe(gulp.dest('assets/js/'))
// 	.pipe(plugins.rename({ suffix: '.min' }))
// 	.pipe(plugins.uglify())
// 	.pipe(plugins.livereload(server))
// 	.pipe(gulp.dest('assets/js'))
// 	.pipe(plugins.notify({ message: 'Scripts task complete' }))
// 	.pipe(reload({stream:true}));
// });

// Site Scripts
gulp.task('scripts', function() {
  return gulp.src('./assets/js/themescript.js')
	.pipe(plugins.jshint('.jshintrc'))
	.pipe(plugins.jshint.reporter('default'))
	.pipe(plugins.concat('themescript.js'))
	.pipe(gulp.dest('assets/js'))
	.pipe(plugins.plumber())
	.pipe(plugins.rename({ suffix: '.min' }))
	.pipe(plugins.uglify())
	.pipe(gulp.dest('assets/js'))
	.pipe(plugins.notify({ message: 'Scripts task complete' }))
	.pipe(reload({stream:true}));
});

// Images
gulp.task('images', function() {
  return gulp.src('assets/images/**/*')
	.pipe(plugins.cache(plugins.imagemin({ optimizationLevel: 7, progressive: true, interlaced: true })))
	.pipe(gulp.dest('assets/images'))
	.pipe(plugins.notify({ message: 'Images task complete' }))
	.pipe(reload({stream:true}));
});

// Watch
gulp.task('watch',['less', 'browser-sync'] ,function() {

	// Watch .scss files
	gulp.watch('assets/less/custom/**/*.less', ['less']);
	gulp.watch('assets/less/bootstrap/*.less', ['bootstrapless']);
	gulp.watch('./**/*.css', ['bootstrapless']);

	// Watch .js files
	gulp.watch('assets/js/**/*.js', ['scripts']);

	// Watch image files
	gulp.watch('assets/images/**/*', ['images']);
	gulp.watch('./**/*.php', ['php']);


});

// Default task
// gulp.task('default', ['less', 'plugins','scripts', 'scripts', 'images', 'watch', 'rev']);
gulp.task('default', ['less', 'images', 'scripts', 'watch']);
