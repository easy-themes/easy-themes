<!doctype html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>このテーマの使い方</title>
	<link rel="stylesheet" href="assets/css/plusstrap.css">
	<script src="assets/js/jquery-1.9.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script>
	  $(function () {
	    $('#myTab a:last').tab('show');
	  })
	</script>
	<style>
	body{
		margin-top: 20px;
		width: 90%;
	}
	.nav-tabs{
		margin-bottom: 0;
		border-bottom: none;
	}
	.tab-content{
		border: 1px solid #e8e8e8;
		padding: 10px;
	}
	img {
		border: 5px solid #e8e8e8;
		width: 95%;
		margin: 10px 0;
		display: block;
	}
	.span6 {
		width: 45% !important;
		background: #444;
		color: #FFF;
		padding: 10px;
		margin: 10px;
		border-radius: 5px;
	}
	.span6 h3 {
		line-height: 1em;
		margin-top: 0;
	}
	.span6
	{
	  	position:relative;
	    -webkit-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
	       -moz-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
	            box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
	}
	</style>
</head>
<body>
	<ul class="nav nav-tabs" id="myTab">
		<li><a href="#plugin" data-toggle="tab">プラグインについて</a></li>
		<li><a href="#layout" data-toggle="tab">レイアウト機能について</a></li>
		<li><a href="#globalNav" data-toggle="tab">グローバルナビゲーションについて</a></li>
	</ul>

	<div class="tab-content row-fluid">
		<div class="tab-pane active" id="plugin">
			<h1>同梱のプラグインについて</h1>
			<p>
				このテーマでは付属のプラグインインストーラーにて、あらかじめ以下のプラグインをテーマ内に付属しています。
				<br>「外観 -> プラグインインストーラー」より、プラグインをインストールすることができます。</p>
			<h3> <i class="icon-star"></i>
				<a href="http://wordpress.org/extend/plugins/addquicktag/">AddQuickTag</a>
			</h3>
			<div class="row-fluid">
				<p>
					WordPressの投稿エディタを拡張できるプラグインです。
					<br>自分でよく使うタグを登録することができます。</p>
			</div>
			<h3> <i class="icon-star"></i>
				<a href="http://wordpress.org/plugins/facebook/">Facebook for wordpress</a>
			</h3>
			<div class="row-fluid">
				<p>
					Facebookプラグインをwordpressで簡単に表示するための、公式プラグインです。<br>
					日本語翻訳ファイルを付属していますので、アップデートをしないでください。
					<br>また、Facebookプラグインを使用するためには、必ずご自身のFacebookアカウントでアプリケーションを作成しなければいけません。<br>
					参考リンク : <a href="http://fb.dev-plus.jp/what-devplus/dev_register/">fb.developers'+</a></p>
			</div>
			<h3>
				<i class="icon-star"></i>
				<a href="http://wordpress.org/extend/plugins/ps-disable-auto-formatting/">PS Disable Auto Formatting</a>
			</h3>
			<div class="row-fluid">
				<p>
					投稿に&lt;p&gt;タグが勝手に挿入される事を防いでくれます。
					<br>ビジュアルエディタで簡単にアイコンを挿入できます。</p>
			</div>
			<h3>
				<i class="icon-star"></i>
				<a href="http://wordpress.org/plugins/all-in-one-seo-pack/">All in One SEO Pack</a>
			</h3>
			<div class="row-fluid">
				<p>
					All in One SEO Pack は、WordPressで作成したサイトのトップページや個別記事に対して、SEO（検索エンジン最適化）の対策ができるプラグインです。
					<br></p>
			</div>
			<h3>
				<i class="icon-star"></i>
				<a href="http://wordpress.org/extend/plugins/wp-social-bookmarking-light/">WP Social Bookmarking Light</a>
			</h3>
			<div class="row-fluid">
				<p>
					SNSボタンを簡単に表示するプラグインです。
					<br>デフォルトではオフになるようになっています。</p>
			</div>

			<h3>
				<i class="icon-star"></i>
				<a href="http://wordpress.org/plugins/breadcrumb-navxt/download/">Breadcrumb NavXT</a>
			</h3>
			<div class="row-fluid">
				<p>
					パンくずを表示するプラグインです。<br>
					レイアウブロック機能と連動しています。</p>
			</div>

			<h1>ウィジェットについて</h1>
			<p>
				このテーマには、いくつかの独自ウィジェットが追加してあります。
				<br>下記にて紹介させて頂きます。</p>
			<h3>
				<i class="icon-star"></i>
				プロフィールウィジェット
			</h3>
			<div class="row-fluid">
				<p>
					サイドバーに簡単に自己紹介ウィジェットを組み込むことができます。
					<br></p>
			</div>
			<h3>
				<i class="icon-star"></i>
				カスタムテキストウィジェット
			</h3>
			<div class="row-fluid">
				<p>
					記事の編集の感覚で簡単にブロックを作成出来るウィジェットです。
					</p>
			</div>

			<h3>
				<i class="icon-star"></i>
				動画コンテンツウィジェット
			</h3>
			<div class="row-fluid">
				<p>
					動画のURL・サイズを入力していただくことで、メインコンテンツ・サイドカラムに簡単にウィジェットとして動画を表示させることができます。
					</p>
			</div>

			<h3>
				<i class="icon-star"></i>
				新着情報ウィジェット
			</h3>
			<div class="row-fluid">
				<p>
					指定したカテゴリの記事を一覧として表示します。<br />
					カテゴリを指定する際には、「投稿 -> カテゴリ 」にて表示してあるIDを入力して下さい。
					</p>
			</div>

			<h3>
				<i class="icon-star"></i>
				外部リンクウィジェット
			</h3>
			<div class="row-fluid">
				<p>
					外部リンクを表示するウィジェットです。<br />
					3つまで登録することができます・
					</p>
			</div>

			<h3>
				<i class="icon-star"></i>
				バナーウィジェット
			</h3>
			<div class="row-fluid">
				<p>
					広告リンク付きバナーを表示するウィジェットです。<br />
					こちらも3つまバナーを表示させる事ができます。
					</p>
			</div>

		</div>
		<!-- レイアウト機能 -->
		<div class="tab-pane" id="layout">
			<h1>レイアウト機能について</h1>
			<p>
				このテーマでは投稿・固定ページで使用出来る「レイアウトブロック」を採用しています。
				<br>
				このブロック型のコンテンツを組み立て、webページを構築することも可能ですし、
				<br>
				通常通りに作成することも可能です。
				<br>
				<img src="assets/images/admin/layout-editer01.png" height="345" width="741" alt="">それでは、ブロックの機能をご紹介します。</p>
			<div class="row-fluid">

				<div class="span6" style="margin-left:15px;">
					<h3>
						<i class="icon-star"></i>
						本文ブロック
					</h3>
					<p>
						通常の投稿エディタに書いた内容が表示されます。
						<br></p>
				</div>
				<div class="span6">
					<h3>
						<i class="icon-star"></i>
						ブログ ブロック
					</h3>
					<p>投稿記事一覧を表示させることができます。</p>
				</div>

				<div class="span6">
					<h3>
						<i class="icon-star"></i>
						ヘッドライン ブロック
					</h3>
					<p>
						記事のタイトルを表示するブロックです。
						<br>デフォルトで表示するように設定してあります。</p>
				</div>
				<div class="span6">
					<h3>
						<i class="icon-star"></i>
						仕切り線 ブロック
					</h3>
					<p>
						コンテンツとコンテンツを仕切る際などに使用します。
						<br></p>
				</div>
				<div class="span6">
					<h3>
						<i class="icon-star"></i>
						ソーシャル ブロック
					</h3>
					<p>
						SNSボタンを表示するブロックです。
						<br>
						使用するには、「外観 -> プラグインインストーラー」からWP Social Bookmark Light プラグインをインストールしなければ使用出来ません。
					</p>
				</div>

				<div class="span6">
					<h3>
						<i class="icon-star"></i>
						コメント ブロック
					</h3>
					<p>
						コメントを表示させるブロックです。
						<br>デフォルトで表示されるように設定してあります。</p>
				</div>

				<div class="span6">
					<h3>
						<i class="icon-star"></i>
						画像 ブロック
					</h3>
					<p>画像をアップロードし、表示するブロックです。</p>
				</div>

				<div class="span6">
					<h3>
						<i class="icon-star"></i>
						お問い合わせ ブロック
					</h3>
					<p>簡易的なお問い合わせフォームを挿入できます。</p>
				</div>

				<div class="span6">
					<h3>
						<i class="icon-star"></i>
						動画ブロック
					</h3>
					<p>
						動画を表示することができます。
						<br>使用にあたっては、</p>
				</div>

				<div class="span6">
					<h3>
						<i class="icon-star"></i>
						パンくず ブロック
					</h3>
					<p>
						Breadcrumb NavXT を使用してパンくずを表示することができます。
						</p>
				</div>

				<div class="span6">
					<h3>
						<i class="icon-star"></i>
						ハーフ ブロック
					</h3>
					<p>
						カラムの半分のブロックを表示出来ます。
						<br></p>
				</div>
				<div class="span6">
					<h3>
						<i class="icon-star"></i>
						定型文 ブロック
					</h3>
					<p>
						テーマオプションの定型文設定で登録した定型文を呼び出すことができます。
						<br></p>
				</div>
				<div class="span6">
					<h3>
						<i class="icon-star"></i>
						画像選択 ブロック
					</h3>
					<p>
						デフォルトで登録している画像を挿入できるブロックです。
						<br></p>
				</div>
			</div>
		<!-- レイアウトブロック終了 -->
		</div>
		<div class="tab-pane" id="globalNav">
			<h1>グローバルナビゲーションについて</h1>
			<div class="welcomebox">
            <p>このテーマではwordpressの「メニュー機能」を使用して<strong>グローバルナビゲーション</strong>を表示しています。<br />
            以下の順序で設定を行ってください。</p>

            <ul>
              <li>1. 「外観 -> メニュー」にアクセスし、メニュー編集画面を表示する<br /><br /></li>
                <li>2. メニュー名を入力し、メニューを追加。<br><br />
              <img src="assets/images/admin/theme-option-menu-img01.png" /><br /><br />
              </li>
              <li>3.メニューに追加する項目を左のサイドバーから選択する。<br><br />

              </li>
              <li>4. テーマの場所で作成したメニュー名を選択。<br><br />
              <img src="assets/images/admin/theme-option-menu-img02.png" /><br /><br />
              </li>
            </ul>
            <p>また、上部の「表示オプション」追加する項目の表示設定ができます。
            <br /><br />
            投稿ページやタグアーカイブページをメニューに追加したい場合には必要な項目を選択してください。<br /><br />
            <br />
            <img src="assets/images/admin/theme-option-menu-img03.png" /></p>


          </div>
		</div>

	<!-- タブ終了 -->
	</div>
</body>
</html>
