<?php

/**
 * Single template.
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */

global $page_layout, $global_layout, $easythemes_layout;
$page_layout = json_decode( SmartMetaBox::get( 'content-layout' ) );
$sidebar_layout = SmartMetaBox::get( 'sidebar_layout' );
$show_page_title_text = SmartMetaBox::get( 'show_page_title_text' );

get_template_part( 'modules/head' );

easythemes_header_path();

do_action( 'get_header' );

get_template_part( 'modules/sidebar' ); ?>

<div class="contents <?php echo $easythemes_layout->get_main_class(); ?>" >

<?php
if ( have_posts() ) {
	the_post();
	get_template_part( 'templates/content-single' );
} else {
	echo '<h2>ファイルが見つかりません！</h2><p>間違ったページを見ているかもしれません？</p>';
}
?>
	</div>

<?php

get_template_part( 'modules/sidebar-right' );

easythemes_footer_path();

