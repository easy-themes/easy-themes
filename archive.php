<?php
/**
 * Archive Template
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */

global $easy_themes,$easythemes_layout;

get_template_part( 'modules/head' );

easythemes_header_path();

do_action( 'get_header' );

dynamic_sidebar( 'main-visual' );

get_template_part( 'modules/sidebar' );
?>

<div class="contents <?php echo $easythemes_layout->get_main_class(); ?>">
<?php
if ( have_posts() ) : ?>
<div class="headline">
<?php
	$post = $posts[0];
	if ( is_category() ) { ?>
		<h2 class="custom-title">
			アーカイブ &#8216;
			<?php single_cat_title(); ?>&#8217; カテゴリー</h2>
		<?php
	} elseif ( is_tag() ) { ?>
		<h2 class="custom-title">
			Posts Tagged &#8216;
			<?php single_tag_title(); ?>&#8217;</h2>
		<?php
	} elseif ( is_day() ) { ?>
		<h2 class="custom-title">
			Archive for
			<?php the_time( 'Y年F jS, ' ); ?></h2>
		<?php
		} elseif ( is_month() ) { ?>
		<h2 class="custom-title">
			<?php the_time('Y年 F'); ?>のアーカイブ</h2>
		<?php
		} elseif ( is_year() ) { ?>
		<h2 class="custom-title">
			<?php the_time('Y年'); ?>のアーカイブ</h2>
		<?php
		} elseif ( is_author() ) { ?>
		<h2 class="custom-title">投稿者アーカイブ</h2>
		<?php
		} elseif ( isset( $_GET['paged'] ) && ! empty( $_GET['paged'] ) ) { ?>
		<h2 class="custom-title">ブログアーカイブ</h2>
		<?php
		} ?></div>

<?php
while ( have_posts() ) :
	the_post();
	get_template_part( 'templates/content-archive' );

endwhile; ?>

		<div class="float-left">
			<?php next_posts_link('&laquo; 前の記事') ?></div>
		<div class="float-right">
			<?php previous_posts_link('次の記事 &raquo;') ?></div>

		<?php else : ?>

		<h2>記事が見つかりませんでした。</h2>
		<p>申し訳ありませんが、あなたが探している記事は見つかりませんでした。</p>

		<?php endif; ?>

		<?php if ( ! function_exists('dynamic_sidebar') || ! dynamic_sidebar( 5 ) ): ?>
		<?php endif; ?>

		</div>

<?php

get_template_part( 'modules/sidebar-right' );

easythemes_footer_path();
