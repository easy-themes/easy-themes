# Easy Themes

## Theme Options

* Welcome Settings
	* 使い方
		* heading
	* welcome
		*

# Option List

## general_heading

* custom_favicon
* custom_appicon
* google_analytics

## styling_option

* theme_colorpicker
* body_background
* footer_background
* copyright_background

## font_options

* font_body
* font_link
* font_h1
* font_h2
* font_h3
* font_h4
* font_h5
* font_h6

## header_option

* header_description
* header_desc_background
* header_height
* header_logo_text
* header_logo_text_style
* header_logo_media_upload

## navigation_option

* navigation_font
* navigation_color
* navigation_background
* navigation_dropdown
* navigation_margin

## information_option

* infobar_display
* info_bar_news

## footer_option

* footer_copyright
* page_top

## layout_option

* top_layout
* archive_layout
* page_layout
* single_layout
* two_column_info
* wrap_colmn_width
* main_colmn_width
* sidebar_width

## template_option

* template_text_one
* template_text_two
* template_text_three
* template_text_four
* template_text_five
* template_text_six
* template_text_seven
* template_text_eight
* template_text_nine
* template_text_ten
