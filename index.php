<?php
/**
 * Index template.
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */

global $easythemes_layout;

get_template_part( 'modules/head' );

easythemes_header_path();

do_action( 'get_header' );

dynamic_sidebar( 'main-visual' );
get_template_part( 'modules/sidebar' );?>

<div class="contents <?php echo $easythemes_layout->get_main_class(); ?>">
<?php
if ( ! dynamic_sidebar( 'main-primary' ) ) { ?>
	<p><img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo-main-image.png" alt=""></p>
<?php
	// the_widget( 'widget_movie_contents' );
	// the_widget( 'WP_Widget_Recent_Posts' );
} ?>

<?php
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();

		get_template_part( 'templates/content-archive.php' );

	}
} else { ?>

<h2>記事が見つかりませんでした。</h2>
<?php
}

		dynamic_sidebar( 'main-secondary' ); ?>

	</div>

<?php
get_template_part( 'modules/sidebar-right' );

easythemes_footer_path();
