<?php
/**
 * Single Page Contents template.
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
?>


<div class="entry-contents single--contents">

<?php
get_template_part( 'modules/page_content' );

wp_link_pages(
	array(
		'before' => '<p><strong>ページ:</strong> ',
		'after' => '</p>',
		'next_or_number' => 'number',
	)
);?>
</div>
