<?php
/**
 * Archive Page Contents Template
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
?>
<article class="row-fluid article--area">
	<?php
	if ( get_the_title() ) { ?>
		<h3 class="entry-title article--title theme_radius">
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
				<?php the_title(); ?></a>
		</h3>
	<?php
	} else { ?>
		<h3 class="entry-title article--title theme_radius">
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">タイトルなし</a>
		</h3>
	<?php
	}
	?>
	<div class="entry-contents article--contents theme_radius">
		<?php the_content( '>> 詳細ページへ' ); ?>
	</div>

	<footer class="entry-footer article--footer post-footer">
		<?php the_tags( 'タグ: ', ', ', '<br/>' ); ?>
		<br />
		<a href="<?php the_permalink() ?>" class="readmore btn theme_radius"> <i class="icon-plus-sign"></i>
			続きを読む
		</a>
		<p class="meta">
			<span class="btn theme_radius"> <i class="icon-user"></i>
				投稿者 :
				<?php the_author() ?></span>
			<span class="btn theme_radius">
				<i class="icon-tags"></i>
				<?php the_category( ' ' ); ?></span>
			<span class="btn theme_radius">
				<i class="icon-calendar"></i>
				<?php the_time( 'Y年Mj日 ' ) ?></span>
			<span class="btn theme_radius">
				<a href="<?php the_permalink(); ?>#comments" title="comments">
					<i class="icon-comment"></i>
					<?php comments_number( 'コメント無し', '1コメント', '% コメント' ); ?></a>
			</span>
	</footer>
</article>
