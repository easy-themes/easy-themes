<?php
/**
 * Static Page contents template.
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
?>

<div class="entry-contents page--contents">
<?php
// the_content();
get_template_part( 'modules/page_content' );
wp_link_pages(
	array(
		'before' => '<p><strong>ページ:</strong> ',
		'after' => '</p>',
		'next_or_number' => 'number',
	)
);?>
</div>
