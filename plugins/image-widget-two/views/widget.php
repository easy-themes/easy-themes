<?php
/**
 * Widget template. This template can be overriden using the "sp_template_image-widget_widget.php" filter.
 * See the readme.txt file for more info.
 */

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');

$before_widget = '<div class="widget ads-banner-widget">';
echo $before_widget;


if ( !empty( $title ) ) { echo $before_title . $title . $after_title; }

echo $this->get_image_html( $instance, true );

if ( !empty( $description ) ) {
	echo '<div class="description" >';
	echo wpautop( $description );
	echo "</div>";
} else {
  echo '<div class="description" >';
  echo '';
  echo "</div>";
}

if ( !empty( $instance['button'] ) ) {
  echo '<a href="'.$instance['link'].'" class="button">';
    echo $instance['button'];
  echo "</a>";
} else {
  echo '<a href="'.$instance['link'].'" class="button">';
  echo ' ご購入はコチラから';
  echo "</a>";
}
$after_widget = '</div>';
echo $after_widget;


?>
