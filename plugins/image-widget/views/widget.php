<?php
/**
 * Widget template. This template can be overriden using the "sp_template_image-widget_widget.php" filter.
 * See the readme.txt file for more info.
 */

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');

echo $before_widget;

if ( !empty( $title ) ) { echo $before_title . $title . $after_title; }

echo $this->get_image_html( $instance, true );

if ( !empty( $description ) ) {
	echo '<div class="description" >';
	echo wpautop( $description );
	echo "</div>";
} else {
  echo '<div class="description" >';
  echo 'ここに説明が入ります。';
  echo "</div>";
}

if ( !empty( $instance['button'] ) ) {
  echo '<a href="'.$instance['link'].'" class="button">';
  echo $instance['button'];
  echo "</a>";
} else {
  echo '<a href="'.$instance['link'].'" class="button">';
  echo 'プロフィール詳細はコチラから';
  echo "</a>";
}
echo $after_widget;
?>
