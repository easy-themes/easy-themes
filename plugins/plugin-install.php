<?php
/**
 * Plugin Activate
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */

add_action( 'tgmpa_register', 'register_required_plugins' );

function register_required_plugins() {
		$plugins = array(
				array(
						'name'               => 'Visual Editor',
						'slug'               => 'black-studio-tinymce-widget',
						'required'           => true,
						'source'             => 'http://downloads.wordpress.org/plugin/black-studio-tinymce-widget.1.3.3.zip',
						'force_activation'   => false,
						'force_deactivation' => false,
				),
				array(
						'name'               => 'wp pagenavi',
						'slug'               => 'wp-pagenavi',
						'required'           => true,
						'source'             => 'http://downloads.wordpress.org/plugin/wp-pagenavi.zip',
						'force_activation'   => false,
						'force_deactivation' => false,
				),
				array(
						'name'               => 'Breadcrumb NavXT',
						'slug'               => 'breadcrumb-navxt',
						'source'             => 'http://downloads.wordpress.org/plugin/breadcrumb-navxt.zip',
						'required'           => true,
						'force_activation'   => false,
						'force_deactivation' => false,
				),
				array(
						'name'               => 'Intuitive Custom Post Order',
						'slug'               => 'intuitive-custom-post-order',
						'source'             => 'http://downloads.wordpress.org/plugin/intuitive-custom-post-order.zip',
						'required'           => true,
						'force_activation'   => false,
						'force_deactivation' => false,
				),
				array(
						'name'               => 'Duplicate Post',
						'slug'               => 'duplicate-post',
						'source'             => 'http://downloads.wordpress.org/plugin/duplicate-post.zip',
						'required'           => true,
						'force_activation'   => false,
						'force_deactivation' => false,
				),
				array(
						'name'               => 'Google XML Sitemaps',
						'slug'               => 'google-sitemap-generator',
						'source'             => 'http://downloads.wordpress.org/plugin/google-sitemap-generator.zip',
						'required'           => true,
						'force_activation'   => false,
						'force_deactivation' => false,
				),
				array(
						'name'               => 'Display Widgets',
						'slug'               => 'display-widgets',
						'source'             => 'http://downloads.wordpress.org/plugin/display-widgets.2.03.zip',
						'required'           => true,
						'force_activation'   => true,
						'force_deactivation' => false,
				),
				array(
						'name'               => 'WP Social Bookmarking Light',
						'slug'               => 'wp-social-bookmarking-light',
						'source'             => 'http://downloads.wordpress.org/plugin/wp-social-bookmarking-light.zip',
						'required'           => true,
						'force_activation'   => true,
						'force_deactivation' => false,
				),
		);

	 /**
		 * Array of configuration settings. Amend each line as needed.
		 * If you want the default strings to be available under your own theme domain,
		 * leave the strings uncommented.
		 * Some of the strings are added into a sprintf, so see the comments at the
		 * end of each line for what each argument will be.
		 */
		$config = array(
				'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
				'default_path' => '',                      // Default absolute path to pre-packaged plugins.
				'menu'         => 'tgmpa-install-plugins', // Menu slug.
				'has_notices'  => true,                    // Show admin notices or not.
				'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
				'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
				'is_automatic' => false,                   // Automatically activate plugins after installation or not.
				'message'      => '',                      // Message to output right before the plugins table.
				'strings'      => array(
						'page_title'                      => __( 'おすすめのプラグインインストール', 'tgmpa' ),
						'menu_title'                      => __( 'プラグインインストール', 'tgmpa' ),
						'installing'                      => __( 'プラグインをインストールしています: %s', 'tgmpa' ), // %s = plugin name.
						'oops'                            => __( 'Something went wrong with the plugin API.', 'tgmpa' ),
						'notice_can_install_required'     => _n_noop( 'このテーマは次のプラグインを必要としています: %1$s.', 'このテーマは次のプラグインを必要としています: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
						'notice_can_install_recommended'  => _n_noop( 'このテーマでは、次のプラグインをおすすめしています。: %1$s.', 'このテーマは次のプラグインをおすすめしています: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
						'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'tgmpa' ), // %1$s = plugin name(s).
						'notice_can_activate_required'    => _n_noop( 'このテーマに必要なプラグインが有効化されていません。: %1$s.', 'このテーマに必要なプラグインが有効化されていません。: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
						'notice_can_activate_recommended' => _n_noop( 'このテーマに必要なプラグインが有効化されていません。: %1$s.', 'このテーマに必要なプラグインが有効化されていません。: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
						'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'tgmpa' ), // %1$s = plugin name(s).
						'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
						'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'tgmpa' ), // %1$s = plugin name(s).
						'install_link'                    => _n_noop( 'プラグインのインストールを始める。', 'プラグインのインストールを始める', 'tgmpa' ),
						'activate_link'                   => _n_noop( 'プラグインの有効化を始める', 'プラグインの有効化を始める', 'tgmpa' ),
						'return'                          => __( 'プラグインインストーラーに戻る', 'tgmpa' ),
						'plugin_activated'                => __( 'プラグインの有効化に成功しました。', 'tgmpa' ),
						'complete'                        => __( 'すべてのプラグインのインストールと有効化に成功しました。 %s', 'tgmpa' ), // %s = dashboard link.
						'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
				)
		);

		tgmpa( $plugins, $config );

}

