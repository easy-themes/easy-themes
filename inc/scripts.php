<?php

/**
 * Script enqueue
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */

add_action( 'wp_enqueue_scripts', 'easythemes_scripts', 100 );

function easythemes_scripts() {

	wp_enqueue_style( 'easythemes_main', get_stylesheet_directory_uri() . '/assets/css/custom.min.css', false, '99972085bc30c435929f5af3cf81d064' );
	wp_enqueue_style( 'easythemes_plusstrap', get_stylesheet_directory_uri() . '/assets/css/plusstrap.min.css', false, '99972085bc30c435929f5af3cf81d064' );
	wp_register_script( 'easythemes_bootstrap', get_stylesheet_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ), '', true );
	wp_register_script( 'easythemes_scripts', get_stylesheet_directory_uri() . '/assets/js/themescript.min.js', array( 'jquery' ), '632995d66dba190b04e58c7bbf9d6222', true );

	if ( ! is_admin() && current_theme_supports( 'jquery-cdn' ) ) {
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js', array(), null, false );
		add_filter( 'script_loader_src', 'easythemes_jquery_local_fallback', 10, 2 );
	}

	if ( is_single() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'easythemes_bootstrap' );
	if ( ! easythemes_is_mobile() ) {
		wp_enqueue_script( 'easythemes_scripts' );
	}
}

/**
 * jQuery local fallback
 *
 */

add_action( 'wp_head', 'easythemes_jquery_local_fallback' );

function easythemes_jquery_local_fallback( $src, $handle = null ) {
	static $add_jquery_fallback = false;

	if ( $add_jquery_fallback ) {
		echo '<script>window.jQuery || document.write(\'<script src="' . get_template_directory_uri() . '/assets/js/vendor/jquery-1.11.0.min.js"><\/script>\')</script>' . "\n";
		$add_jquery_fallback = false;
	}

	if ( $handle === 'jquery' ) {
		$add_jquery_fallback = true;
	}

	return $src;
}
