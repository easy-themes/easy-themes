<?php
/**
 * Register Widgets
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */


add_action( 'widgets_init', function(){ register_widget( 'Widget_Category_News' ); } );

add_action( 'widgets_init', function(){ register_widget( 'Widget_Mailmag' ); } );


/**
 * Recent News
 */

class Widget_Category_News extends WP_Widget
{

	/**
	 * __construct
	 */

	public function __construct()
	{

		$widget_ops = array(
			'description' => __( '特定のカテゴリーの最新情報を表示', 'easythemes' ),
		);

		parent::WP_Widget( false, $name = __( '最新情報', 'easythemes' ), $widget_ops );

	}

	/**
	 * Widget
	 * @param  [type] $args     [description]
	 * @param  [type] $instance [description]
	 * @return [type]           [description]
	 */
	public function widget( $args, $instance )
	{
		global $post;
		extract( $args );

		echo wp_kses_post( $before_widget );
		echo wp_kses_post( $before_title );

		if ( $instance['news_title'] ) {

			echo wp_kses_post( $instance['news_title'] );

		} elseif ( $instance['news_cate'] == null ) {

			echo wp_kses_post( get_catname( '1' ) );

		} else {
			echo wp_kses_post( get_catname( $instance['news_cate'] ) );
		}; ?>

		<span class="wrap-button">
		<a class="button-all" href="<?php
		if ( $instance['news_cate'] != null ) {
			echo get_category_link( $instance['news_cate'] );
		} else {
			echo get_category_link( 1 );
		} ?>" title="<?php echo wp_kses_post( get_catname( $instance['news_cate'] ) ); ?><?php _e( '>>> 全記事を見る', 'easythemes' ) ;?>, </a>
		</span>

		<?php
		echo wp_kses_post( $after_title ) ?>

		<div class="entryicon">
		<?php
		if ( $instance['news_cate'] != null ) {
			$this_query = new WP_Query( 'cat=' . $instance['news_cate'] . '&showposts=' . $instance['news_count'] );
		} else {
			$this_query = new WP_Query( 'cat=1&showposts=3' );
		} ?>
			<div class="f-clear">
		<?php
		if ( $this_query->have_posts() ) :
			while ( $this_query->have_posts() ) :
				$this_query->the_post(); ?>
				<dl class="clearfix category_news--area">
					<dt class="category_news--thumbnail">
						<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
						<?php
						$thumbsize = 'entryicon';
						if ( has_post_thumbnail() ) {
								the_post_thumbnail( $thumbsize );
						} else {
							$imgset = array(
								'post_type' => 'attachment',
								'post_mime_type' => 'image',
								'post_parent' => $post->ID,
								'numberposts' => '1',
							);

							$images = get_children( $imgset );
							$image = array_shift( $images );

							if ( $image ) {
								echo wp_get_attachment_image( $image->ID, $thumbsize );
							}
						}; ?>
						</a>
						<!-- /.thumb -->
						</dt>

						<dd  class="category_news--contents">
							<span class="metadate"><?php the_time( 'Y.n.j' ); ?></span>
							<h3 class="category_news--title">
								<a href="<?php the_permalink() ?>" title="">
									<?php echo wp_kses_post( mb_substr( the_title_attribute( 'echo=0' ) , 0, 38 ) ); ?>
								</a>
							</h3>
							<p><?php echo wp_kses_post( mb_substr( get_the_excerpt() , 0, 36 ) ); ?>......</p>
							<p class="text-right">
								<a href="<?php the_permalink(); ?>" class="btn btn-more"> <?php _e( '詳細を見る', 'easythemes' ); ?></a>
							</p>
						</dd>
					</dl>
										<?php
			endwhile;
		else : ?>
					<p><?php _e( '該当する記事はありません｡', 'easythemes' );?></p>
				<?php
		endif;
		wp_reset_postdata(); ?>
				</div>
			</div>
				<?php echo $after_widget;
	}

	/** @see WP_Widget::update */
	public function update( $new_instance, $old_instance )
	{
		return $new_instance;
	}

	/** @see WP_Widget::form */
	public function form( $instance )
	{
		$news_title = isset( $instance['news_title']) ? $instance['news_title'] : '';
		$news_cate  = isset( $instance['news_cate'])  ? $instance['news_cate']  : 1;
		$news_count = isset( $instance['news_count']) ? $instance['news_count'] : 10;
?>

		<p><label for="<?php echo esc_attr( $this->get_field_id( 'news_title' ) ); ?>">
				タイトルの設定:<br/ ><small>タイトルを変更する場合に入力して下さい（未記入の場合はカテゴリ名）</small>
				<input class="widefat" id="<?php echo $this->get_field_id( 'news_title' ); ?>" name="<?php echo $this->get_field_name('news_title'); ?>" type="text" value="<?php echo $news_title; ?>" />
		</label></p>

		<p>
			<label for="<?php echo $this->get_field_id( 'news_cate' ); ?>">
				<?php _e('記事カテゴリーナンバー:<br/ ><small>記事を取得するカテゴリナンバーを指定して下さい。</small>'); ?>
			<input class="widefat" id="<?php echo $this->get_field_id( 'news_cate' ); ?>" name="<?php echo $this->get_field_name( 'news_cate' ); ?>" type="text" value="<?php echo $news_cate; ?>" />
		</label></p>

		<p><label for="<?php echo $this->get_field_id('news_count'); ?>">
				<?php _e('記事取得数:<br/ ><small>記事を取得する数を指定して下さい。</small>'); ?>
				<input class="widefat" id="<?php echo $this->get_field_id('news_count'); ?>" name="<?php echo $this->get_field_name('news_count'); ?>" type="text" value="<?php echo $news_count; ?>" />
		</label></p>

				<?php
	}
}


/*--------------------------------------------------
		3.ピックアップ記事
--------------------------------------------------*/

class Widget_Mailmag extends WP_Widget
{

	/**
	 * construct
	 */

	public function __construct()
	{
		$widget_ops = array(
			'description' => 'ピックアップ記事（特定のカテゴリーの最新記事）を1つ表示',
		);

		parent::WP_Widget( false, $name = 'ピックアップ記事', $widget_ops );

	}

	public function widget( $args, $instance )
	{
		global $post;

		extract( $args );
		echo wp_kses_post( $before_widget );
		if ( $instance['news_one'] ) {
			$this_query = new WP_Query( 'cat=' . $instance['news_one'] . '&showposts=1' );
		} else {
			$this_query = new WP_Query( 'cat=1&showposts=1' );
		} ?>
		<?php
		echo wp_kses_post( $before_title ); ?>

		<?php
		if ( $instance['news_title'] ) {
			echo wp_kses_post( $instance['news_title'] );

		} elseif ( $instance['news_one'] ) {
			echo wp_kses_post( get_catname( $instance['news_one'] ) );
		} else {
			echo wp_kses_post( get_catname( '1' ) );
		}; ?>
		<?php
		echo wp_kses_post( $after_title );

		if ( $this_query->have_posts() ) :
			while ( $this_query->have_posts() ) :
				$this_query->the_post(); ?>

			<div class="pickup2 pickup--area  clear-fix">

				<p class="thumb pickup--thumbnail">
					<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
					<?php
					$thumbsize = 'pic';
					if ( has_post_thumbnail() ) {
						the_post_thumbnail( $thumbsize );
					} else {
						$imgset = array(
							'post_type' => 'attachment',
							'post_mime_type' => 'image',
							'post_parent' => $post->ID,
							'numberposts' => '1'
						);
						$images = get_children( $imgset );
						$image = array_shift( $images );
						if ( $image ){
							echo wp_get_attachment_image( $image->ID, $thumbsize );
						} else { ?>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/pic_thumb.png" alt="No Image" />
						<?php
						}
					} ?>
					</a>
				</p>
				<div class="pickup--contents">
					<p class="metadate"><?php the_time( 'Y.n.j' ); ?></p>

					<h3>
						<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
							<?php the_title_attribute(); ?>
						</a>
					</h3>
					<p><?php
					echo mb_substr( get_the_excerpt() , 0, 60 ); ?>...</p>
					<p class="more"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">>> 続きを読む</a></p>
				</div>
			</div>

		<?php
			endwhile;
		endif;
		echo $after_widget;
	}

	/** @see WP_Widget::update */
	public function update( $new_instance, $old_instance )
	{
		return $new_instance;
	}

	/** @see WP_Widget::form */
	public function form( $instance )
	{
		$news_one = isset($instance['news_one']) ? $instance['news_one'] : 1;
		$news_title = isset($instance['news_title']) ? $instance['news_title'] : '';
?>

				<p><label for="<?php echo $this->get_field_id( 'news_title' ); ?>">
						<?php _e( 'タイトルの設定:<br/ ><small>タイトルを変更する場合に入力して下さい（未記入の場合はカテゴリ名）</small>' ); ?>
						<input class="widefat" id="<?php echo $this->get_field_id('news_title'); ?>" name="<?php echo $this->get_field_name( 'news_title' ); ?>" type="text" value="<?php echo $news_title; ?>" />
				</label></p>

				<p><label for="<?php echo $this->get_field_id( 'news_one' ); ?>">
						<?php _e( '記事カテゴリーナンバー:<br/ ><small>記事を取得するカテゴリナンバーを指定して下さい。</small>' ); ?>
						<input class="widefat" id="<?php echo $this->get_field_id( 'news_one' ); ?>" name="<?php echo $this->get_field_name( 'news_one' ); ?>" type="text" value="<?php echo $news_one; ?>" />
				</label></p>

				<?php
	}
}


/*--------------------------------------------------
		5.外部リンクウィジェット
--------------------------------------------------*/

class widget_ds_commercial extends WP_Widget
{
	/** constructor */
	public function widget_ds_commercial()
	{
		$widget_ops = array(
			'description' => '外部リンクを3つまで設定できます。'
		);
		parent::WP_Widget( false, $name = '外部リンクウィジェット', $widget_ops );
	}

	/** @see WP_Widget::widget */
	public function widget( $args, $instance )
	{
		extract( $args );

		echo $before_widget;

		echo $before_title;

		if ( $instance['title'] == null ) {
			echo '外部リンク';
		} else {
			echo $instance['title'];
		}
		echo $after_title; ?>

			<ul>
						<li><a href="<?php echo $instance['commercial_url_1']; ?>">
						<?php if ($instance['commercial_image_1'] == null) {
			echo '外部リンク1';
		} else {
			echo $instance['commercial_image_1'];
		} ?>-
						<?php if ($instance['commercial_text_1'] == null) {
			echo "ここにリンク先の紹介テキストが入ります。";
		} else {
			echo $instance['commercial_text_1'];
		} ?>
						</a></li>

						<?php if ($instance['commercial_url_2'] == !null): ?>
						<li><a href="<?php echo $instance['commercial_url_2']; ?>">
						<?php if ($instance['commercial_image_2'] == null) {
				echo '';
			} else {
				echo $instance['commercial_image_2'];
			} ?>-
						<?php if ($instance['commercial_text_2'] == null) {
				echo "";
			} else {
				echo $instance['commercial_text_2'];
			} ?>
						</a></li>
						<?php
		endif; ?>

						<?php if ($instance['commercial_url_3'] == !null): ?>
						<li><a href="<?php echo $instance['commercial_url_3']; ?>">
						<?php if ($instance['commercial_image_3'] == null) {
				echo '';
			} else {
				echo $instance['commercial_image_3'];
			} ?>-
						<?php if ($instance['commercial_text_3'] == null) {
				echo "";
			} else {
				echo $instance['commercial_text_3'];
			} ?>
						</a></li><?php
		endif; ?>
						</ul>

						<?php echo $after_widget ?>
				<?php
	}

	/** @see WP_Widget::update */
	public function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		$instance['movie_title'] = strip_tags($new_instance['movie_title']);
		$instance['movie_url'] = strip_tags($new_instance['movie_url']);
		$instance['movie_size'] = strip_tags($new_instance['movie_size']);
		$instance['movie_text'] = strip_tags($new_instance['movie_text']);
		$instance['movie_link'] = strip_tags($new_instance['movie_link']);

		return $instance;
		return $new_instance;
	}

	/** @see WP_Widget::form */
	public function form($instance)
	{
		$title = $instance['title'] ? $instance['title'] : "外部リンク";
		$commercial_image_1 = $instance['commercial_image_1'] ? $instance['commercial_image_1'] : "外部リンク1";
		$commercial_url_1 = $instance['commercial_url_1'] ? $instance['commercial_url_1'] : "";
		$commercial_text_1 = $instance['commercial_text_1'] ? $instance['commercial_text_1'] : "ここにリンク先1の紹介テキストが入ります。";
		$commercial_image_2 = $instance['commercial_image_2'] ? $instance['commercial_image_2'] : "";
		$commercial_url_2 = $instance['commercial_url_2'] ? $instance['commercial_url_2'] : "";
		$commercial_text_2 = $instance['commercial_text_2'] ? $instance['commercial_text_2'] : "";
		$commercial_image_3 = $instance['commercial_image_3'] ? $instance['commercial_image_3'] : "";
		$commercial_url_3 = $instance['commercial_url_3'] ? $instance['commercial_url_3'] : "";
		$commercial_text_3 = $instance['commercial_text_3'] ? $instance['commercial_text_3'] : "";
?>

				<p><label for="<?php echo $this->get_field_id('title'); ?>">
						<?php _e('リンク集タイトル:<br/ ><small>リンク集のタイトルの文字列を入力して下さい。</small>'); ?>
						<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
				</label></p>

				<hr />

				<p><label for="<?php echo $this->get_field_id('commercial_image_1'); ?>">
						<?php _e('リンク先（1）:<br/ ><small>リンク先（1）の名前を入力して下さい。</small>'); ?>
						<input class="widefat" id="<?php echo $this->get_field_id('commercial_image_1'); ?>" name="<?php echo $this->get_field_name('commercial_image_1'); ?>" type="text" value="<?php echo $commercial_image_1; ?>" />
				</label></p>

				<p><label for="<?php echo $this->get_field_id('commercial_url_1'); ?>">
						<?php _e('URL（1）:<br/ ><small>リンク先（1）のURLを入力して下さい。</small>'); ?>
						<input class="widefat" id="<?php echo $this->get_field_id('commercial_url_1'); ?>" name="<?php echo $this->get_field_name('commercial_url_1'); ?>" type="text" value="<?php echo $commercial_url_1; ?>" />
				</label></p>

				<p><label for="<?php echo $this->get_field_id('commercial_text_1'); ?>">
						<?php _e('説明文（1）:<br/ ><small>リンク先（1）の説明文を入力して下さい。</small>'); ?>
						<input class="widefat" id="<?php echo $this->get_field_id('commercial_text_1'); ?>" name="<?php echo $this->get_field_name('commercial_text_1'); ?>" type="text" value="<?php echo $commercial_text_1; ?>" />
				</label></p>

				<hr />

				<p><label for="<?php echo $this->get_field_id('commercial_image_2'); ?>">
						<?php _e('リンク先（2）:<br/ ><small>リンク先（2）の名前を入力して下さい。</small>'); ?>
						<input class="widefat" id="<?php echo $this->get_field_id('commercial_image_2'); ?>" name="<?php echo $this->get_field_name('commercial_image_2'); ?>" type="text" value="<?php echo $commercial_image_2; ?>" />
				</label></p>

				<p><label for="<?php echo $this->get_field_id('commercial_url_2'); ?>">
						<?php _e('URL（2）:<br/ ><small>リンク先（2）のURLを入力して下さい。</small>'); ?>
						<input class="widefat" id="<?php echo $this->get_field_id('commercial_url_2'); ?>" name="<?php echo $this->get_field_name('commercial_url_2'); ?>" type="text" value="<?php echo $commercial_url_2; ?>" />
				</label></p>

				<p><label for="<?php echo $this->get_field_id('commercial_text_2'); ?>">
						<?php _e('説明文（2）:<br/ ><small>リンク先（2）の説明文を入力して下さい。</small>'); ?>
						<input class="widefat" id="<?php echo $this->get_field_id('commercial_text_2'); ?>" name="<?php echo $this->get_field_name('commercial_text_2'); ?>" type="text" value="<?php echo $commercial_text_2; ?>" />
				</label></p>

				<hr />

				<p><label for="<?php echo $this->get_field_id('commercial_image_3'); ?>">
						<?php _e('リンク先（3）:<br/ ><small>リンク先（3）の名前を入力して下さい。</small>'); ?>
						<input class="widefat" id="<?php echo $this->get_field_id('commercial_image_3'); ?>" name="<?php echo $this->get_field_name('commercial_image_3'); ?>" type="text" value="<?php echo $commercial_image_3; ?>" />
				</label></p>

				<p><label for="<?php echo $this->get_field_id('commercial_url_3'); ?>">
						<?php _e('URL（3）:<br/ ><small>リンク先（3）のURLを入力して下さい。</small>'); ?>
						<input class="widefat" id="<?php echo $this->get_field_id('commercial_url_3'); ?>" name="<?php echo $this->get_field_name('commercial_url_3'); ?>" type="text" value="<?php echo $commercial_url_3; ?>" />
				</label></p>

				<p><label for="<?php echo $this->get_field_id('commercial_text_3'); ?>">
						<?php _e('説明文（3）:<br/ ><small>リンク先（3）の説明文を入力して下さい。</small>'); ?>
						<input class="widefat" id="<?php echo $this->get_field_id('commercial_text_3'); ?>" name="<?php echo $this->get_field_name('commercial_text_3'); ?>" type="text" value="<?php echo $commercial_text_3; ?>" />
				</label></p>

				<?php
	}
}

add_action('widgets_init', create_function('', 'return register_widget("widget_ds_commercial");'));

/*--------------------------------------------------
		6.動画ウィジェット
--------------------------------------------------*/

class widget_movie_contents extends WP_Widget
{
	/** constructor */
	public function widget_movie_contents()
	{
		$widget_ops = array(
			'description' => '動画コンテンツを表示'
		);
		parent::WP_Widget(false, $name = '動画コンテンツ', $widget_ops);
	}
	public function widget($args, $instance)
	{
		extract($args);
?>

				<?php if ($instance["movie_url"] != null) { ?>
					<?php echo $before_widget ?>
					<?php echo $before_title ?>
					<?php if ($instance['movie_title']) {
				echo $instance['movie_title'];
			} else {
				echo '動画コンテンツ';
			}; ?>
					<?php echo $after_title ?>
					<?php switch ($instance["movie_size"]) {
				case 0:
					$size = 'width="335" height="250"';
				break;
				case 1:
					$size = 'width="440" height="280"';
				break;
				case 2:
					$size = 'width="670" height="350"';
				break;
			}; ?>
					<div id="movie-widget" class="clear-fix">
					<iframe <?php echo $size; ?> src="<?php echo $instance["movie_url"]; ?>" frameborder="0" allowfullscreen class="mt10 movie-frame"></iframe>
					<?php if ($instance['movie_text']) { ?>
						<div class="movie-text-area" class="mt20" style="<?php if ($instance["movie_size"] == 0) {
					echo 'width:46%;float:right;';
				} elseif ($instance["movie_size"] == 1) {
					echo 'width:30%;float:right';
				} else {
					echo 'none';
				}; ?>"><?php echo $instance['movie_text']; ?>
						<?php
			} ?>
					<?php
					if ( $instance['movie_link'] ) { ?>
						<a href="<?php echo esc_url( $instance['movie_link'] ); ?>" class="button">>> 詳細記事を見る</a>
					<?php
			} ?>
						</div>
					</div>
					<?php
			echo $after_widget;

		} else {

			echo $before_widget;
			echo $before_title;
			echo '動画コンテンツ';
			echo $after_title;
			echo '<img src="' . get_template_directory_uri() . '/assets/images/new_thumb.png"  class="movie-frame mt20"/>'; ?>

			<?php
			echo '<div class="movie-text-area mt20" style="width:35%;float:right;">ここは動画ウィジェットです。管理画面のウィジェット -> 動画コンテンツから設定ができます。<br />
				 <a href="#" class="button" ><img src="' . get_template_directory_uri() . '/assets/images/read-more01.png" alt="続きを読む"></a></div>'; ?>
				 <?php echo $after_widget ?>
		 <?php
		}
	}
	public function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;
		$instance['movie_title'] = strip_tags( $new_instance['movie_title'] );
		$instance['movie_url']   = strip_tags( $new_instance['movie_url'] );
		$instance['movie_size']  = strip_tags( $new_instance['movie_size'] );
		$instance['movie_text']  = strip_tags( $new_instance['movie_text'] );
		$instance['movie_link']  = strip_tags( $new_instance['movie_link'] );

		return $instance;
	}
	public function form( $instance )
	{
		$movie_title = esc_attr( $instance['movie_title'] );
		$movie_url   = esc_attr( $instance['movie_url'] );
		$movie_size  = esc_attr( $instance['movie_size'] );
		$movie_text  = esc_attr( $instance['movie_text'] );
		$movie_link  = esc_attr( $instance['movie_link'] );
		?>

		<p>
			<label for="<?php
				echo $this->get_field_id( 'movie_title' ); ?>">
				<?php
				_e( 'タイトルの設定:<br/ ><small>タイトルを変更する場合に入力して下さい（未記入の場合は動画コンテンツ）</small>' ); ?>
				<input class="movie-widget" id="<?php echo $this->get_field_id( 'movie_title' ); ?>" name="<?php echo $this->get_field_name('movie_title'); ?>" type="text" value="<?php echo $movie_title; ?>" size="17" />
			</label>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'movie_url' ); ?>">
				<?php _e( '動画URL設定:<br/ ><small>動画のURLを入力してください。<br />
				＊iFrame用url<br />(例：http://www.youtube.com/embed/.........：http://player.vimeo.com/video/...........)</small>'); ?>
				<input class="movie-widget" id="<?php echo $this->get_field_id('movie_url'); ?>" name="<?php echo $this->get_field_name('movie_url'); ?>" type="text" value="<?php echo $movie_url; ?>" size="17"  />
			</label>
		</p>

		<p>
		<?php _e( '動画サイズ:<br/ ><small>下のアスペクト比から動画サイズを選んでください。</small>' ); ?><br />
			<input class="movie-widget" id="<?php echo $this->get_field_id( 'movie_size' ); ?>" name="<?php echo $this->get_field_name('movie_size'); ?>" type="radio" <?php checked('0', $movie_size); ?> value="0" />
				335px × 250px<br />
			 <input class="movie-widget" id="<?php echo $this->get_field_id( 'movie_size' ); ?>" name="<?php echo $this->get_field_name('movie_size'); ?>" type="radio" <?php checked('1', $movie_size); ?> value="1" />
				440px × 280px<br />
			 <input class="movie-widget" id="<?php echo $this->get_field_id( 'movie_size' ); ?>" name="<?php echo $this->get_field_name('movie_size'); ?>" type="radio" <?php checked('2', $movie_size); ?> value='2' />
				670px × 350px<br />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'movie_text' ); ?>">
				<?php
				_e( '動画説明テキスト:<br/ ><small>動画の説明文を入力して下さい。<br />
				HTMLタグが使用できます。</small>' ); ?>
				<textarea class="widefat" id="<?php echo $this->get_field_id('movie_text'); ?>" name="<?php echo $this->get_field_name( 'movie_text' ); ?>" value="<?php echo $movie_text; ?>" rows="10" cols="16">
					<?php echo $movie_text; ?>
				</textarea>
			</label>
		</p>
		<p><label for="<?php echo $this->get_field_id('movie_link'); ?>">
			リンク : 詳細記事へのリンクを入力してください。<br>
				<input class="movie-widget" id="<?php echo $this->get_field_id('movie_link'); ?>" name="<?php echo $this->get_field_name('movie_link'); ?>" type="text" value="<?php echo $movie_link; ?>" size="30" />
		</label>
		</p>

				<?php
	}
}

add_action('widgets_init', create_function('', 'return register_widget("widget_movie_contents");'));

/*--------------------------------------------------
		7.メルマガウィジェット
--------------------------------------------------*/

class widget_mail_register_contents extends WP_Widget
{
	/** constructor */
	public function widget_mail_register_contents()
	{
		$widget_ops = array(
			'description' => 'メールマガジンフォームを表示 ( メインカラムのみ対応 )'
		);
		parent::WP_Widget(false, $name = 'メールマガジンフォームウィジェット', $widget_ops);
	}
	public function widget($args, $instance)
	{
		extract($args);
?>

				<?php if ($instance["mail_register_title"] != null) { ?>
					<?php echo $before_widget ?>
						<div class="clear-fix"></div>
							<?php global $data; ?>
						<!-- スクイーズページ -->
						<div class="content">
							<div class="main-content">
								<div class="optin" id="opt">
									<div class="row-fluid" style="background: <?php echo $data['theme_colorpicker']; ?>);">
									<h3 class="squeeze-title" style="background: <?php echo $data['theme_colorpicker']; ?>"><?php echo $instance['mail_register_title']; ?></h3>
										<?php
			$optin_form_parsed = squeeze_parse_optin_form($instance['mail_register_form']);
			$optin_form_action = $optin_form_parsed['action'];
			unset($optin_form_parsed['action']);
			$optin_form_hidden_fields = $optin_form_parsed;
			$optin_form_name_emails_fields = squeeze_name_email_fields($instance['mail_register_type']);
			$optin_form_name = $optin_form_name_emails_fields['name'];
			$optin_form_email = $optin_form_name_emails_fields['email'];
?>
										<div style="color:#000;margin-bottom:10px;text-shadow:1px 1px #fff; border-color: <?php echo $squeeze_design_type; ?>;" class="bold center font-16 squeeze-box">
										<p class="text-center">
											<strong><?php echo $instance['mail_register_text']; ?></strong>
										</p>
											<form action="<?php echo $optin_form_action ?> " method="post">
												<?php
			foreach ($optin_form_hidden_fields as $key => $value) {
				echo "
												<input type=\"hidden\" name=\"$key\" value=\"$value\" />
												";
			}
?>
												<label for="<?php echo $optin_form_name ?>" class="text form-icon">氏名 <input class="text" type="text" name="<?php echo $optin_form_name ?>" id="name" value="お名前を入力して下さい" onclick="if(this.value=='お名前を入力して下さい')this.value='';" onblur="if(this.value=='')this.value='お名前を入力して下さい';"></label>
												<label for="<?php echo $optin_form_name ?>" class="text form-icon">メールアドレス <input class="text" type="text" name="<?php echo $optin_form_email ?>" id="email" value="メールアドレスを入力して下さい" onclick="if(this.value=='メールアドレスを入力して下さい')this.value='';" onblur="if(this.value=='')this.value='メールアドレスを入力して下さい';"></label>
												<p class="text-center">
												<input class="button btn" type="submit" alt="Submit Form" value=" <?php if ($instance['mail_register_button_text']) {
				echo $instance['mail_register_button_text'];
			} else {
				echo '送信する';
			} ?>">
												</p>
											</form>
									</div>
								</div>
							</div>
						</div>
						</div>
						<!-- スクイーズページ -->

						<div class="clear-fix"></div>

				 <?php echo $after_widget ?>
		 <?php
		}
	}
	public function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		$instance['mail_register_title'] = strip_tags($new_instance['mail_register_title']);
		$instance['mail_register_text'] = strip_tags($new_instance['mail_register_text']);
		$instance['mail_register_type'] = strip_tags($new_instance['mail_register_type']);
		$instance['mail_register_form'] = $new_instance['mail_register_form'];
		$instance['mail_register_button_text'] = strip_tags($new_instance['mail_register_button_text']);

		return $instance;
	}
	public function form($instance)
	{
		$mail_register_title = esc_attr($instance['mail_register_title']);
		$mail_register_text = esc_attr($instance['mail_register_text']);
		$mail_register_type = esc_attr($instance['mail_register_type']);
		$mail_register_form = esc_attr($instance['mail_register_form']);
		$mail_register_button_text = esc_attr($instance['mail_register_button_text']); ?>
				<!-- タイトル -->

				<p><label for="<?php echo $this->get_field_id('mail_register_title'); ?>">
						<?php _e('タイトルの設定:<br/ ><small>タイトルを入力してください。</small>'); ?>
						<input class="mail-register-widget" id="<?php echo $this->get_field_id('mail_register_title'); ?>" name="<?php echo $this->get_field_name('mail_register_title'); ?>" type="text" value="<?php echo $mail_register_title; ?>" size="17" />
				</label></p>

				<p><label for="<?php echo $this->get_field_id('mail_register_text'); ?>">
						<?php _e('テキスト:<br/ ><small>フォーム上部に表示させるテキストを入力してください。<br /></small>'); ?>
						<textarea  class="widefat" id="<?php echo $this->get_field_id('mail_register_text'); ?>" name="<?php echo $this->get_field_name('mail_register_text'); ?>" value="<?php echo $mail_register_text; ?>" rows="10" cols="16"><?php echo $mail_register_text; ?></textarea>
				</label></p>
				<?php
		$options = array(
			'0' => __('楽メールPro', 'bb') ,
			'1' => __('ネット商人Pro', 'bb') ,
			'2' => __('メール商人', 'bb') ,
			'3' => __('Jcity', 'bb') ,
			'4' => __('アスメル', 'bb') ,
			'5' => __('オートビズ', 'bb') ,
			'6' => __('その他1（name属性がemail、nameになってるフォーム）', 'bb') ,
			'7' => __('その他1（name属性がemail、nameになってるフォーム）', 'bb') ,
			'8' => __('1ShoppingCart', 'bb') ,
			'9' => __('aWeber', 'bb') ,
			'10' => __('AutoResponsePlus', 'bb') ,
			'11' => __('Email Aces', 'bb') ,
			'12' => __('FreeAutobot', 'bb') ,
			'13' => __('GetResponse', 'bb') ,
			'14' => __('iContact', 'bb') ,
			'15' => __('InfusionSoft', 'bb') ,
			'23' => __('InfusionSoft(Old)', 'bb') ,
			'16' => __('ListPing', 'bb') ,
			'17' => __('MailChimp', 'bb') ,
			'18' => __('ParaBots', 'bb') ,
			'19' => __('ProSender', 'bb') ,
			'20' => __('QuickPayPro', 'bb') ,
			'21' => __('Turbo Autoresponders', 'bb') ,
			'22' => __('GVO', 'bb') ,
		);
?>
				<p><label for="<?php echo $this->get_field_id('mail_register_type'); ?>">
						<?php _e('フォームタイプ:<br/ ><small>メルマガのフォームタイプを選択してください。<br /></small>'); ?>
						<select name="<?php echo $this->get_field_name('mail_register_type'); ?>" id="mail_register_type">
							<?php foreach ($options as $key => $value): ?>
								<option value="<?php echo $key; ?>" <?php selected($mail_register_type, $key); ?> ><?php echo $value; ?></option>
							<?php
		endforeach; ?>
						</select>
				</label></p>
				<p><label for="<?php echo $this->get_field_id('mail_register_form'); ?>">
						<?php _e('HTMLフォームを入力:<br/ ><small>利用するメールサービスのフォームを入力してください。</small>'); ?>
						<textarea  class="widefat" id="<?php echo $this->get_field_id('mail_register_form'); ?>" name="<?php echo $this->get_field_name('mail_register_form'); ?>" value="<?php echo $mail_register_form; ?>" rows="10" cols="16"><?php echo $mail_register_form; ?></textarea>
				</label></p>

				<!-- ボタンのテキスト -->
				<p><label for="<?php echo $this->get_field_id('mail_register_button_text'); ?>">
						<?php _e('ボタンのテキスト:<br/ ><small>送信ボタンのテキストを入力してください。</small>'); ?>
						<input class="mail-register-widget" id="<?php echo $this->get_field_id('mail_register_button_text'); ?>" name="<?php echo $this->get_field_name('mail_register_button_text'); ?>" type="text" value="<?php echo $mail_register_button_text; ?>" size="17" />
				</label></p>
				<?php
	}
}

add_action('widgets_init', create_function('', 'return register_widget("widget_mail_register_contents");'));
