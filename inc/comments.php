<?php

/**
 * Extend Comments
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */

/**
 * Easy themes Comment form
 * @param  array  $args
 * @param  integer $post_id
 * @return string comment form html
 */

function easythemes_comment_form( $args = array(), $post_id = null ) {

	wp_parse_args(
		$args,
		array(
			'required' => '<span class="required">*</span>',
			'require_note' => '%1$sは必須項目です。',
		)
	);

	// Get current comment user info
	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );
	$required_notes = null;

	if ( $req ) {
		$required_notes = sprintf(
			$args['required_notes'], $args['required']
		);
	}

	foreach ( $args['fields'] as $field_name => $field ) {
			$args['fields'][$field_name] = sprintf(
					$field,
					( $req ? $args['required'] : '' ),
					esc_attr( $commenter['comment_author'. str_replace( 'author', '', $field_name )] ),
					$aria_req
			);
	}

	if ( $args['comment_notes_before'] ) {
		$args['comment_notes_before'] = sprintf(
			$args['comment_notes_before'], $required_notes
		);
	}

	comment_form( $args, $post_id );
}



/**
 * Easy theme comment
 * @param  array $comment
 * @param  array $args
 * @param  string $depth
 * @return string
 */
function easy_theme_comment( $comment, $args, $depth ) {

	$GLOBALS['comment'] = $comment; ?>
	<li id="comment-<?php comment_ID() ?>">
	<h4>
	<?php
	if ( ! ( get_comment_meta( $comment->comment_ID, 'comment-title', true ) ) ) {
		echo '<span>タイトルなし</span>';
	} else {
		echo get_comment_meta( $comment->comment_ID, 'comment-title', true );
	}
	?></h4>
		<div class="row-fluid coments-meta">
			<p class="pull-left"><?php comment_date(); ?>
		<?php comment_time(); ?></p>
		<p class="pull-right">コメント者名 : <?php comment_author_link(); ?></p>
	</div>

	<div class="compost">
	<?php comment_text(); ?>
	<br class="clear" />
	</div>
	<?php
}


/**
 * Add comment title field
 * @param  [type] $comment_id [description]
 * @return [type]             [description]
 */
function comment_field( $comment_id ) {

	if ( ! $comment = get_comment( $comment_id ) ){
		return false;
	}

	$custom_key_title  = 'comment-title';
	$get_comment_title = esc_attr( $_POST[$custom_key_title] );

	if ( '' == get_comment_meta( $comment_id, $custom_key_title ) ) {
		add_comment_meta( $comment_id, $custom_key_title, $get_comment_title, true );
	} else if ( $get_comment_title !== get_comment_meta( $comment_id, $custom_key_title ) ) {
		update_comment_meta( $comment_id, $custom_key_title, $get_comment_title );
	} else if ( '' == $get_comment_title ) {
		delete_comment_meta( $comment_id, $custom_key_title );
	}
	return false;
}

/**
 * Add comment title form
 *
 * @return void : echo comment title form.
 */

add_action( 'comment_post', 'comment_field' );
add_action( 'edit_comment', 'comment_field' );
add_action( 'add_meta_boxes_comment', 'comment_field_box' );

function comment_field_box() {
		global $comment;
		$comment_ID = $comment->comment_ID;
		$custom_key         = 'post_reviews_date' ;
		$custom_key_title   = 'comment-title' ;
		$noncename          = $custom_key . '_noncename' ;
		$get_comment_title  = esc_attr( get_comment_meta( $comment_ID, $custom_key_title, true ) );
		echo '<input type="hidden" name="' . $noncename . '" id="' . $noncename . '" value="' . wp_create_nonce( get_template() ) . '" />' . "\n";
		echo '<p><b><label for="comment-title">【コメントタイトル】</label></b></p><p><input id="' . $custom_key_title . '" name="' . $custom_key_title . '" type="text" value="' . $get_comment_title . '" size="56" maxlength="30"/></p>' . "\n";
}

/**
 * Manage Comment Column
 *
 * @param  string $columns
 * @return string : comment column title
 */

add_filter( 'manage_edit-comments_columns', 'manage_comment_columns' );

function manage_comment_columns( $columns ) {

	$columns['comment-title'] = "コメントタイトル";

	return $columns;

}


/**
 * Add Comment Column
 *
 * @param string $column_name
 * @param integer $comment_id
 */

add_action( 'manage_comments_custom_column', 'add_comment_columns', 10, 2 );

function add_comment_columns( $column_name, $comment_id ) {
	if ( $column_name == 'comment-title' ) {
		$comment_title = get_comment_meta( $comment_id, 'comment-title', true );
		echo attribute_escape( $comment_title );
	}
}
