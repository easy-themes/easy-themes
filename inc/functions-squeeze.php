<?php
/**
 * Squeeze Function
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */

add_action( 'save_post', 'squeeze_save_postdata' );
add_action( 'admin_print_scripts', 'squeeze_admin_scripts' );
add_action( 'admin_print_styles', 'squeeze_admin_styles' );

/**
 * Squeeze Add Custom Box
 * @return void
 */
function squeeze_add_custom_box(){
	add_meta_box( 'squeeze_sectionid', 'スクイーズページオプション', 'squeeze_inner_custom_box', 'page' );
	add_meta_box( 'headline-page', 'ページヘッドライン', 'headline_inner_custom_box', 'page', 'advanced', 'high' );
	add_meta_box( 'headline-post', '投稿ヘッドライン', 'headline_inner_custom_box', 'post', 'advanced', 'high' );
}

/**
 * Heading Inner Custom Box
 * @return [type] [description]
 */
function headline_inner_custom_box(){
		global $post;
		$pid = $post->ID;
		$headline = get_post_meta( $pid, 'headline', true );
		?>
<div class="squeeze-set squeeze-set-default">
		<div class="field">
				<label for="headline">ヘッドライン:</label>
				<input style="width: 350px;" class="text" type="text" name="headline" id="headline"
							 value="<?php echo $headline; ?>">

				<div class="clear"></div>
		</div>
</div>
<?php

}

function squeeze_admin_scripts() {
	wp_enqueue_script( 'media-upload' );
	wp_enqueue_script( 'thickbox' );
}

function squeeze_admin_styles() {
	wp_enqueue_style( 'thickbox' );
}

function squeeze_inner_custom_box()
{

		wp_nonce_field(plugin_basename(__FILE__), 'squeeze_noncename');

		global $post;
		$pid = $post->ID;

		$squeeze_type	= get_post_meta( $pid, 'squeeze_type', true );

		$optin_cta		= get_post_meta( $pid, 'squeeze_optin_cta', true );
		$optin_type		= get_post_meta( $pid, 'squeeze_optin_type', true );
		$optin_button	= get_post_meta( $pid, 'squeeze_optin_button', true );

		$footer_menu	= get_post_meta( $pid, 'squeeze_footer_menu', true );

		?>
<style type="text/css">
		.squeeze-thumb {
				background: url(<?php bloginfo( 'stylesheet_directory' ); ?>/squeezes/<?php echo $squeeze_type; ?>/screenshot.jpg) left top no-repeat;
				width: 400px;
				height: 300px;
				margin: 10px auto;
				border: 1px solid #000;
		}

		.squeeze-set {
				display: none;
		}

		.squeeze-set-default {
				display: block;
		}

		.center {
				text-align: center !important;
		}

		.bold {
				font-weight: bold;
		}

		hr {
				background: #ddd;
				color: #ddd;
				border: none;
				height: 1px;
		}

		.field {
				padding: 5px 0;
		}

		.field label {
				float: left;
				line-height: 23px;
				margin-right: 10px;
				display: block;
				width: 150px;
		}

		.field input.text,
		.field textarea,
		.field select {
				float: left;
				width:;
		}

		.field input.text {
				width: 450px;
		}

		.field textarea {
				width: 450px;
				min-height: 100px;
		}

		.field-squeeze-type label,
		.field-squeeze-type select {
				display: inline;
				float: none;
		}

		.add-list-item {
				margin-left: 160px;
		}

		.item .button {
				margin-left: 5px;
		}

		.list-items .item {
				padding: 0 0 10px 160px;
		}

		.list-items .item input {
				width: 350px;
		}

		.list-items label {
				line-height: 25px;
		}

</style>
<script type="text/javascript">

		var currentUploadTextfield = null;
		function showMediaLibraryBox() {
				currentUploadTextfield = jQuery(this).attr('href');
				tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true&amp;post_id=0');
				return false;
		}

		jQuery(document).ready(function () {

				jQuery('.upload-trigger').click(showMediaLibraryBox);

				window.original_send_to_editor = window.send_to_editor;
				window.send_to_editor = function(html) {
						alert(currentUploadTextfield );
						if (currentUploadTextfield != null) {
								imgurl = jQuery('img', html).attr('src');
								jQuery(currentUploadTextfield).val(imgurl);
								currentUploadTextfield = null;
								tb_remove();
						} else {
								window.original_send_to_editor(html);
						}
				};

				jQuery('#squeeze_type').change(function () {
						var type = jQuery(this).val();
						jQuery('.squeeze-thumb').css('background-image', 'url(<?php bloginfo('stylesheet_directory'); ?>/squeezes/' + type + '/screenshot.jpg)');
						updateFieldSets(type);
				});

				// Set select box values
				jQuery('#squeeze_type').val('<?php echo $squeeze_type; ?>');
				jQuery('#squeeze_optin_cta').val('<?php echo $optin_cta; ?>');
				jQuery('#squeeze_optin_type').val('<?php echo $optin_type; ?>');
				jQuery('#squeeze_optin_button').val('<?php echo $optin_button; ?>');
				jQuery('#squeeze_footer_menu').val('<?php echo $footer_menu; ?>');

				jQuery('.remove-button').live('click', function () {
						jQuery(this).parent().remove();
						return false;
				});
				jQuery('.add-list-item').click(function () {
						jQuery('<div class="item"><input type="text" class="text" name="squeeze-list-items[]"><a class="button remove-button" href="#">削除</a></div>')
										.insertBefore(jQuery(this).parent());
						return false;
				});

				updateFieldSets('<?php echo $squeeze_type; ?>');

		});

		function updateFieldSets(type) {
				switch (type) {
						case 'text-squeeze-01':
								jQuery('.video-fields').hide();
								jQuery('.testimonial-fields').hide();
								jQuery('.subheader-field').show();
								jQuery('.image-field').show();
								jQuery('.yellow-text-field').show();
								jQuery('.list-fields').show();
								jQuery('.cta-field').show();
								break;
						case 'text-squeeze-02':
								jQuery('.video-fields').hide();
								jQuery('.testimonial-fields').hide();
								jQuery('.subheader-field').show();
								jQuery('.image-field').show();
								jQuery('.yellow-text-field').show();
								jQuery('.list-fields').show();
								jQuery('.cta-field').hide();
								break;
						case 'text-squeeze-03':
								jQuery('.video-fields').hide();
								jQuery('.testimonial-fields').hide();
								jQuery('.subheader-field').show();
								jQuery('.image-field').show();
								jQuery('.yellow-text-field').show();
								jQuery('.list-fields').show();
								jQuery('.cta-field').hide();
								break;
						case 'text-squeeze-04':
								jQuery('.video-fields').hide();
								jQuery('.testimonial-fields').hide();
								jQuery('.subheader-field').show();
								jQuery('.image-field').show();
								jQuery('.yellow-text-field').show();
								jQuery('.list-fields').show();
								jQuery('.cta-field').hide();
								break;
						case 'text-squeeze-05':
								jQuery('.video-fields').hide();
								jQuery('.testimonial-fields').hide();
								jQuery('.subheader-field').show();
								jQuery('.image-field').show();
								jQuery('.yellow-text-field').show();
								jQuery('.list-fields').show();
								jQuery('.cta-field').hide();
								break;
						case 'text-squeeze-06':
								jQuery('.video-fields').hide();
								jQuery('.testimonial-fields').hide();
								jQuery('.subheader-field').show();
								jQuery('.image-field').show();
								jQuery('.yellow-text-field').show();
								jQuery('.list-fields').show();
								jQuery('.cta-field').show();
								break;

						case 'video-squeeze-01':
								jQuery('.video-fields').show();
								jQuery('.testimonial-fields').hide();
								jQuery('.subheader-field').show();
								jQuery('.image-field').hide();
								jQuery('.yellow-text-field').hide();
								jQuery('.list-fields').hide();
								jQuery('.cta-field').hide();
								break;
						case 'video-squeeze-02':
								jQuery('.video-fields').show();
								jQuery('.testimonial-fields').hide();
								jQuery('.subheader-field').show();
								jQuery('.image-field').hide();
								jQuery('.yellow-text-field').hide();
								jQuery('.list-fields').hide();
								jQuery('.cta-field').hide();
								break;
						case 'video-squeeze-03':
								jQuery('.video-fields').show();
								jQuery('.testimonial-fields').show();
								jQuery('.subheader-field').show();
								jQuery('.image-field').hide();
								jQuery('.yellow-text-field').hide();
								jQuery('.list-fields').hide();
								jQuery('.cta-field').hide();
								break;
						case 'video-squeeze-04':
								jQuery('.video-fields').show();
								jQuery('.testimonial-fields').show();
								jQuery('.subheader-field').hide();
								jQuery('.image-field').hide();
								jQuery('.yellow-text-field').hide();
								jQuery('.list-fields').hide();
								jQuery('.cta-field').hide();
								break;
						case 'video-squeeze-05':
								jQuery('.video-fields').show();
								jQuery('.testimonial-fields').hide();
								jQuery('.subheader-field').show();
								jQuery('.image-field').hide();
								jQuery('.yellow-text-field').hide();
								jQuery('.list-fields').hide();
								jQuery('.cta-field').hide();
								break;
				}
		}

</script>
<?php

		$warning_text = get_post_meta($pid, 'squeeze_warning_text', true);
		$top_header = get_post_meta($pid, 'squeeze_top_header', true);
		$sub_header = get_post_meta($pid, 'squeeze_sub_header', true);
		$image = get_post_meta($pid, 'squeeze_image', true);
		$yellow_bg_text = get_post_meta($pid, 'squeeze_yellow_bg_text', true);
		$list_title = get_post_meta($pid, 'squeeze_list_title', true);
		$list_items = get_post_meta($pid, 'squeeze_list_items', true);

		$optin_top_text = get_post_meta($pid, 'squeeze_optin_top_text', true);
		$optin_code = get_post_meta($pid, 'squeeze_optin_code', true);
		$optin_privacy_text = get_post_meta($pid, 'squeeze_optin_privacy_text', true);

		$video_thumb = get_post_meta($pid, 'squeeze_video_thumbnail', true);
		$video_url = get_post_meta($pid, 'squeeze_video_url', true);
		$video_autostart = get_post_meta($pid, 'squeeze_video_autostart', true);
		$video_width = get_post_meta($pid, 'squeeze_video_width', true);
		$video_height = get_post_meta($pid, 'squeeze_video_height', true);

		$testimonial_1_text = get_post_meta($pid, 'squeeze_testimonial_1_text', true);
		$testimonial_1_name = get_post_meta($pid, 'squeeze_testimonial_1_name', true);
		$testimonial_1_url = get_post_meta($pid, 'squeeze_testimonial_1_url', true);

		$testimonial_2_text = get_post_meta($pid, 'squeeze_testimonial_2_text', true);
		$testimonial_2_name = get_post_meta($pid, 'squeeze_testimonial_2_name', true);
		$testimonial_2_url = get_post_meta($pid, 'squeeze_testimonial_2_url', true);

		?>
<div class="wrap">
<hr/>
<div class="field field_squeeze_type center">
		<label for="squeeze_type" class="bold" id="squeeze_type_label">ページタイプ:</label>
		<select name="squeeze_type" id="squeeze_type" style="width: 300px;">
				<option value="">__ 下から選択して下さい __</option>
				<option id="text_squeeze_01" value="text_squeeze_01">テキストスクイーズ01</option>
				<option id="text_squeeze_02" value="text_squeeze_02">テキストスクイーズ02</option>
				<option id="text_squeeze_03" value="text_squeeze_03">テキストスクイーズ03</option>
				<option id="text_squeeze_04" value="text_squeeze_04">テキストスクイーズ04</option>
				<option id="text_squeeze_05" value="text_squeeze_05">テキストスクイーズ05</option>
				<option id="text_squeeze_06" value="text_squeeze_06">テキストスクイーズ06</option>
				<option id="video_squeeze_01" value="video_squeeze_01">ビデオスクイーズ01</option>
				<option id="video_squeeze_02" value="video_squeeze_02">ビデオスクイーズ02</option>
				<option id="video_squeeze_03" value="video_squeeze_03">ビデオスクイーズ03</option>
				<option id="video_squeeze_04" value="video_squeeze_04">ビデオスクイーズ04</option>
				<option id="video_squeeze_05" value="video_squeeze_05">ビデオスクイーズ05</option>
		</select>

		<div class="clear"></div>
</div>
<hr/>
<div class="squeeze-thumb"></div>
<hr/>
<div id="squeeze-settings-sets">
<div class="squeeze-set squeeze-set-default">
<h2>基本設定</h2>

<div class="field">
		<label for="squeeze-warning-text">最上部テキスト:</label>
		<input class="text" type="text" name="squeeze-warning-text" id="squeeze-warning-text"
					 value="<?php echo $warning_text; ?>">

		<div class="clear"></div>
</div>
<div class="field">
		<label for="squeeze-top-header">トップヘッダー:</label>
		<textarea style="height:3.5em" name="squeeze-top-header" id="squeeze-top-header" cols="30"
							rows="10"><?php echo $top_header; ?></textarea>

		<div class="clear"></div>
</div>
<div class="field subheader-field">
		<label for="squeeze-sub-header">サブヘッダー:</label>
		<textarea style="height:3.5em" name="squeeze-sub-header" id="squeeze-sub-header" cols="30"
							rows="10"><?php echo $sub_header; ?></textarea>

		<div class="clear"></div>
</div>

<div class="field image-field">
		<label for="squeeze-image">イメージ画像:</label>
		<input type="text" style="width: 300px;" id="squeeze-image" class="text"
					 value="<?php echo $image; ?>"
					 name="squeeze-image">&nbsp;<a href="#squeeze-image" class="upload-trigger"
																				 id="squeeze-image-upload">
		<img alt="Add a Image" src="<?php bloginfo('url');?>/wp-admin/images/media-button-image.gif"></a>

		<div class="clear"></div>
		<div style="margin: 10px 0 10px 160px;">
<?php
								if (!empty($image)) {
		echo '<img style="max-width: 300px;max-height: 200px;height:auto;width:auto;" src="' . $image . '">';
} else {
		echo 'まだ画像はありません。';
}
		?>
		</div>
</div>

<div class="field yellow-text-field">
		<label for="squeeze-sub-header">黄色背景用テキスト:</label>
		<textarea style="height:3.5em" name="squeeze-yellow-bg-text" id="squeeze-yellow-bg-text" cols="30"
							rows="10"><?php echo $yellow_bg_text; ?></textarea>

		<div class="clear"></div>
</div>

<div class="list-fields">
		<h2>リスト書き設定</h2>

		<div class="field">
				<label for="squeeze-list-title">箇条書きのタイトル:</label>
				<input class="text" type="text" name="squeeze-list-title" id="squeeze-list-title"
							 value="<?php echo $list_title; ?>">

				<div class="clear"></div>
		</div>

		<div class="list-items">
				<label style="float:left;">箇条書きコンテンツ:</label>
				<?php if (!is_array($list_items) || count($list_items) <= 0) { ?>
				<div class="item"><input type="text" class="text" name="squeeze-list-items[]"><a
								class="button remove-button"
								href="#">削除</a></div>
				<?php

		} else {

				foreach ($list_items as $item) {
						echo '<div class="item"><input type="text" class="text" name="squeeze-list-items[]" value="' . $item . '"><a class="button remove-button" href="#">削除</a></div>';
				}

		}?>
				<div>
						<a class="button add-list-item" href="#">新規リスト追加</a>
				</div>
		</div>
</div>

<h2>オプトイン設定</h2>

<div class="field cta-field">
		<label for="squeeze-optin-cta">呼び込み画像:</label>
		<select name="squeeze-optin-cta" id="squeeze-optin-cta">
				<option value="download-free-now">今すぐ無料ダウンロード!</option>
				<option value="get-free-access">今すぐ無料アクセス!</option>
				<option value="get-instant-access">今すぐ簡単アクセス!</option>
				<option value="sign-up-for-free">登録無料!</option>
				<option value="download-free-report">無料レポートをダウンロード!</option>
		</select>

		<div class="clear"></div>
</div>
<div class="field">
		<label for="squeeze-optin-top-text">トップヘッダー:</label>
		<textarea style="height:3.5em" name="squeeze-optin-top-text" id="squeeze-optin-top-text" cols="30"
							rows="10"><?php echo $optin_top_text; ?></textarea>

		<div class="clear"></div>
</div>
<div class="field">
		<label for="squeeze-optin-type">フォームタイプ:</label>
		<select name="squeeze-optin-type" id="squeeze-optin-type">
				<option value="0">楽メールPro</option>
				<option value="1">ネット商人Pro</option>
				<option value="2">メール商人</option>
				<option value="3">Jcity</option>
				<option value="4">アスメル</option>
				<option value="5">オートビズ</option>
				<option value="6">その他1（name属性がemail、nameになってるフォーム）</option>
				<option value="7">その他2（name属性がaddress、nameになってるフォーム）</option>
				<option value="8">1ShoppingCart</option>
				<option value="9">aWeber</option>
				<option value="10">AutoResponsePlus</option>
				<option value="11">Email Aces</option>
				<option value="12">FreeAutobot</option>
				<option value="13">GetResponse</option>
				<option value="14">iContact</option>
				<option value="15">InfusionSoft</option>
				<option value="23">InfusionSoft (Old)</option>
				<option value="16">ListPing</option>
				<option value="17">MailChimp</option>
				<option value="18">ParaBots</option>
				<option value="19">ProSender</option>
				<option value="20">QuickPayPro</option>
				<option value="21">Turbo Autoresponders</option>
				<option value="22">GVO</option>
		</select>

		<div class="clear"></div>
</div>
<div class="field">
		<label for="squeeze-optin-code">HTMLフォーム:</label>
		<textarea name="squeeze-optin-code" id="squeeze-optin-code" cols="30"
							rows="10"><?php echo $optin_code; ?></textarea>

		<div class="clear"></div>
</div>
<div class="field">
		<label for="squeeze-optin-button">ボタン画像:</label>
		<select name="squeeze-optin-button" id="squeeze-optin-button">
				<option value="download-now">今すぐダウンロード</option>
				<option value="free-access">無料アクセス</option>
				<option value="instant-access">簡単アクセス</option>
				<option value="sign-up-now">今すぐ登録</option>
				<option value="get-free-report">無料レポートを入手</option>
				<option value="watch-video">ビデオを見る</option>
		</select>

		<div class="clear"></div>
</div>
<div class="field">
		<label for="squeeze-optin-privacy-text">プライバシー方針（短文）:</label>
		<textarea style="height:3.5em" name="squeeze-optin-privacy-text" id="squeeze-optin-privacy-text"
							cols="30"
							rows="10"><?php echo $optin_privacy_text; ?></textarea>

		<div class="clear"></div>
</div>

<h2>フッターメニュー</h2>

<div class="field">
		<label for="squeeze-footer-menu">フッターメニュー:</label>
		<?php $menus = wp_get_nav_menus(); ?>
		<select name="squeeze-footer-menu" id="squeeze-footer-menu">
				<option value="0">-- 下から選択して下さい --</option>
				<?php foreach ($menus as $menu) : ?>
				<option value="<?php echo $menu->term_id; ?>"><?php $truncated_name = wp_html_excerpt($menu->name, 40);
						echo $truncated_name == $menu->name ? $menu->name : trim($truncated_name) . '&hellip;';
						?></option>
				<?php endforeach; ?>
		</select>

		<div class="clear"></div>
		<div>
				<small><a href="<?php bloginfo('url'); ?>/wp-admin/nav-menus.php">こちらをクリック</a>してメニューを設定</small>
		</div>
</div>

<div class="video-fields">
		<h2>ビデオ</h2>

		<div class="field field-checkbox">
				<label for="squeeze-video-autostart">ビデオの自動再生:</label>
				<input type="checkbox" name="squeeze-video-autostart" value="autostart"
							 id="squeeze-video-autostart" <?php echo empty($video_autostart) ? '' : 'checked="checked"'; ?>">
				<div class="clear"></div>
		</div>

		<div class="field">
				<label for="squeeze-video-url">ビデオURL:</label>
				<input class="text" type="text" name="squeeze-video-url" id="squeeze-video-url"
							 value="<?php echo $video_url; ?>">

				<div class="clear"></div>
		</div>
		<div class="field">
				<label for="squeeze-video-thumbnail">ビデオサムネイル:</label>
				<input type="text" style="width: 300px;" id="squeeze-video-thumbnail" class="text"
							 value="<?php echo $video_thumb; ?>"
							 name="squeeze-video-thumbnail">&nbsp;<a href="#squeeze-video-thumbnail" class="upload-trigger"
																											 id="squeeze-video-thumbnail-upload">
				<img alt="Add a Thumbnail" src="<?php bloginfo('url');?>/wp-admin/images/media-button-image.gif"></a>

				<div class="clear"></div>
				<div style="margin: 10px 0 10px 160px;">
<?php
								if (!empty($video_thumb)) {
		echo '<img style="max-width: 300px;max-height: 200px;height:auto;width:auto;" src="' . $video_thumb . '">';
} else {
		echo 'まだ画像は登録されていません';
}
		?>
				</div>
		</div>
</div>

<div class="testimonial-fields">

		<h2>お客様の声#1</h2>

		<div class="field">
				<label for="testimonial-text-1">テキスト:</label>
				<textarea name="testimonial-text-1" id="testimonial-text-1" cols="30"
									rows="10"><?php echo $testimonial_1_text; ?></textarea>

				<div class="clear"></div>
		</div>
		<div class="field">
				<label for="testimonial-name-1">名前/会社名:</label>
				<input type="text" class="text" name="testimonial-name-1" id="testimonial-name-1"
							 value="<?php echo $testimonial_1_name; ?>">

				<div class="clear"></div>
		</div>
		<div class="field">
				<label for="testimonial-url-1">URL:</label>
				<input type="text" class="text" name="testimonial-url-1" id="testimonial-url-1"
							 value="<?php echo $testimonial_1_url; ?>">

				<div class="clear"></div>
		</div>

		<h2>お客様の声#2</h2>

		<div class="field">
				<label for="testimonial-text-2">テキスト:</label>
				<textarea name="testimonial-text-2" id="testimonial-text-2" cols="30"
									rows="10"><?php echo $testimonial_2_text; ?></textarea>

				<div class="clear"></div>
		</div>
		<div class="field">
				<label for="testimonial-name-2">名前/会社名:</label>
				<input type="text" class="text" name="testimonial-name-2" id="testimonial-name-2"
							 value="<?php echo $testimonial_2_name; ?>">

				<div class="clear"></div>
		</div>
		<div class="field">
				<label for="testimonial-url-2">URL:</label>
				<input type="text" class="text" name="testimonial-url-2" id="testimonial-url-2"
							 value="<?php echo $testimonial_2_url; ?>">

				<div class="clear"></div>
		</div>
</div>


</div>
<div class="squeeze-set squeeze-set-1" id="text-squeeze-01-set">1</div>
<div class="squeeze-set squeeze-set-2" id="text-squeeze-02-set">2</div>
<div class="squeeze-set squeeze-set-3" id="text-squeeze-03-set">3</div>
<div class="squeeze-set squeeze-set-4" id="text-squeeze-04-set">4</div>
<div class="squeeze-set squeeze-set-5" id="text-squeeze-05-set">5</div>
<div class="squeeze-set squeeze-set-6" id="text-squeeze-06-set">6</div>
<div class="squeeze-set squeeze-set-7" id="video-squeeze-01-set">7</div>
<div class="squeeze-set squeeze-set-8" id="video-squeeze-02-set">8</div>
<div class="squeeze-set squeeze-set-9" id="video-squeeze-03-set">9</div>
<div class="squeeze-set squeeze-set-10" id="video-squeeze-04-set">10</div>
<div class="squeeze-set squeeze-set-11" id="video-squeeze-05-set">11</div>
</div>
</div>
<?php

}

function squeeze_save_postdata($post_id)
{

		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
				return;
		$headline = isset( $_POST['headline'] ) ? $_POST['headline'] : '';
		update_post_meta($post_id, 'headline', esc_attr( $headline ));


		$squeeze_noncename = isset( $_POST['squeeze_noncename'] ) ? $_POST['squeeze_noncename'] : '';

		if ( ! wp_verify_nonce( $squeeze_noncename, plugin_basename( __FILE__ ) ) ){
			return;
		}

		if ( 'page' == $_POST['post_type'] ) {
			if ( ! current_user_can( 'edit_page', $post_id ) ){
				return;
			}
		} else {
			if ( ! current_user_can( 'edit_post', $post_id ) ){
				return;
			}
		}


	update_post_meta( $post_id, 'squeeze-type', esc_attr( $_POST['squeeze-type'] ) );
	update_post_meta( $post_id, 'squeeze-warning-text', esc_attr( $_POST['squeeze-warning-text'] ) );
	update_post_meta( $post_id, 'squeeze-top-header', esc_attr( $_POST['squeeze-top-header'] ) );
	update_post_meta( $post_id, 'squeeze-sub-header', esc_attr( $_POST['squeeze-sub-header'] ) );
	update_post_meta( $post_id, 'squeeze-image', esc_attr( $_POST['squeeze-image'] ) );
	update_post_meta( $post_id, 'squeeze-yellow-bg-text', esc_attr( $_POST['squeeze-yellow-bg-text'] ) );
	update_post_meta( $post_id, 'squeeze-list-title', esc_attr( $_POST['squeeze-list-title'] ) );
	update_post_meta( $post_id, 'squeeze-list-items', $_POST['squeeze-list-items' ]   );
	update_post_meta( $post_id, 'squeeze-optin-cta', esc_attr( $_POST['squeeze-optin-cta'] ) );
	update_post_meta( $post_id, 'squeeze-optin-top-text', esc_attr( $_POST['squeeze-optin-top-text'] ) );
	update_post_meta( $post_id, 'squeeze-optin-type', esc_attr( $_POST['squeeze-optin-type'] ) );
	update_post_meta( $post_id, 'squeeze-optin-code', esc_attr( $_POST['squeeze-optin-code'] ) );
	update_post_meta( $post_id, 'squeeze-optin-button', esc_attr( $_POST['squeeze-optin-button'] ) );
	update_post_meta( $post_id, 'squeeze-optin-privacy-text', esc_attr( $_POST['squeeze-optin-privacy-text'] ) );
	update_post_meta( $post_id, 'squeeze-video-thumbnail', esc_attr( $_POST['squeeze-video-thumbnail'] ) );
	update_post_meta( $post_id, 'squeeze-video-url', esc_attr( $_POST['squeeze-video-url'] ) );
	update_post_meta( $post_id, 'squeeze-video-autostart', esc_attr( $_POST['squeeze-video-autostart'] ) );
	update_post_meta( $post_id, 'squeeze-video-width', esc_attr( $_POST['squeeze-video-width'] ) );
	update_post_meta( $post_id, 'squeeze-video-height', esc_attr( $_POST['squeeze-video-height'] ) );
	update_post_meta( $post_id, 'squeeze-footer-menu', esc_attr( $_POST['squeeze-footer-menu'] ) );
	update_post_meta( $post_id, 'squeeze-testimonial-1-text', esc_attr( $_POST['testimonial-text-1'] ) );
	update_post_meta( $post_id, 'squeeze-testimonial-1-name', esc_attr( $_POST['testimonial-name-1'] ) );
	update_post_meta( $post_id, 'squeeze-testimonial-1-url', esc_attr( $_POST['testimonial-url-1'] ) );
	update_post_meta( $post_id, 'squeeze-testimonial-2-text', esc_attr( $_POST['testimonial-text-2'] ) );
	update_post_meta( $post_id, 'squeeze-testimonial-2-name', esc_attr( $_POST['testimonial-name-2'] ) );
	update_post_meta( $post_id, 'squeeze-testimonial-2-url', esc_attr( $_POST['testimonial-url-2'] ) );

	return;
}

/**
 * Squeeze Parse Option Form
 * @param  string $optin_form_code
 * @return
 */

function squeeze_parse_optin_form( $optin_form_code ){

	global $plc;
	$pattern = '/<form\s[^>]*action[\s]*=[\s]*[\'|"](.*?)[\'|"][^>]*>/i';

	preg_match( $pattern, $optin_form_code, $form_tag_matches );
	$form_action	= $form_tag_matches[1];
	$all_inputs		= return_between_multiple( $optin_form_code, '<input', '>', true );

	foreach ( $all_inputs as $input ) {
		if ( strpos( $input, 'hidden' ) !== false ) {
			$input = $input . '>';
			$name = return_between( $input, 'name', ' ', false );
			if ( $name == 'n' ) {
				$name = return_between( $input, 'name', '>', false );
			}
			$name = trim( $name );
			$name = explode( '=', $name, 2 );
			$name = str_replace( '\'', '', $name[1] );
			$name = str_replace( '"', '', $name );
			$name = str_replace( '>', '', $name );
			$val  = return_between( $input, 'value', ' ', false );

			if ( $val == 'v' ) {
				$val = return_between( $input, 'value', '>', false );
			}

			$val = trim( $val );
			$val = explode( '=', $val, 2 );
			$val = str_replace( '\'', '', $val[1] );
			$val = str_replace( '"', '', $val );
			$val = str_replace( '>', '', $val );
			$arr_name[] = $name;
			$arr_value[] = $val;
		}
	}

	if ( is_array( $arr_name )
			 && is_array( $arr_value ) ) {

		$post_params = array_combine( $arr_name, $arr_value );

	}

	$post_params['action'] = $form_action;

	return $post_params;

}

/**
 * Squeeze Name Email Fields
 * @param  [type] $optin_list_provider [description]
 * @return [type]                      [description]
 */
function squeeze_name_email_fields( $optin_list_provider ) {
	$providerNameFields  = array( 'name', 'c_name', 'rdlastname', 'sName', 'touroku_name', 'name1', 'name', 'name', 'Name', 'name', 'full_name', 'web_name', 'Name', 'category2', 'fields_fname', 'inf_field_FirstName', 'name', 'NAME', 'fname', 'name', 's_first_name', 'name', 'FullName', 'Contact0FirstName', 'FirstName');
	$providerEmailFields = array( 'mail', 'c_mailaddress', 'rdemail', 'sEmail', 'touroku_mail', 'email', 'email', 'address', 'Email1', 'email', 'email', 'web_email', 'Email', 'category3', 'fields_email', 'inf_field_Email', 'from', 'EMAIL', 'email', 'from', 's_email', 'from', 'Email', 'Contact0Email');
	$fields['name']      = $providerNameFields[$optin_list_provider];
	$fields['email']     = $providerEmailFields[$optin_list_provider];

	return $fields;

}

/**
 * Between
 *
 * @param  string $string
 * @param  string $start
 * @param  string $stop
 * @param  string $type
 * @return string
 */
function return_between( $string, $start, $stop, $type ){

	if ( strpos( $string, $stop ) === false ) {

		$stop_err = str_replace( '<', '&lt;', $stop );
		$stop_err = str_replace( '>', '&gt;', $stop_err );

		if ( is_admin() ) {
			die( '<span style="color:red;">エラー！divで問題が発生しました！プロセスを停止します"' . $stop_err . '" 投稿内容が見つかりません。</span>' );
		} else {
			die( '投稿プロセスエラー' );
		}

	}

	$temp = split_string( $string, $start, 'AFTER', $type );

	if ( empty( $temp ) ) {
		return $temp;
	} else {
		return split_string( $temp, $stop, 'BEFORE', $type );
	}

}

/**
 * BetWeen Multiple
 *
 * @param  string $string
 * @param  string $start
 * @param  string $stop
 * @param  string $type
 * @return string
 */
function return_between_multiple( $string, $start, $stop, $type ){
	$return = array();
	while ( stristr( $string, $start ) ) {
		$temp = split_string( $string, $start, 'AFTER', $type );
		$return[] = split_string( $temp, $stop, 'BEFORE', $type );
		$string = $temp;
	}
	return $return;
}

/**
 * Split string
 * @param  [type] $string     [description]
 * @param  [type] $delineator [description]
 * @param  [type] $desired    [description]
 * @param  [type] $type       [description]
 * @return [type]             [description]
 */
function split_string( $string, $delineator, $desired, $type ) {
	# Case insensitive parse, convert string and delineator to lower case
	$lc_str = strtolower( $string );
	$marker = strtolower( $delineator );

	# Return text BEFORE the delineator
	if ( $desired == 'BEFORE' ) {
		// Return text ESCL of the delineator
		if ( $type == 'EXCL' ) {
			$split_here = strpos( $lc_str, $marker );
		// Return text INCL of the delineator
		} else {
			$split_here = strpos( $lc_str, $marker ) + strlen( $marker );
		}
		$parsed_string = substr( $string, 0, $split_here );
	// Return text AFTER the delineator
	} else {
		if ( $type == 'EXCL' ) {
			// Return text ESCL of the delineator
			$split_here = strpos( $lc_str, $marker ) + strlen( $marker );
		} else {
			// Return text INCL of the delineator
			$split_here = strpos( $lc_str, $marker );
		}
		$parsed_string = substr( $string, $split_here, strlen( $string ) );
	}

	return $parsed_string;

}
