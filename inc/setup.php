<?php

/**
 * Theme setup script
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */

/**
 * Theme setup
 * @return void
 */
add_action( 'init', 'easy_themes_setup', 10 );

function easy_themes_setup(){

	add_editor_style( 'css/editor.css' );
	register_nav_menu( 'navi_menu', 'グローバルメニュー' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'jquery-cdn' );
	set_post_thumbnail_size( 360, 360, true );
	add_image_size( 'new',420,230,true );
	add_image_size( 'pic',135,120,true );
	add_image_size( 'mainposts',120,100,true );
	add_image_size( 'entryicon',78,68,true );

}

add_action( 'init', 'easythemes_head_cleanup', 10 );

function easythemes_head_cleanup(){
	remove_action( 'wp_head', 'feed_links', 2 );
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );

	global $wp_widget_factory;

	remove_action( 'wp_head',
		array(
			$wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
			'recent_comments_style',
		)
	);

}

/**
 * Remove more link
 *
 * @param  string $link
 * @return string $link
 */

add_filter( 'the_content_more_link', 'remove_more_jump_link' );

function remove_more_jump_link( $link ) {
	$offset = strpos( $link, '#more-' );

	if ( $offset ) {
		$end = strpos( $link, '"', $offset );
	}

	if ( $end ) {
		$link = '<p class="mt20 align2">' . substr_replace( $link, '', $offset, $end - $offset ) . '</p>';
	}
	return $link;
}


/**
 * Page Navigation
 *
 * @return void : print page navigation.
 */
function page_navigation() {

	global $wp_rewrite, $wp_query, $paged;

	$paginate_base = get_pagenum_link( 1 );
	if ( ( $wp_query->max_num_pages ) > 1 ) {

		if ( strpos( $paginate_base, '?' )
				 || ! $wp_rewrite->using_permalinks() ) {

			$paginate_format = '';
			$paginate_base = add_query_arg( 'paged', '%#%' );

		} else {

			$paginate_format = ( substr( $paginate_base, -1 ,1 ) == '/' ? '' : '/' ) . user_trailingslashit( 'page/%#%/', 'paged' );
			$paginate_base .= '%_%';

		}

		$result = paginate_links( array(
			'base' => $paginate_base,
			'format' => $paginate_format,
			'total' => $wp_query->max_num_pages,
			'mid_size' => 3,
			'current' => ( $paged ? $paged : 1),
			)
		);
		echo '<p class="local-navigation">'."\n".$result."</p>\n";

	}

}

function no_thumbnail( $img_size = null, $stat = null ) {
	$crownTheme = '/images/thumbnail-icon.jpg';
	if ( $img_size == null ) {
		echo '<img src="' . get_stylesheet_directory_uri() . $crownTheme . '" '. $stat . ' />';
	} else {
		echo '<img src="' . get_stylesheet_directory_uri() . $crownTheme . '" width="'.$img_size.'" height="'. $img_size .'" '. $stat . ' />';
	}
}



// 管理画面でカテゴリID表示
add_action( 'admin_head', 'always_showid' );
function always_showid() {
?>
<style type="text/css">div.row-actions{visibility:visible !important;}</style>
<?php
}

function catid_show($actions,$category) {
	if ( current_user_can('manage_categories') ) {
		$actions['edit'] = "ID:" . $category->term_id. " | " . $actions['edit'] ;
	}
	return $actions;
}
add_filter('cat_row_actions', 'catid_show', '10', '2');

function tagid_show($actions,$tag) {
	if ( current_user_can('edit_posts') ) {
		$actions['edit'] = "ID:" . $tag->term_id. " | " . $actions['edit'] ;
	}
	return $actions;
}
add_filter('tag_row_actions', 'tagid_show', '10', '2');

add_action( 'after_switch_theme', 'myactivationfunction', 10 , 2 );
function myactivationfunction() {
	retrieve_widgets( $theme_changed = true );
}



/**
 * オプション変換
 *
 **/

function convert_option( $settingName , $property = NULL){
	$data = '';
	$data = get_option( $settingName );
	if ( !$property == '' && isset( $data ) ) {
		$dataProperty = $data[$property];
		return $dataProperty;
	} elseif ( $property == NULL  && isset( $data ) ) {
		return $data;
	} else {
		$data = '';
		return $data;
	}
}

/**
 * 画像のアップロード
 */
function media_upload_theme_switch () {
	// media upload dir
	$wp_upload_dir  = wp_upload_dir();

	$root_template = get_template_directory() . '/assets/images/pattern/';

	// copy template dir to upload dir
	$filenames = easythemes_activate_list_files( $root_template );
	foreach ( $filenames as $key => $filename ) {
		if ( ! file_exists( $filename )  ){
			continue;
		}
		$copy_flg = copy( $filename, $wp_upload_dir['path'] . '/' . basename( $filename ) );
	}

	$root_upload_dir = $wp_upload_dir['path'] . '/';
	$upload_filenames = easythemes_activate_list_files( $root_upload_dir );

	foreach ( $upload_filenames as $key => $filename ) {
		$parent_post_id = 0;
		$filetype       = wp_check_filetype( basename( $filename ) , null );
		$attachment     = array(
			'guid'						=> $wp_upload_dir['url'] . '/' . basename( $filename ),
			'post_mime_type'	=> $filetype['type'],
			'post_title'			=> preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
			'post_content'		=> '',
			'post_status'			=> 'inherit',
		);
		$attach_id      = wp_insert_attachment( $attachment, $filename, $parent_post_id );
		require_once ( ABSPATH . 'wp-admin/includes/image.php');
		// Generate the metadata for the attachment, and update the database record.
		$attach_data = wp_generate_attachment_metadata( $attach_id,  $filename  );
		$return = wp_update_attachment_metadata( $attach_id , $attach_data );
	}
	$return = add_option( 'media_upload_theme_switch', 'true' );
	return $return;
}

/**
 * ファイル名を取得
 * @param $dir
 */

function easythemes_activate_list_files( $dir, $fileonly = false  ) {
  $files = array();
  $list  = scandir( $dir );

	if ( empty( $list ) ) {
		return;
	}
	foreach ( $list as $file ) {
		if ( $fileonly ) {
			if ( $file == '.' || $file == '..' ) {
	      continue;
			} else if ( is_file( $dir . $file ) && ( strstr( $file , '.png' ) || strstr( $file , '.jpg' ) ) ) {
	      $files[] = $file;
			} else if ( is_dir( $dir . $file ) ) {
			  $files = array_merge( $files, easythemes_activate_list_files(  $file . '/' ) );
			}
		} else {
			if ( $file == '.' || $file == '..' ) {
			  continue;
			} else if ( is_file( $dir . $file ) && ( strstr( $file , '.png' ) || strstr( $file , '.jpg' ) ) ) {
			  $files[] = $dir . $file ;
			} else if ( is_dir( $dir . $file ) ) {
			  $files = array_merge( $files, easythemes_activate_list_files( $dir . $file . '/' ) );
			}
  	}
  }
  return $files;
}

add_filter( 'admin_init', 'free_image_import', 10, 1 );
function free_image_import(){
	if ( ! is_admin() || ! is_user_logged_in() && !isset( $_GET['free_image_import'] ) ){
		return;
	}
	$flg = $_GET['free_image_import'];
	if ( $flg === 'on' ) {
		media_upload_theme_switch();
		$return = update_option( 'media_upload_theme_switch', 'true' );
	}
}
