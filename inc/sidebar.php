<?php
/**
 * Register Sidebar
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */

add_action( 'widgets_init', 'easy_theme_sidebar_init', 10 );

function easy_theme_sidebar_init(){

	register_sidebars( 1,
			array(
			'id' => 'sidebar-primary',
			'name' => 'サイドバーエリア',
			'before_widget' => '<section id="%1$s" class="widget--block %2$s">',
			'after_widget'  => '</section>',
			'before_title' => '<h2 class="widget--title theme_radius">',
			'after_title' => '</h2>',
			)
	);

	register_sidebars( 1,
		array(
			'id' => 'main-visual',
			'name' => 'メインビジュアルエリア ',
			'before_widget' => '<section id="%1$s" class="widget--block %2$s">',
			'after_widget'  => '</section>',
			'before_title' => '<h2 class="widget--title theme_radius">',
			'after_title' => '</h2>',
		)
	);

	register_sidebars( 1,
		array(
			'id' => 'main-primary',
			'name' => 'メインカラムエリア : 上部',
			'before_widget' => '<section id="%1$s" class="widget--block %2$s">',
			'after_widget'  => '</section>',
			'before_title' => '<h2 class="widget--title theme_radius">',
			'after_title' => '</h2>',
		)
	);

	register_sidebars( 1,
		array(
			'id' => 'main-secondary',
			'name' => 'メインカラムエリア : 下部',
			'before_widget' => '<section id="%1$s" class="widget--block %2$s">',
			'after_widget'  => '</section>',
			'before_title' => '<h2 class="widget--title theme_radius">',
			'after_title' => '</h2>',
		)
	);

	register_sidebars( 1,
			array(
			'id' => 'sidebar-right',
			'name' => '右サイドバーエリア',
			'before_widget' => '<section id="%1$s" class="widget--block %2$s">',
			'after_widget'  => '</section>',
			'before_title' => '<h2 class="widget--title theme_radius">',
			'after_title' => '</h2>',
			)
	);

	register_sidebars( 1,
			array(
			'id' => 'footer-one',
			'name' => 'フッター（左から1番目）',
			'before_widget' => '<section id="%1$s" class="widget--block %2$s">',
			'after_widget'  => '</section>',
			'before_title' => '<h2 class="widget--title theme_radius">',
			'after_title' => '</h2><!-- /.widget--title -->',
		)
	);

	register_sidebars( 1,
			array(
			'id' => 'footer-two',
			'name' => 'フッター（左から2番目）',
			'before_widget' => '<section id="%1$s" class="widget--block %2$s">',
			'after_widget'  => '</section>',
			'before_title' => '<h2 class="widget--title theme_radius">',
			'after_title' => '</h2><!-- /.widget--title -->',
		)
	);

	register_sidebars( 1,
			array(
			'id' => 'footer-three',
			'name' => 'フッター（左から3番目）',
			'before_widget' => '<section id="%1$s" class="widget--block %2$s">',
			'after_widget'  => '</section>',
			'before_title' => '<h2 class="widget--title theme_radius">',
			'after_title' => '</h2><!-- /.widget--title -->',
		)
	);

	register_sidebars( 1,
			array(
			'id' => 'footer-four',
			'name' => 'フッター（左から4番目）',
			'before_widget' => '<section id="%1$s" class="widget--block %2$s">',
			'after_widget'  => '</section>',
			'before_title' => '<h2 class="widget--title theme_radius">',
			'after_title' => '</h2><!-- /.widget--title -->',
	));

}

/**
 * Class Easy Themes Layout
 *
 */

class EasyThemesLayout
{

	public $top_layout = array();
	public $archive_layout = array();
	public $page_layout = array();
	public $single_layout = array();

	/**
	 * construct
	 * @return string class
	 */
	public function __construct()
	{
		$this->set_option();
	}

	/**
	 * set attr class
	 * @return string class
	 */
	public function set_attr( $option )
	{

		$attr = array();

		switch ( $option ) {

			// 1 column
			case '1col':

				$attr['sidebar'] = false;
				$attr['sidebar-right'] = false;
				$attr['main'] = 'one-column';

				break;

			// 2 column left sidebar
			case '2cl':

				$attr['sidebar'] = true;
				$attr['sidebar-right'] = false;
				$attr['main'] = 'two-column';

				break;

			// 2 column right sidebar
			case '2cr':

				$attr['sidebar'] = false;
				$attr['sidebar-right'] = true;
				$attr['main'] = 'two-column';

				break;

			// 3 column right & left sidebar
			case '3col':

				$attr['sidebar'] = true;
				$attr['sidebar-right'] = true;
				$attr['main'] = 'three-column';

				break;

			default:

				$attr['sidebar'] = true;
				$attr['sidebar-right'] = false;
				$attr['main'] = 'two-column';

				break;
		}

		return $attr;

	}

	/**
	 * set option attr
	 */
	public function set_option()
	{

		global $easy_themes;

		$top_layout     = isset( $easy_themes['top_layout'] )     ? $easy_themes['top_layout'] : array();
		$archive_layout = isset( $easy_themes['archive_layout'] ) ? $easy_themes['archive_layout'] : array();
		$page_layout    = isset( $easy_themes['page_layout'] )    ? $easy_themes['page_layout'] : array();
		$single_layout  = isset( $easy_themes['single_layout'] )  ? $easy_themes['single_layout'] : array();

		$this->top_layout     = $this->set_attr( $easy_themes['top_layout'] );
		$this->archive_layout = $this->set_attr( $easy_themes['archive_layout'] );
		$this->page_layout    = $this->set_attr( $easy_themes['page_layout'] );
		$this->single_layout  = $this->set_attr( $easy_themes['single_layout'] );

	}

	/**
	 * get layout
	 * @param  string $page : description
	 * @param  string $element [description]
	 * @return [type]          [description]
	 */
	public function get_page_layout( $element = '' )
	{

		$page = '';

		if ( $element == '' ) {
			$element = 'main';
		}

		if ( is_home() || is_front_page() ) {
			$page = 'top_layout';
		} else if ( is_archive() ){
			$page = 'archive_layout';
		} else if ( is_page() ){
			$page = 'page_layout';
		} else if ( is_single() ){
			$page = 'single_layout';
		} else {
			$page = 'top_layout';
		}


		if ( $this->{$page}[$element] ) {
			return $this->{$page}[$element];

		} else {

			return false;

		}

	}

	/**
	 * Get Sidebar Setting
	 * @return boolean [description]
	 */
	public function get_sidebar_setting(  )
	{
		return $this->get_page_layout( 'sidebar' );
	}

	/**
	 * Get sidebar class
	 * @return [type] [description]
	 */
	public function get_sidebar_class(  )
	{

		$class = '';

		if ( $this->get_sidebar_setting() && ! easythemes_is_mobile() ) {
			$class = 'sidebar-left';
		}
		return $class;

	}

	/**
	 * set sidebar right class
	 * @return string class
	 */
	public function get_sidebar_right_setting( )
	{
		return $this->get_page_layout( 'sidebar-right' );
	}

	/**
	 * Get Sidebar right class
	 * @return string class
	 */
	public function get_sidebar_right_class()
	{

		$class = '';

		if ( $this->get_sidebar_right_setting() && ! easythemes_is_mobile() ) {
			$class = 'sidebar-right';
		}

		return $class;

	}

	/**
	 * Get main class
	 * @return string class
	 */
	public function get_main_class()
	{
		$class = '';
		if ( ! easythemes_is_mobile() ) {
			$class = $this->get_page_layout( 'main' ) . ' ' . $this->get_sidebar_right_class() . ' ' . $this->get_sidebar_class();
		}
		return $class;
	}

}

// create instance.
$easythemes_layout = new EasyThemesLayout;
