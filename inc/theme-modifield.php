<?php

/**
 * For Theme Settings
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */


/**
 * easy themes setting
 *
 * @param  string  $key
 * @param  boolean $sanitize
 * @param  boolean $echo
 * @return void
 */
function easy_themes_setting( $key = '', $sanitize = true, $echo = true ){

	global $easy_themes;

	$output = '';

	if ( $key && $easy_themes[$key] ) {
		$output = $easy_themes[$key];
	}

	if ( $sanitize ) {
		$output = esc_html( $output );
	}

	if ( $echo ) {
		echo $output;
	} else {
		return $output;
	}

}


function easythemes_is_mobile() {
	static $is_mobile;

	if ( isset( $is_mobile ) )
			return $is_mobile;

	if ( empty( $_SERVER['HTTP_USER_AGENT'] ) ) {
		$is_mobile = false;
	} elseif ( strpos( $_SERVER['HTTP_USER_AGENT'], 'Android' ) !== false
		|| strpos( $_SERVER['HTTP_USER_AGENT'], 'Silk/' ) !== false
		|| strpos( $_SERVER['HTTP_USER_AGENT'], 'Kindle' ) !== false
		|| strpos( $_SERVER['HTTP_USER_AGENT'], 'BlackBerry' ) !== false
		|| strpos( $_SERVER['HTTP_USER_AGENT'], 'Opera Mini' ) !== false
		|| strpos( $_SERVER['HTTP_USER_AGENT'], 'Opera Mobi' ) !== false
		|| strpos( $_SERVER['HTTP_USER_AGENT'], 'iPhone' ) !== false ) {

		$is_mobile = true;

	} else {
		$is_mobile = false;
	}

	return $is_mobile;
}

/**
 * favicon & appicon settings
 *
 * @return void : echo wp_head
 */

add_action( 'wp_head', 'easythemes_favicon', 10 );

function easythemes_favicon(){

	$output = '';

	$favicon = easy_themes_setting( 'custom_favicon', false , false );
	$appicon = easy_themes_setting( 'custom_appicon', false , false );

	if ( $favicon ) {

		$output .= '
<meta name="mobile-web-app-capable" content="yes">
<link rel="icon" sizes="196x196" href="' . $favicon['url'] . '">';

	}

	if ( $appicon ) {

		$output .= '
<meta name="msapplication-TileImage" content="' . $appicon['url'] . '">
';

	}

	$output .= '
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-title" content="' . get_bloginfo( 'title' ) . '">
';

	echo $output;

}

/**
 * Google Analytics
 */

add_action( 'wp_footer', 'easythemes_google_analytics', 10 );

function easythemes_google_analytics(){

	$output = '';
	$code   = easy_themes_setting( 'google_analytics', false , false );

	if ( $code && ! is_user_logged_in() ) {
		$output = $code;
	}

	echo $output;

}


/**
 * Header file path
 * @return [type] [description]
 */
function easythemes_header_path(){
	$header_type = easy_themes_setting( 'header_type', false, false );

	if ( 1 === intval( $header_type ) ) {
		get_template_part( 'modules/header' );

	} else {

		get_template_part( 'modules/header-max-width' );

	}

}


/**
 * Footer file path
 * @return [type] [description]
 */
function easythemes_footer_path(){
	$footer_type = easy_themes_setting( 'footer_type', false, false );

	if ( 1 === intval( $footer_type ) ) {
		get_template_part( 'modules/footer' );

	} else {

		get_template_part( 'modules/footer-max-width' );

	}

}

add_action( 'get_header', 'easythemes_infomartion_bar' );

function easythemes_infomartion_bar(){
	$output = '';

	$disp_ib = easy_themes_setting( 'infobar_display', false, false );

	if ( $disp_ib == 'false' ) {
		return ;
	}

	$ib_text = easy_themes_setting( 'info_bar_news', false, false );
	$output .= <<<EOF
<script>
$(function(){
	var news_ticker = $('#news_ticker').newsTicker({
		row_height: 20,
		max_rows: 1,
		duration: 3000,
		prevButton: $('#news-ticker-prev'),
		nextButton: $('#news-ticker-next')
	});
});
</script>
EOF;
	$output .= '<div class="information-bar">
	<span class="label ticker--label theme-color theme-color--bg">新着情報</span>
	<ul id="news_ticker">';

	foreach ( $ib_text as $key => $value ) {
		$output .= '<li>' . $value . '</li>';
	}

	$output .= '</ul></div>';

	echo $output;

}


add_action( 'wp_enqueue_scripts', 'easythemes_infomartion_bar_scripts' );
/**
 * easythemes infomartion bar scripts
 * @return [type] [description]
 */
function easythemes_infomartion_bar_scripts(){

	$disp_ib = easy_themes_setting( 'infobar_display', false, false );

	if ( $disp_ib == 'true' ) {
		wp_register_script( 'news_ticker_script', get_template_directory_uri() . '/assets/js/news-ticker.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'news_ticker_script' );
	}

}

/**
 * ===============================
 * Require TitanFramework
 * ===============================
 */
// if ( class_exists( TitanFramework ) ) {

/**
 * facebook js sdk をwp_footerへ埋め込み
 *
 * @package growcreater
 * @return javascript
 */
// if ( function_exists('affiliate_tools_get_option') ) {


/**
 * scroll top
 */
add_action( 'wp_footer', 'add_footer_scrolltop_init', 10 ,1 );
function add_footer_scrolltop_init()
{
	$pagetop = easy_themes_setting( 'page_top', false, false );
	if ( 'true' === $pagetop ) {

		echo '<a href="#body" class="scroll-to-top theme-color theme-color--bg"><i class="dashicons dashicons-arrow-up-alt2"></i> ページトップへ</a>';
	}

}

/**
 * SEOメタタグ設定
 *
 * @return string shortcode
 */

add_filter( 'wp_head', 'easythemes_override_seo_meta', 0,  1 );

function easythemes_override_seo_meta()
{
	global $post,$facebook_loader;

	$meta_tag = '';


	if ( is_single() || is_page() ) {
		$seo_description	= ( SmartMetaBox::get( 'seo_description' ) )  ? SmartMetaBox::get( 'seo_description' ) : '';
		$seo_keyword			= ( SmartMetaBox::get( 'seo_keyword' ) ) ? SmartMetaBox::get( 'seo_keyword' ) : '';
		$ogp_title				= ( SmartMetaBox::get( 'ogp_title' ) ) ? SmartMetaBox::get( 'ogp_title' ) : get_the_title( $post->ID );
		$ogp_description	= ( SmartMetaBox::get( 'ogp_description' ) ) ? SmartMetaBox::get( 'ogp_description' ) : mb_substr( get_the_content(), 0, 50 );
		$ogp_image				= ( SmartMetaBox::get( 'ogp_image' ) ) ? SmartMetaBox::get( 'ogp_image' ) : wp_get_attachment_url( $post->ID );
		$url							= get_permalink( $post->ID );
	} else {
		$seo_title				= ( SmartMetaBox::get( 'seo_title' ) ) ? SmartMetaBox::get( 'seo_title' ) : get_bloginfo( 'name' );
		$seo_description	= ( SmartMetaBox::get( 'seo_description' ) )  ? SmartMetaBox::get( 'seo_description' ) : easy_themes_setting( 'seo_description', false, false );
		$seo_keyword			= ( SmartMetaBox::get( 'seo_keyword' ) ) ? SmartMetaBox::get( 'seo_keyword' ) : easy_themes_setting( 'seo_keyword', false, false );
		$ogp_title				= ( SmartMetaBox::get( 'ogp_title' ) ) ? SmartMetaBox::get( 'ogp_title' ) : get_bloginfo( 'name' );
		$ogp_description	= ( SmartMetaBox::get( 'ogp_description' ) ) ? SmartMetaBox::get( 'ogp_description' ) : easy_themes_setting( 'seo_description', false, false );
		$ogp_image				= ( SmartMetaBox::get( 'ogp_image' ) ) ? SmartMetaBox::get( 'ogp_image' ) : easy_themes_setting( 'ogp_image', false, false );
		$url							= get_permalink( $post->ID );
	}
	if ( isset( $ogp_image['url'] ) ) {
		$ogp_image = $ogp_image['url'];
	}
	$blog_name = get_bloginfo( 'name' );

	$meta_tag .= '
<title>' . easythemes_seo_title( '|', false,'right' ) . '</title>
	';
				// description
	$meta_tag .= '
<meta name="description" content="' . $seo_description . '">
	';
				// keyword
	$meta_tag .= '
<meta name="keywords" content="' . $seo_keyword . '">
	';
	if ( isset( $facebook_loader ) ) {
		if ( isset( $facebook_loader->locale ) )
			$meta_tag .= '
<meta property="og:locale" content="' . $facebook_loader->locale . '">
';
		if ( isset( $facebook_loader->credentials ) && isset( $facebook_loader->credentials['app_id'] ) && $facebook_loader->credentials['app_id'] )
			$meta_tag .= '
<meta property="fb:app_id" content="' . $facebook_loader->credentials['app_id'] . '">
';
	}

		$meta_tag .= '
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />

<meta property="og:type" content="blog">
<meta property="og:description" content="'. $ogp_description . '">
<meta property="og:title" content="' . $ogp_title . '">
<meta property="og:url" content="' . $url . '">
<meta property="og:site_name" content="' . $blog_name . '">
<meta property="og:image" content="' . $ogp_image . '">
		';
	if ( 'true' === easy_themes_setting( 'seo_disp', false, false ) ) {
		echo $meta_tag;
	}
}

/**
 * タイトルを置換
 *
 */

function easythemes_seo_title( $sep = '&raquo;', $display = true, $seplocation = '' )
{
	global $wpdb, $wp_locale, $post;
	// $growcreater = TitanFramework::getInstance( 'growcreater' );
	$post_title = ( SmartMetaBox::get( 'seo_title' ) ) ? SmartMetaBox::get( 'seo_title' ) : get_the_title( $post->ID );
	$m = get_query_var( 'm' );
	$year = get_query_var( 'year' );
	$monthnum = get_query_var( 'monthnum' );
	$day = get_query_var( 'day' );
	$search = get_query_var( 's' );
	$title = '';

	$t_sep = '%WP_TITILE_SEP%';

	if ( is_single() || ( is_home() && !is_front_page() ) || ( is_page() && !is_front_page() ) ) {

		if ( $post_title ) {

			$title = $post_title;

		} else {

			$title = single_post_title( '', false );

		}

	}

	// If there's a post type archive
	if ( is_post_type_archive() ) {

		$post_type = get_query_var( 'post_type' );

		if ( is_array( $post_type ) )
			$post_type = reset( $post_type );

		$post_type_object = get_post_type_object( $post_type );

		if ( ! $post_type_object->has_archive )
			$title = post_type_archive_title( '', false );

	}

	// If there's a category or tag
	if ( is_category() || is_tag() )
		$title = single_term_title( '', false );

	// If there's a taxonomy
	if ( is_tax() ) {

		$term = get_queried_object();

		if ( $term ) {
			$tax = get_taxonomy( $term->taxonomy );
			$title = single_term_title( $tax->labels->name . $t_sep, false );
		}

	}

	// If there's an author
	if ( is_author() ) {
					$author = get_queried_object();
					if ( $author )
									$title = $author->display_name;
	}

	// Post type archives with has_archive should override terms.
	if ( is_post_type_archive() && $post_type_object->has_archive )
					$title = post_type_archive_title( '', false );

	// If there's a month
	if ( is_archive() && ! empty( $m ) ) {
		$my_year = substr( $m, 0, 4 );
		$my_month = $wp_locale->get_month( substr( $m, 4, 2 ) );
		$my_day = intval( substr( $m, 6, 2 ) );
		$title = $my_year . ( $my_month ? $t_sep . $my_month : '' ) . ( $my_day ? $t_sep . $my_day : '' );
	}

	// If there's a year
	if ( is_archive() && ! empty( $year ) ) {
		$title = $year;

		if ( ! empty( $monthnum ) )
			$title .= $t_sep . $wp_locale->get_month( $monthnum );

		if ( ! empty( $day ) )
			$title .= $t_sep . zeroise( $day, 2 );
	}

	// If it's a search
	if ( is_search() ) {
		/* translators: 1: separator, 2: search phrase */
		$title = sprintf( __( 'Search Results %1$s %2$s' ), $t_sep, strip_tags( $search ) );
	}

	// If it's a 404 page
	if ( is_404() ) {
		$title = __( 'Page not found' );
	}

	$prefix = '';
	if ( ! empty( $title ) )
		$prefix = " $sep ";

	// Determines position of the separator and direction of the breadcrumb
	if ( 'right' == $seplocation ) { // sep on right, so reverse the order

		$title_array = explode( $t_sep, $title );
		$title_array = array_reverse( $title_array );
		$title = implode( " $sep ", $title_array ) . $prefix;

	} else {

		$title_array = explode( $t_sep, $title );
		$title = $prefix . implode( " $sep ", $title_array );

	}

	/**
	 * Filter the text of the page title.
	 *
	 * @since 2.0.0
	 *
	 * @param string $title       Page title.
	 * @param string $sep         Title separator.
	 * @param string $seplocation Location of the separator (left or right).
	 */
	$title = apply_filters( 'easy_themes_wp_title', $title, $sep, $seplocation );

	global $page, $paged;

	$title .= get_bloginfo( 'name', 'display' );

	$site_description = get_bloginfo( 'description', 'display' );

	if ( $site_description && ( is_home() || is_front_page() ) ) {

			$title .= " $sep $site_description";

	}

	if ( $paged >= 2 || $page >= 2 ) {

			$title .= " $sep " . sprintf( __( 'Page %s', 'growcreater' ) , max( $paged, $page ) );

	}

	// Send it out
	if ( $display )

		echo $title;

	else
		return $title;

}

