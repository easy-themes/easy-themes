<?php
/**
 * Repeater Field - Image module
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
?>

<div class="row-fluid">
  <?php if($image_link != NULL){ echo '<a href="'. $image_link.'">'; }?>
  <img src="<?php if($image_image_upload != NULL){ echo $image_image_upload;}?>" alt="" class="custom <?php if($image_shadow == 'on'){ echo "shadow";}?>" />
  <?php if($image_link != NULL){ echo '</a>'; }?>
<!-- /画像ブロック -->
</div>
