<?php
/**
 * Repeater Field - Contact module
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
?>

<div class="clearfix"></div>

<<?php
// title
echo esc_attr( $title_type ); ?> class="widget--title">
	<?php echo esc_attr( $contact_title );?>
</<?php echo esc_attr( $title_type ); ?>>

<?php
function wp_get_the_ip() {
	if ( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
		return $_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	elseif ( isset( $_SERVER['HTTP_CLIENT_IP'] ) ) {
		return $_SERVER['HTTP_CLIENT_IP'];
	}
	else {
		return $_SERVER['REMOTE_ADDR'];
	}
}

$sent = '';
$info = '';

$attss = isset( $atts ) ? $atts : '';

$form_data = isset( $form_data ) ? $form_data : '';

extract(
	shortcode_atts(
		array(
			'email' => get_bloginfo( 'admin_email' ),
			'subject' => '',
			'label_name' => 'お名前',
			'label_email' => 'メールアドレス',
			'label_subject' => 'タイトル',
			'label_message' => 'メッセージ',
			'label_submit' => $contact_submit_text,
			'error_empty' => '空の項目があります。',
			'error_noemail' => 'メールアドレスが正しくありません。',
			'success' => 'お問い合わせありがとうございます。',
		),
	$attss
	)
);

$result = '';
if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
	$error = false;
	$required_fields = array( 'your_name', 'email', 'message', 'subject' );

	foreach ( $_POST as $field => $value ) {
		if ( get_magic_quotes_gpc() ) {
			$value = stripslashes( $value );
		}
		$form_data[$field] = strip_tags( $value );
	}

	foreach ( $required_fields as $required_field ) {
		$value = trim( $form_data[$required_field] );
		if ( empty( $value ) ) {
			$error = true;
			$result = $error_empty;
		}
	}

	if ( ! is_email( $form_data['email'] ) ) {
		$error = true;
		$result = $error_noemail;
	}

	if ( $error == false ) {
		$email_subject = '[' . get_bloginfo( 'name' ) . '] ' . $form_data['subject'];
		$email_message = $form_data['message'] . '
		IP: ' . wp_get_the_ip() . '
		このメッセージは' . get_bloginfo( 'name' ) . 'から送信されました。';
		$headers  = 'From: ' . $form_data['your_name'] . ' <' . $form_data['email'] . '>\n';
		$headers .= "Content-Type: text/plain; charset=UTF-8\n";
		$headers .= "Content-Transfer-Encoding: 8bit\n";
		wp_mail( $email, $email_subject, $email_message, $headers );
		$result = 'お問い合わせが完了しました';
		$sent = true;
	}
}

if ( $result ) {
	$info = '<div class="alert">'.$result.'</div>';
}

$your_name = isset( $form_data['your_name'] ) ? $form_data['your_name'] : '';
$email = isset( $form_data['email'] ) ? $form_data['email'] : '';
$form_subject = isset( $form_data['subject'] ) ? $form_data['subject'] : '';
$message = isset( $form_data['message'] ) ? $form_data['message'] : '';
$email_form = '
<div class="row-fluid fields contact-form">
	<form method="post" action="' . get_permalink() . '">
		<table class="table-striped table">
			<tbody>
				<tr class="row-fluid">
					<th class="span8 headding"><label for="cf_name" class="span8">' . $label_name . ' :</label></th>
					<td class="span16"><span class="span16"><input type="text" name="your_name" id="cf_name" size="50" maxlength="50" value="'.$your_name.'" /></span></td>
				</tr>
				<tr class="row-fluid">
					<th class="span8 headding"><label for="cf_email" class="span8">'.$label_email.':</label></th>
					<td class="span16"><span class="span16"><input type="text" name="email" id="cf_email" size="50" maxlength="50" value="'.$email.'" /></span></td>
				</tr>
				<tr class="row-fluid">
					<th class="span8 headding"><label for="cf_subject" class="span8">'.$label_subject.':</label></th>
					<td class="span16"><span class="span16"><input type="text" name="subject" id="cf_subject" size="50" maxlength="50" value="'. $subject . $form_subject.'" /></span></td>
				</tr>
				<tr class="row-fluid">
					<th class="span8 headding"><label for="cf_message" class="span8">'.$label_message.':</label></th>
					<td class="span16">
						<span class="span16"><textarea name="message" id="cf_message" cols="50" rows="15">' . $message . '</textarea>
					</span></td>
				</tr>
			</tbody>
		</table>
		<p class="submit">
			<input type="submit" class="btn btn-large" value="' . $contact_submit_text.'" name="send" id="cf_send" style="color:'.$contact_submit_text_color.'; background: '.$data['theme_colorpicker'].' ;"/>
		</p>
	</form>
</div>';

if ( $sent ) {
	echo $info;
} else {
	echo $info . $email_form;
}
?>

<!-- /お問い合わせ -->
<div class="clear-fix"></div>
