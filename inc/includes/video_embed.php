<?php
/**
 * Repeater Field - Video Embed
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
?>
<div class="clearfix"></div>
<div class="row-fluid video_embed">

	<div class="span24 <?php echo $video_align;?> ">

		<?php echo wp_oembed_get( $video_code, array( 'width' => $video_width, 'height' => $video_height ) );?>

	</div>
<!-- /動画ブロック -->
</div>
<div class="clearfix"></div>
