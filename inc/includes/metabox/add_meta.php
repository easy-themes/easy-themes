<?php
/**
 * add metabox to post
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */

global $post;

add_smart_meta_box( 'layout-editor',
	array(
		'title' => 'レイアウトブロック設定',
		'pages' => array(
			'post',
			'page'
		),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'id' => 'content-layout',
				'type' => 'content_layout',
			),
		)
	)
);

/**
 * Post Layout Settings
 */
// add_smart_meta_box( 'custom-posts',
// 	array(
// 		'title' => 'レイアウト設定',
// 		'pages' => array( 'post' ),
// 		'context' => 'normal',
// 		'priority' => 'high',
// 		'fields' => array(
// 			array(
// 				'name' => 'ページレイアウト',
// 				'id' => 'sidebar_layout',
// 				'desc' => 'サイドバーの位置を決めてください。',
// 				'type' => 'radio_images',
// 				'default' => 'right_sidebar',
// 				'options' => array(
// 					'no_sidebar' => 'no_sidebar',
// 					'right_sidebar' => 'right_sidebar',
// 					'left_sidebar' => 'left_sidebar'
// 				),
// 			),
// 		),
// 	)
// );

/**
 * Custom SEO
 */
add_smart_meta_box( 'custom-seo',
	array(
		'title' => 'SEO 設定',
		'pages' => array( 'post', 'page' ),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'name' => 'SEO : タイトル',
				'id' => 'seo_title',
				'desc' => 'タイトルを入力してください。※ デフォルトでは、記事タイトルがそのまま適用されます。',
				'type' => 'text',
				'default' => '',
			),
			array(
				'name' => 'SEO : 説明文',
				'id' => 'seo_description',
				'desc' => 'SEO 説明文を入力してください。※ デフォルトでは、サイトの説明文が適用されます。',
				'type' => 'textarea',
				'default' => '',
			),
			array(
				'name' => 'SEO : キーワード',
				'id' => 'seo_keyword',
				'desc' => 'SEO キーワードを入力してください。 ※ デフォルトでは、テーマ設定での SEO キーワードが適用されます',
				'type' => 'text',
				'default' => '',
			),
			array(
				'name' => 'OGP : タイトル',
				'id' => 'ogp_title',
				'desc' => 'OGP タイトルを入力してください。※ デフォルトでは、記事タイトルがそのまま適用されます。',
				'type' => 'text',
				'default' => '',
			),
			array(
				'name' => 'OGP : 説明文',
				'id' => 'ogp_description',
				'desc' => 'OGP 説明文を入力してください。※ デフォルトでは、記事本文の先頭50文字が適用されます。',
				'type' => 'textarea',
				'default' => '',
			),
					array(
				'name' => 'OGP : イメージ画像',
				'id' => 'ogp_image',
				'desc' => 'OGP イメージ画像をアップロードしてください。※ デフォルトでは、テーマ設定のOGP イメージが適用されます。',
				'type' => 'file',
				'default' => '',
			),
		),
	)
);


/**
 * Page Layout Setting
 */
// add_smart_meta_box( 'custom-page',
// 	array(
// 		'title' => 'レイアウト設定',
// 		'pages' => array('page'),
// 		'context' => 'normal',
// 		'priority' => 'high',
// 		'fields' => array(
// 			array(
// 			'name' => 'サイドバーの設定',
// 			'id' => 'sidebar_layout',
// 			'desc' => 'サイドバーの位置を設定してください。',
// 			'type' => 'radio_images',
// 			'default' => 'right_sidebar',
// 			'options' => array(
// 				'no_sidebar' => 'no_sidebar',
// 				'right_sidebar' => 'right_sidebar',
// 				'left_sidebar' => 'left_sidebar'
// 				),
// 			),
// 		),
// 	)
// );
