<?php
/**
 * Separator field
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
?>
<h2 style="color:#333;font-size:14px;font-family:arial;font-style:normal;font-weight:bold;letter-spacing:.5px;margin:5px 0 0 0;padding-left:3px;padding-bottom:0;"><?php echo $title ?></h2>
