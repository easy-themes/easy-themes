<?php
/**
 * text field
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
?>
<input type="text" name="<?php echo $id?>" id="<?php echo $id?>" value="<?php echo esc_attr($value)?><?php if( $value == '' && $id == $prefix . 'headline_text'){ echo get_the_title();}?>" class="regular-text" />
