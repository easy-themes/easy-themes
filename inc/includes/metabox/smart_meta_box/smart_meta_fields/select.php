<?php
/**
 * Select field
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */

if ( $type === 'template' ){
	$templates['one']   = easy_themes_setting( 'template_text_one', false, false );
	$templates['two']   = easy_themes_setting( 'template_text_two', false, false );
	$templates['three'] = easy_themes_setting( 'template_text_three', false, false );
	$templates['four']  = easy_themes_setting( 'template_text_four', false, false );
	$templates['five']  = easy_themes_setting( 'template_text_five', false, false );
	$templates['six']   = easy_themes_setting( 'template_text_six', false, false );
	$templates['seven'] = easy_themes_setting( 'template_text_seven', false, false );
	$templates['eight'] = easy_themes_setting( 'template_text_eight', false, false );
	$templates['nine']  = easy_themes_setting( 'template_text_nine', false, false );
	$templates['ten']   = easy_themes_setting( 'template_text_ten', false, false );
	?>
	<select name="<?php
	echo esc_attr( $id );
	if ( isset( $multiple ) && $multiple ) { echo '[]';}; ?>" id="<?php echo esc_attr( $id );?>" class="chosenfield">
<?php
	foreach ( $templates as $template_key => $template ) { ?>
		<option <?php selected( $value, $template_key );?> value="<?php echo $template_key; ?>"><?php echo esc_attr( $template );?></option>
		<?php
	} ?>
	</select>
	<?php $multiple = false ?>
<?php
} else { ?>
	<select name="<?php echo $id?><?php if( isset( $multiple ) && $multiple ) echo '[]'?>" id="<?php echo $id?>" <?php if ( isset( $multiple ) && $multiple) echo 'multiple'?> class="chosenfield">

	<?php foreach ($options as $opt_value=>$opt_name): ?>
		<option <?php
			if(isset($multiple) && $multiple && is_array($value))
				selected(in_array($opt_value, $value), true);
			else
				selected($value, $opt_value);
			?> value="<?php echo $opt_value?>"><?php echo $opt_name?></option>
	<?php endforeach ?>
</select>
<?php $multiple = false ?>
<?php }  ?>
