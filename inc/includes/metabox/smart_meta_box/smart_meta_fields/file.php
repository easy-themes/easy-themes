<?php
/**
 * file upload field
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */

if ( ! function_exists( 'optionsframework_mlu_get_silentpost' ) ) {

  function optionsframework_mlu_get_silentpost ( $_token ) {

    global $wpdb;
    $_id = 0;

    // Check if the token is valid against a whitelist.
    // $_whitelist = array( 'of_logo', 'of_custom_favicon', 'of_ad_top_image' );
    // Sanitise the token.

    $_token = strtolower( str_replace( ' ', '_', $_token ) );

    // if ( in_array( $_token, $_whitelist ) ) {
    if ( $_token ) {

      // Tell the function what to look for in a post.

      $_args = array( 'post_type' => 'optionsframework', 'post_name' => 'of-' . $_token, 'post_status' => 'draft', 'comment_status' => 'closed', 'ping_status' => 'closed' );

      // Look in the database for a "silent" post that meets our criteria.
      $query = 'SELECT ID FROM ' . $wpdb->posts . ' WHERE post_parent = 0';
      foreach ( $_args as $k => $v ) {
        $query .= ' AND ' . $k . ' = "' . $v . '"';
      } // End FOREACH Loop

      $query .= ' LIMIT 1';
      $_posts = $wpdb->get_row( $query );

      // If we've got a post, loop through and get it's ID.
      if ( count( $_posts ) ) {
        $_id = $_posts->ID;
      } else {

        // If no post is present, insert one.
        // Prepare some additional data to go with the post insertion.
        $_words = explode( '_', $_token );
        $_title = join( ' ', $_words );
        $_title = ucwords( $_title );
        $_post_data = array( 'post_title' => $_title );
        $_post_data = array_merge( $_post_data, $_args );
        $_id = wp_insert_post( $_post_data );
      }
    }
    return $_id;
  }
}
    $int = optionsframework_mlu_get_silentpost('media_upload');
?>
<script type="text/javascript">
  /**
    * Media Uploader
    * Dependencies   : jquery, wp media uploader
    * Feature added by : Smartik - http://smartik.ws/
    * Date       : 05.28.2013
    */
  function optionsframework_add_file(event, selector) {

    var upload = jQuery(".uploaded-file"), frame;
    var $el = jQuery(this);

    event.preventDefault();

    // If the media frame already exists, reopen it.
    if ( frame ) {
      frame.open();
      return;
    }

    // Create the media frame.
    frame = wp.media({
      // Set the title of the modal.
      title: $el.data('choose'),

      // Customize the submit button.
      button: {
        // Set the text of the button.
        text: $el.data('update'),
        // Tell the button not to close the modal, since we're
        // going to refresh the page when the image is selected.
        close: false
      }
    });

    // When an image is selected, run a callback.
    frame.on( 'select', function() {
      // Grab the selected attachment.
      var attachment = frame.state().get('selection').first();
      frame.close();
      selector.find('.upload').val(attachment.attributes.url);
      if ( attachment.attributes.type == 'image' ) {
        selector.find('.screenshot').empty().hide().append('<img class="of-option-image" src="' + attachment.attributes.url + '">').slideDown('fast');
      }
      selector.find('.media_upload_button').unbind();
      selector.find('.remove-image').show().removeClass('hide');//show "Remove" button
      selector.find('.of-background-properties').slideDown();
      optionsframework_file_bindings();
    });

    // Finally, open the modal.
    frame.open();
  }

  function optionsframework_remove_file(selector) {
    selector.find('.remove-image').hide().addClass('hide');//hide "Remove" button
    selector.find('.upload').val('');
    selector.find('.of-background-properties').hide();
    selector.find('.screenshot').slideUp();
    selector.find('.remove-file').unbind();
    // We don't display the upload button if .upload-notice is present
    // This means the user doesn't have the WordPress 3.5 Media Library Support
    if ( jQuery('.section-upload .upload-notice').length > 0 ) {
      jQuery('.media_upload_button').remove();
    }
    optionsframework_file_bindings();
  }

  function optionsframework_file_bindings() {
    jQuery('.remove-image, .remove-file').on('click', function() {
      optionsframework_remove_file( jQuery(this).parents('.section-upload, .section-media, .slide_body') );
        });

        jQuery('.media_upload_button').unbind('click').click( function( event ) {
          optionsframework_add_file(event, jQuery(this).parents('.section-upload, .section-media, .slide_body'));
        });
    }

    optionsframework_file_bindings();
</script>

<div class="section-upload">

<input value="<?php echo $value ?>" id="media_upload_inputbox<?php echo $id?>" name="<?php echo $id?>" class="upload of-input media-upload-inputbox">
<div class="upload_button_div" style="padding-top: 5px;">
    <span rel="<?php echo $int;?>" id="media_upload<?php echo $id?>" class="button media_upload_button">アップロード</span>
    <span title="media_upload" id="reset_media_upload<?php echo $id?>" class="button mlu_remove_button hide" style="display: none;">削除</span>
</div>
<div class="screenshot"></div>
<div class="clear"></div>

</div>
