<?php
/**
 * Textarea field
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */

if ( $type === 'squeeze' ) {

	echo '<textarea name="' . esc_attr( $id ) . '" id="' . esc_attr( $id ) . '" rows="5" cols="50" class="regular-text">' . $value . '</textarea>';

} else {
	echo '<textarea name="' . esc_attr( $id ) . '" id="' . esc_attr( $id ) . '" rows="5" cols="50" class="regular-text">' . $value . '</textarea>';

	// $ediotr_config = array(
	// 	'textarea_name' => $id,
	// 	'textarea_rows' => '10',
	// 	'media_buttons' => true,
	// 	'quicktags'     => false,
	// 	'drag_drop_upload' => true,
	// );

 //  wp_editor( $value, $id, $editor_config );

}
