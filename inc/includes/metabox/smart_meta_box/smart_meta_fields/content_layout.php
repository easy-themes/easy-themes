<?php
/**
 * content layout main file
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
$areas = include('layout-editor/config/areas.php'); ?>

<div class="content-layout-editor">

	<textarea class="hidden cle-data" name="<?php echo $id?>"><?php echo $value?></textarea>

	<div class="content-layout-blocks" data-id="<?php echo $id?>">
<?php
$saved_areas = json_decode( $value );
if ( ! empty( $saved_areas ) ) {
	foreach ( $saved_areas as $area ) {
		echo cle_generate($area->type, (array)$area);
	}
} else {
	echo cle_generate( 'headline' );
	if ( function_exists( 'bcn_display' ) ) {
    echo cle_generate('breadcrumb');
	}
	echo cle_generate( 'wordpress' );
	echo cle_generate( 'comments' );
}
		?>
		<div class="block-separator">このバーは必ず一番下に設定してください。</div>
	</div>

	<div class="add-area-wrapper">
		<a href="#" class="button add-area"><?php _e( 'ブロックを追加', 'bb' )?></a>
		<div class="hidden areas">
			<?php
foreach ( $areas as $slug => $area ): ?>
			<a href="#" class="new-area" data-area="<?php echo isset( $slug ) ?  $slug : ''; ?>" data-hide="<?php echo isset( $area['hide'] ) ? $area['hide'] : ''; ?>"><?php echo isset( $area['title'] ) ? $area['title'] : '';?></a>
<?php
endforeach; ?>
		</div>
	</div>
</div>
