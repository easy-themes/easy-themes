<?php
/**
 * radio field
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
?>
<input type="text" name="<?php echo $id?>" id="<?php echo $id?>" value="<?php echo $value?>" class="colorfield regular-text" data-hex="true" />
