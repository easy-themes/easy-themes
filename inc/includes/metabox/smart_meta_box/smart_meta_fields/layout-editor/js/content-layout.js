(function($, undefined) {

$(function() {

	if ($("input[name='sidebar_layout']:checked").val() == 'no_sidebar'){
	    $('.radio-label.radio_image.sidebar_layout_no_sidebar').css('border', 'solid 2px #BBBBBB');
	    $('.radio-label.radio_image.sidebar_layout_right_sidebar').css('border','solid 1px #CCCCCC');
	    $('.radio-label.radio_image.sidebar_layout_left_sidebar').css('border','solid 1px #CCCCCC');
	}else if($("input[name='sidebar_layout']:checked").val() == 'right_sidebar'){
	    $('.radio-label.radio_image.sidebar_layout_right_sidebar').css('border','solid 2px #BBBBBB');
	    $('.radio-label.radio_image.sidebar_layout_left_sidebar').css('border','solid 1px #CCCCCC');
	    $('.radio-label.radio_image.sidebar_layout_no_sidebar').css('border','solid 1px #CCCCCC');
	}else if($("input[name='sidebar_layout']:checked").val() == 'left_sidebar'){
	    $('.radio-label.radio_image.sidebar_layout_left_sidebar').css('border','solid 2px #BBBBBB');
	    $('.radio-label.radio_image.sidebar_layout_right_sidebar').css('border','solid 1px #CCCCCC');
	    $('.radio-label.radio_image.sidebar_layout_no_sidebar').css('border','solid 1px #CCCCCC');
	}

	$('.radio-label.radio_image.sidebar_layout_no_sidebar').click(function(){
	    $(this).css('border', 'solid 2px #BBBBBB');
	    $('.radio-label.radio_image.sidebar_layout_right_sidebar').css('border','solid 1px #CCCCCC');
	    $('.radio-label.radio_image.sidebar_layout_left_sidebar').css('border','solid 1px #CCCCCC');
	});

	$('.radio-label.radio_image.sidebar_layout_right_sidebar').click(function(){
	    $(this).css('border','solid 2px #BBBBBB');
	    $('.radio-label.radio_image.sidebar_layout_no_sidebar').css('border','solid 1px #CCCCCC');
	    $('.radio-label.radio_image.sidebar_layout_left_sidebar').css('border','solid 1px #CCCCCC');
	});
	$('.radio-label.radio_image.sidebar_layout_left_sidebar').click(function(){
	    $(this).css('border','solid 2px #BBBBBB');
	    $('.radio-label.radio_image.sidebar_layout_no_sidebar').css('border','solid 1px #CCCCCC');
	    $('.radio-label.radio_image.sidebar_layout_right_sidebar').css('border','solid 1px #CCCCCC');
	});

	$('.chosenfield').chosen();
	$('.colorfield').mColorPicker();

	$('#TB_iframeContent').contents().find('.savesend .button');

//    tbframe_interval = setInterval(function() {jQuery('#TB_iframeContent').contents().find('.savesend .button').click(){editor.};}, 2000);

	$('.content-layout-editor').each(function() {
		var editor = $(this);

		$('.content-layout-block', editor).each(function(i, e) {
			e = $(e);
			if(e.attr('data-hide').length) {
				$('.areas', editor).find('[data-hide="'+e.attr('data-hide')+'"]').hide();
			}
		});

		$('.content-layout-block .field.width select', editor).focus(function() {
			$(this).attr('prev-val', $(this).val());
		});
        $('.content-layout-block .fields .field_wrap .field.width .field-content select', editor).live('change', function() {
			var block = $(this).closest('.content-layout-block');
            block.removeClass('full');
            block.removeClass('one-sixth');
            block.removeClass('one-fifth');
            block.removeClass('one-fourth');
            block.removeClass('one-third');
            block.removeClass('two-fifths');
            block.removeClass('one-half');
            block.removeClass('three-fifths');
            block.removeClass('two-thirds');
            block.removeClass('three-fourths');
            block.removeClass('four-fifths');
            block.removeClass('five-sixths');
			block.addClass($(this).val());

            if (block.hasClass('maximize-active'))
                block.children('.fields').children('.field_wrap').css('border-top-width', '5px');
            else
                block.children('.fields').children('.field_wrap').css('border-top-width', '0px');

			if($(this).attr('prev-val'))
				block.removeClass($(this).attr('prev-val'));
		}).change();

		editor.bind('cle-change', function() {
			var data = [];
			editor.find('.content-layout-blocks .content-layout-block').each(function() {
				var block = $(this);
				var block_prefix = block.attr('data-prefix');

				var block_data = {
					type: block.attr('data-type')
				};

				block.find('input, textarea, select').each(function() {
					var f = $(this);
					if(f.attr('name')) {
						if(!f.is(':radio') || f.is(':checked')) {
							block_data[f.attr('name').replace(block_prefix, '').replace(/\[\]$/, '')] = f.val();
						} else if (f.is(':checkbox') && !f.is(':checked')) {
							block_data[f.attr('name').replace(block_prefix, '')] = false;
						}
					}
				});

                data.push(block_data);
			});

			editor.find('.cle-data').val($.toJSON(data));
		}).trigger('cle-change');

		editor.find('.content-layout-blocks').sortable({
			axis: 'y',
			forcePlaceholderSize: true,
			stop: function() {
				editor.trigger('cle-change');
			},
			containment: 'parent'
		});

		$('input, textarea, select', editor).live('change', function(e) {
			editor.trigger('cle-change');
		});

		tbmedia_interval = setInterval(checkMediaChanged, 200);

        searchValue = new Array();
        $('.media-upload-inputbox').each(function(){
            i = $('.media-upload-inputbox').index(this);
            searchValue[i] = $(this).val();
        });

        function checkMediaChanged() {
            $('.media-upload-inputbox').each(function(){
                i = $('.media-upload-inputbox').index(this);
                currentValue = $(this).val();

                if (currentValue != searchValue[i]) {
                    searchValue[i] = currentValue[i];
                    editor.trigger('cle-change');
                }
            });
        }

        $('.content-layout-block .remove-block', editor).live('click', function() {
			var hide = $(this).parent().attr('data-hide');
			$(this).closest('.content-layout-blocks').siblings().filter('.add-area-wrapper').find('[data-hide="'+hide+'"]').show();


			$(this).closest('.content-layout-block').fadeOut('fast', function() {
				$(this).remove();
				setTimeout(function() {
					editor.trigger('cle-change');
				}, 200);
			});
		});

		$('.content-layout-block .minimize-block', editor).live('click', function() {

		$(this).parent().removeClass('maximize-active');
        $(this).parent().children('.fields').children('.field_wrap').css('border-top-width', '0px');
		});

		$('.content-layout-block .maximize-block', editor).live('click', function() {
		$(this).parent().addClass('maximize-active');
        $(this).parent().children('.fields').children('.field_wrap').css('border-top-width', '5px');
		});

		$('.content-layout-block input[type="radio"]', editor).live('change', function() {
			if($(this).is(':checked')) {
				$(this).closest('label').addClass('active').siblings().removeClass('active');
			}
		}).change();

		$( '.add-area', editor ).click( function( e ) {
			$( this ).siblings().filter('.areas').fadeIn();
			$( this ).focus();
			e.preventDefault();
		}).blur(function() {
			$(this).siblings().filter('.areas').fadeOut();
		});

		$('.areas > a', editor).click(function(e) {
            var button = $(this);
			button.parent().hide();
			if(button.attr('data-hide').length > 0) {
				button.siblings().filter('[data-hide="'+button.attr('data-hide')+'"]').hide();
				button.hide();
			}

			$.post(ajaxurl, {
				'action': 'cle_get_clean',
				'type': button.attr('data-area')
			}, function( data ) {
				button.closest('.content-layout-editor').find('.content-layout-blocks')
						.append(data)
						.find('input[type="radio"]').change();

				editor.find('.content-layout-block:last select').chosen();
				editor.find('.content-layout-block:last .colorfield').mColorPicker();

				editor.trigger('cle-change');
			});

			e.preventDefault();
		});
	});
	$(".datepicker").datepicker({
		dateFormat: 'yy,mm -1 ,dd'
	});

});

})(jQuery);

