<?php


function cle_generate( $type, $atts = array() ) {
  $area = include dirname(__FILE__).'/../config/areas.php';
  $area = $area[$type];
  $hide = isset( $area['hide'] ) ? $area['hide'] : '';

  $prefix = rand( 0, 100000 ) . '-';

  ob_start();
	?>
  <div class="content-layout-block <?php echo $type?>" data-prefix="<?php echo $prefix?>" data-type="<?php echo $type?>" data-hide="<?php echo $hide ?>" style="overflow: hidden;float:left;">

    <h5><?php echo $area['title']?></h5>
    <div class="fields">
      <?php foreach($area['fields'] as $field): ?>
      <div class="field_wrap">
        <div class="field <?php echo $type.'-'.$field['id'].' '.$field['type']?> <?php if(isset($field['class'])) echo $field['class']; ?>">
          <h6><?php echo $field['name']?></h6>
          <?php
            $value = isset($atts[$field['id']]) ? $atts[$field['id']] :
                  (isset($atts[$field['id'].'[]']) ? $atts[$field['id'].'[]'] :

                  (isset($field['default']) ? $field['default'] : ''));

            $id = $prefix . $field['id'];
            $options = isset($field['options']) ? $field['options'] : array();

            $multiple = (isset($field['multiple'])) ? $field['multiple'] : false;

          ?>
          <div class="field-content">
            <?php include get_template_directory() . '/inc/includes/metabox/smart_meta_box/smart_meta_fields/'.$field['type'].'.php'; ?>
          </div>
        </div>
        <div class="desc">
        <p><?php echo isset( $field['desc'] ) ? $field['desc'] : '' ; ?></p>
        </div>
        <div style="clear:both;"></div>
      </div>
      <?php endforeach; ?>
    </div>

    <span class="minimize-block">-</span>
    <span class="maximize-block">+</span>
    <span class="remove-block">x</span>
  </div>

<?php
  return ob_get_clean();
}

/**
 * clean
 * @param  [type] $type [description]
 * @return [type]       [description]
 */
function cle_get_clean( $type ) {
  return cle_generate( $type );
}

/**
 *
 * @return [type] [description]
 */

add_action( 'wp_ajax_cle_get_clean', 'cle_ajax_get_clean' );
function cle_ajax_get_clean() {
  echo cle_get_clean( $_POST['type'] );
  exit;
}
