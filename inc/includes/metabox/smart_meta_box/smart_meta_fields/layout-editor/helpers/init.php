<?php

require_once 'generator.php';

add_action( 'admin_enqueue_scripts', 'contents_layout_script', 10 );
function contents_layout_script(){
	$path = get_template_directory_uri() . '/inc/includes/metabox/smart_meta_box/smart_meta_fields/layout-editor/';
	if ( is_user_logged_in() && is_admin() ) {
	  wp_enqueue_script( 'mediaup', $path . 'js/of-medialibrary-uploader.js', array( 'jquery' ) );
	  wp_enqueue_script( 'mcolorpicker', $path . 'js/mColorPicker.js', array( 'jquery' ) );
	  wp_enqueue_script( 'jquery-ui-datepicker', array('jquery') );
	  wp_enqueue_style( 'chosen', $path . 'css/chosen.css' );
	  wp_enqueue_style( 'content-layout', $path . 'css/content-layout.css' );
	  wp_enqueue_script( 'jquery-json', $path . 'js/jquery.json.js', array( 'jquery' ) );
	  wp_enqueue_script( 'chosen-jquery', $path . 'js/chosen.jquery.min.js', array( 'jquery' ) );
	  wp_enqueue_script( 'content-layout-js', $path . 'js/content-layout.js', array( 'jquery', 'jquery-ui-sortable' ) );
	}
}
