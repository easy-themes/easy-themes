<?php

// $data = of_get_options();
$one   = easy_mod( 'template_text_one', false, false );
$two   = easy_mod( 'template_text_two', false, false );
$three = easy_mod( 'template_text_three', false, false );
$four  = easy_mod( 'template_text_four', false, false );
$five  = easy_mod( 'template_text_five', false, false );
$six   = easy_mod( 'template_text_six', false, false );
$seven = easy_mod( 'template_text_seven', false, false );
$eight = easy_mod( 'template_text_eight', false, false );
$nine  = easy_mod( 'template_text_nine', false, false );
$ten   = easy_mod( 'template_text_ten', false, false );

$choose_img = array(
	'one'   => get_template_directory_uri() . '/assets/images/crown-img-01.png',
	'two'   => get_template_directory_uri() . '/assets/images/crown-img-02.png',
	'three' => get_template_directory_uri() . '/assets/images/crown-img-03.png',
	'four'  => get_template_directory_uri() . '/assets/images/crown-img-04.png',
	'five'  => get_template_directory_uri() . '/assets/images/crown-img-05.png',
	'six'   => get_template_directory_uri() . '/assets/images/badge-img-01.png',
	'seven' => get_template_directory_uri() . '/assets/images/badge-img-02.png',
);

$post_id = isset( $_GET['post'] ) ? $_GET['post'] : '0';
$post    = get_post( $post_id  );
$post_title = isset( $post ) ? $post->post_title : '';

global $post;

return array(
'wordpress' => array(
	'title' => __( '本文 ブロック', 'easythemes' ),
	'fields' => array(
		array(
			'name' => __( 'このブロックは通常の本文が表示されます。<br />
				上記の大きなテキストフォームで入力した内容が表示されます。', 'easythemes' ),
			'id' => 'wordpress-desc',
			'type' => 'description',
			),
	),
),
'blog' => array(
	'title' => __( '投稿記事 ブロック', 'easythemes' ),
	'fields' => array(
		array(
			'name' => __( 'このブロックでは、投稿記事を表示するブロックを簡単に作成出来ます。
			記事の抜粋は<!-- more -->タグを使用して調整することができます。
			また、デフォルトでは最新の3記事を使用し、特定のカテゴリを設定することができます。', 'easythemes' ),
			'id' => 'blog-desc',
			'type' => 'description',
		),
		array(
			'name' => __( '記事数', 'easythemes'),
			'id' => 'posts_per_row',
			'type' => 'select',
			'desc' => '表示する記事数を選択してください。',
				'default' => 3,
				'options' => array(
				1 => 1,2,3,4,5,6,7,8,
			),
		),
		array(
			'name' => __( '見出しタグ', 'easythemes'),
			'id' => 'b_header_type',
			'type' => 'select',
			'desc' => '記事見出しのタグを選択してください。',
			'default' => 'h3',
			'options' => array(
				'h1' => __( 'h1', 'easythemes'),
				'h2' => __( 'h2', 'easythemes'),
				'h3' => __( 'h3', 'easythemes'),
				'h4' => __( 'h4', 'easythemes'),
				'h5' => __( 'h5', 'easythemes'),
				'h6' => __( 'h6', 'easythemes'),
			),
		),
		array(
			'name' => __( '記事抜粋を表示させますか？', 'easythemes'),
			'id' => 'show_text',
			'type' => 'radio',
			'desc' => '表示しないを選択すると記事テキストを表示させません。',
			'default' => 'yes',
			'options' => array(
				'yes' => __( '表示する', 'easythemes' ),
				'no' => __( '表示しない', 'easythemes' ),
			),
		),
		array(
			'name' => __( '画像を表示させますか？', 'easythemes' ),
			'id' => 'show_image',
			'type' => 'radio',
			'desc' => '「表示しない」を選択するとアイキャッチ画像を表示しません。',
			'default' => 'yes',
			'options' => array(
				'yes' => __( '表示する', 'easythemes' ),
				'no' => __( '表示しない', 'easythemes' ),
			),
		),
		array(
			'name' => __( '画像の場所を選択してください。', 'easythemes' ),
			'id' => 'image_location',
			'type' => 'select',
			'desc' => '画像を表示する場所を選択してください。',
			'default' => 'top',
			'options' => array(
				'above image' => __( 'タイトルの上', 'easythemes' ),
				'below image' => __( 'タイトルの下', 'easythemes' ),
			),
		),
		array(
			'name' => __( 'セパレートバーの表示', 'easythemes' ),
			'id' => 'show_bar',
			'type' => 'radio',
			'desc' => '記事と記事の間にセパレートバーを表示しますか?',
			'default' => 'no',
			'options' => array(
				'yes' => '表示する',
				'no' => '表示しない',
				),
			),
		array(
			'name' => __( 'カテゴリ', 'easythemes' ),
			'id' => 'category',
			'type' => 'select',
			'desc' => '記事を読み込むカテゴリを選択してください。',
			'multiple' => true,
			'options' => SmartMetaBox::parse_terms( get_terms( 'category', 'hide_empty=1' ) ),
		),
	),
),



'headline' => array(
	'title' => __( 'ヘッドライン ブロック', 'easythemes' ),
	'fields' => array(
		array(
			'name' => __( 'このブロックを使用するとヘッドライン(大きな見出し)を表示することができます。
				<br><span class="red">※そのままのタイトルを使用する際は一旦更新して下さい。</span>', 'easythemes' ),
			'id' => 'headline-desc',
			'type' => 'description',
		),
		array(
			'name' => __( 'ヘッドラインタイプ', 'easythemes'),
			'desc' => 'header_type',
			'id' => 'header_type',
			'type' => 'select',
			'desc' => '見出しタグを設定してください。',
			'default' => 'h2',
			'options' => array(
				'h1' => __( 'h1', 'easythemes'),
				'h2' => __( 'h2', 'easythemes'),
				'h3' => __( 'h3', 'easythemes'),
				'h4' => __( 'h4', 'easythemes'),
				'h5' => __( 'h5', 'easythemes'),
				'h6' => __( 'h6', 'easythemes'),
			),
		),
		array(
			'name' => __( '見出しテキスト', 'easythemes'),
			'id' => 'headline_text',
			'type' => 'text',
			'default' => $post_title,
			'desc' => 'ヘッドライン内のテキストを設定してください。<br>
			デフォルトは記事のタイトルが入ります。',
		),
		array(
			'name' => __( 'テキストの色', 'easythemes'),
			'id' => 'headline_font_color',
			'type' => 'color',
			'desc' => 'ヘッドラインのテキストの色を指定してください。',
		),
		array(
			'name' => __( '小さなテキスト', 'easythemes'),
			'id' => 'small_text',
			'type' => 'text',
			'desc' => 'ヘッドラインに表示される小さな注訳を入力してください。<br>
			空の場合には表示されません。',
		),
		array(
			'name' => __( 'テキストの整列', 'easythemes'),
			'id' => 'text_align',
			'type' => 'radio',
			'desc' => 'テキストをセンター寄り、左寄りの中から選択してださい。',
			'default' => 'center',
			'options' => array(
				'center' => 'センター寄せ',
				'left' => '左寄せ',
			),
		),
		array(
			'name' => __( '線', 'easythemes'),
			'id' => 'border',
			'type' => 'radio',
			'desc' => 'ヘッドライン枠線の表示・非表示を選択してください。',
			'default' => 'yes',
			'options' => array(
				'yes' => '表示する',
				'no' => '表示しない',
			),
		),
		array(
			'name' => __( '線の色', 'easythemes'),
			'id' => 'border_color',
			'type' => 'color',
			'desc' => '枠線の色を選択してください。',
		),
		array(
			'name' => __( '背景画像をアップロード', 'easythemes'),
			'id' => 'headline_image_upload',
			'type' => 'file',
			'desc' => '背景画像を指定することもできます。',
			),

		array(
			'name' => __( 'カスタムクラス', 'easythemes'),
			'id' => 'custom_classes',
			'type' => 'text',
			'desc' => 'ヘッドラインにclass名を設定出来ます。CSSで調整したい場合などに活用してください。',
			'default' => __( '', 'easythemes'),
		),
	),
),

// SNSブロック
'social' => array(
	'title' => __( 'SNS ブロック', 'easythemes'),
	'fields' => array(
		array(
			'name' => __( 'WP Social Bookmarking Lightを利用してSNSシェアボタンを設置します。<br />
				<span style="color:red">※プラグインは「外観 -> プラグインをインストール」からインストール出来ます。</span>', 'easythemes'),
			'id' => 'social-desc',
			'type' => 'description',
		),
	),
),

// パンくずブロック
'breadcrumb' => array(
	'title' => __( 'パンくず ブロック', 'easythemes'),
	'fields' => array(
		array(
			'name' => __( 'Breadcrumb NavXTを使用してパンくずナビを設置します。<br />
				<span style="color:red">※プラグインは「外観 -> プラグインをインストール」からインストール出来ます。</span>', 'easythemes'),
			'id' => 'social-desc',
			'type' => 'description',
		),
	),
),

// お申込みブロック
'signup' => array(
	'title' => __( 'お申込み ブロック', 'easythemes'),
	'fields' => array(
		array(
			'name' => __( '画像・テキスト・お申込みボタンのブロックです。', 'easythemes'),
			'id' => 'signup-desc',
			'type' => 'description',
		),

		array(
			'name' => __( '画像をアップロード', 'easythemes'),
			'id' => 'signup_image_upload',
			'type' => 'file',
			'desc' => '画像をアップロードするか、メディアライブラリから選んでください。',
			'default' => ''
			),

		array(
			'name' => __( 'テキスト', 'easythemes'),
			'id' => 'signup_text',
			'type' => 'textarea',
			'desc' => '',
			'default' => ''
		),

		array(
			'name' => __( 'リンク先URL', 'easythemes'),
			'id' => 'signup_button_url',
			'type' => 'text',
			'desc' => 'ボタンのリンク先URLを入力してください。',
			'default' => 'http://example.com/'
		),

		array(
			'name' => __( 'ボタンテキスト', 'easythemes'),
			'id' => 'signup_button_text',
			'type' => 'text',
			'desc' => 'ボタン内に表示するテキストを入力して下さい。',
			'default' => 'お申し込みはコチラをクリック'
		),
		array(
			'name' => __( 'ボタン背景グラデーションカラー : 上', 'easythemes'),
			'id' => 'signup_button_color_top',
			'type' => 'color',
			'desc' => 'ボタンのカラーを選んでください。',
			'default' => '#e33b3b'
		),

		array(
			'name' => __( 'ボタン背景グラデーションカラー : 下', 'easythemes'),
			'id' => 'signup_button_color_bottom',
			'type' => 'color',
			'desc' => 'ボタンのカラーを選んでください。',
			'default' => '#8f1717'
		),

		array(
			'name' => __( 'カスタムクラス', 'easythemes'),
			'id' => 'signup_custom_classes',
			'type' => 'text',
			'desc' => '仕切り線にclass名を設定出来ます。CSSで調整したい場合などに活用してください。',
			'default' => __( '', 'easythemes'),
		),
	),
),


// 仕切り線
'divider' => array(
	'title' => __( '仕切り線 ブロック', 'easythemes'),
	'fields' => array(
		array(
			'name' => __( 'コンテンツとコンテンツの間に挟む仕切り線を挿入します。', 'easythemes'),
			'id' => 'divider-desc',
			'type' => 'description',
		),
		array(
			'name' => __( 'タイプ', 'easythemes'),
			'id' => 'divider_type',
			'type' => 'select',
			'desc' => '仕切り線のスタイルを選んでください。',
			'default' => 'solid',
			'options' => array(
				'solid' => __( '直線', 'easythemes'),
				'dotted' => __( 'ドット', 'easythemes'),
				'double' => __( 'ダブル', 'easythemes'),
				'dashed' => __( 'ダッシュ(２本線)', 'easythemes'),
				'groove' => __( '立体的', 'easythemes'),
				'trans' => __( '透明(隙間)', 'easythemes'),
			),
		),
		array(
			'name' => __( '仕切り線の幅', 'easythemes'),
			'desc' => '仕切り線の幅を指定してください。<br>
			デフォルト : 5px ',
			'id' => 'divider_height',
			'type' => 'select',
			'default' => '5px',
			'options' => array(
				'1px' => __( '1px', 'easythemes'),
				'2px' => __( '2px', 'easythemes'),
				'3px' => __( '3px', 'easythemes'),
				'5px' => __( '5px', 'easythemes'),
				'8px' => __( '8px', 'easythemes'),
				'12px' => __( '12px', 'easythemes'),
				'15px' => __( '15px', 'easythemes'),
				'18px' => __( '18px', 'easythemes'),
				'21px' => __( '21px', 'easythemes'),
				),
			),
		array(
			'name' => __( 'カラー', 'easythemes'),
			'id' => 'divider_color',
			'type' => 'color',
			'desc' => '仕切り線のカラーを選んでください。',
		),
		array(
			'name' => __( 'カスタムクラス', 'easythemes'),
			'id' => 'divider_custom_classes',
			'type' => 'text',
			'desc' => '仕切り線にclass名を設定出来ます。CSSで調整したい場合などに活用してください。',
			'default' => __( '', 'easythemes'),
		),
	),
),

//動画ブロック
'video_embed' => array(
	'title' => __( '動画 ブロック', 'easythemes'),
	'fields' => array(
		array(
			'name' => __( '各動画サービスの埋め込みコードを使用して動画を埋め込みます。', 'easythemes'),
			'id' => 'video_embed_desc',
			'type' => 'description',
		),
		array(
			'name' => __( '動画 : URL', 'easythemes'),
			'id' => 'video_embed_code',
			'type' => 'text',
			'desc' => '動画のURLを入力して下さい。',
		),
		array(
			'name' => __( '動画 : 幅', 'easythemes'),
			'id' => 'video_width',
			'type' => 'text',
			'desc' => 'プレーヤーの幅を入力してください。( 単位 : px )',
			'default' => '600',
		),
		array(
			'name' => __( '動画 : 高さ', 'easythemes'),
			'id' => 'video_height',
			'type' => 'text',
			'desc' => 'プレーヤーの高さを入力してください。( 単位 : px )',
			'default' => '400',
		),
		array(
			'name' => __( '動画 : 位置', 'easythemes'),
			'id' => 'video_align',
			'type' => 'radio',
			'desc' => '動画を整列させるオプションを選択してください。',
			'default' => 'center',
			'options' => array(
				'center' => 'センター寄せ',
				'left' => '左寄せ',
				'right' => '右寄せ',
			),
		),
		array(
			'name' => __( 'カスタムクラス', 'easythemes'),
			'id' => 'custom_classes',
			'type' => 'text',
			'desc' => '動画ブロックにclass名を設定出来ます。CSSで調整したい場合などに活用してください。',
			'default' => __( '', 'easythemes'),
		),
	),
),

//画像ブロック
'image_block' => array(
	'title' => __( '画像 ブロック', 'easythemes'),
	'fields' => array(
		array(
			'name' => __( '画像を挿入することができます。', 'easythemes'),
			'id' => 'image-desc',
			'type' => 'description',
			),
		array(
			'name' => __( '画像をアップロード', 'easythemes'),
			'id' => 'image_image_upload',
			'type' => 'file',
			'desc' => '画像をアップロードするか、メディアライブラリから選んでください。',
			),
		array(
			'name' => __( '画像リンク', 'easythemes'),
			'id' => 'image_link',
			'type' => 'text',
			'desc' => '画像のリンク先を指定することができます。<br>
			リンクさせない場合は空にしてください。',
			),
		array(
			'name' => __( 'カスタムクラス', 'easythemes'),
			'id' => 'custom_classes',
			'type' => 'text',
			'desc' => '画像にclass名を設定出来ます。CSSで調整したい場合などに活用してください。',
			'default' => __( '', 'easythemes'),
			),
		),
),

//コメント
'comments' => array(
	'title' => __( 'コメント ブロック', 'easythemes'),
	'fields' => array(
		array(
			'name' => __( 'コメント一覧とコメントフォームが表示されます。', 'easythemes'),
			'id' => 'comments-desc',
			'type' => 'description',
			),
	),
),

//ハーフブロック
'half_block' => array(
	'title' => __( 'ハーフ ブロック', 'easythemes'),
	'fields' => array(
		array(
			'name' => __( 'width', 'easythemes'),
			'id' => 'width',
			'type' => 'select',
			'default' => 'one-half',
			'class' => 'width',
			'options' => array(
				'one-half' => __( '1/2', 'easythemes'),
			),
		),
		array(
			'name' => __( '見出しとテキストを持つ半分の長さのブロックを挿入できます。', 'easythemes'),
			'id' => 'half-desc',
			'type' => 'description',
			),
		array(
			'name' => __( 'タイトル', 'easythemes'),
			'id' => 'half_title',
			'type' => 'text',
			'desc' => 'タイトルを入力できます。',
			),
			array(
				'name' => __( 'タイトルタグの種類', 'easythemes'),
				'desc' => 'title type',
				'id' => 'half_title_type',
				'type' => 'select',
				'desc' => 'タイトルを挟むタグを設定してください。',
				'default' => 'h3',
				'options' => array(
					'h1' => __( 'h1', 'easythemes'),
					'h2' => __( 'h2', 'easythemes'),
					'h3' => __( 'h3', 'easythemes'),
					'h4' => __( 'h4', 'easythemes'),
					'h5' => __( 'h5', 'easythemes'),
					'h6' => __( 'h6', 'easythemes'),
					),
				),
			array(
				'name' => __( 'タイトルリンク', 'easythemes'),
				'id' => 'half_link',
				'type' => 'text',
				'desc' => 'タイトルにリンクを埋め込無ことができます。',
				),
			array(
				'name' => __( '画像をアップロード', 'easythemes'),
				'id' => 'half_image_upload',
				'type' => 'file',
				'desc' => '画像をアップロードするか、メディアライブラリから選んでください。',
				),
			array(
				'name' => __( 'テキスト', 'easythemes'),
				'id' => 'half_text',
				'type' => 'textarea',
				'desc' => 'テキスト本文を入力してください。',
				),
			array(
				'name' => __( 'カスタムクラス', 'easythemes'),
				'id' => 'custom_classes',
				'type' => 'text',
				'desc' => 'ハーフボックスにclass名を設定出来ます。CSSで調整したい場合などに活用してください。',
				'default' => __( '', 'easythemes'),
				),
		),
),
//お問い合わせ
'contact' => array(
	'title' => __( 'お問い合わせフォーム ブロック', 'easythemes'),
	'fields' => array(

		array(
			'name' => __( '簡単にお問い合わせフォームを追加できます。', 'easythemes'),
			'id' => 'contactform-desc',
			'type' => 'description',
		),
		array(
			'name' => __( 'フォームのタイトル', 'easythemes'),
			'id' => 'contact_title',
			'type' => 'text',
			'desc' => 'ここに入力された内容はフォームの一番上にタイトルとして表示されます。',
		),
		array(
			'name' => __( 'フォームタイトルのタイプ', 'easythemes'),
			'id' => 'contact_title_type',
			'type' => 'select',
			'desc' => 'フォームタイトルのタグを選んでください。',
			'default' => 'h4',
			'options' => array(
				'h1' => __( 'h1', 'easythemes'),
				'h2' => __( 'h2', 'easythemes'),
				'h3' => __( 'h3', 'easythemes'),
				'h4' => __( 'h4', 'easythemes'),
				'h5' => __( 'h5', 'easythemes'),
				'h6' => __( 'h6', 'easythemes'),
			),
		),
		array(
			'name' => __( '送信ボタンテキスト', 'easythemes'),
			'id' => 'contact_submit_text',
			'type' => 'text',
			'desc' => '送信ボタンのテキストを入力して下さい。',
			'default' => '送信する'
		),
		array(
			'name' => __( '送信ボタンテキストカラー', 'easythemes'),
			'id' => 'contact_submit_text_color',
			'type' => 'color',
			'desc' => '送信ボタンのテキストカラーを選択して下さい。',
		),
		array(
			'name' => __( 'カスタムクラス', 'easythemes'),
			'id' => 'custom_classes',
			'type' => 'text',
			'desc' => 'お問い合せフォームにclass名を設定出来ます。CSSで調整したい場合などに活用してください。',
			'default' => __( '', 'easythemes'),
		),
	),
),
//オプトイン登録
'squeeze' => array(
	'title' => __( 'メルマガ登録フォーム ブロック', 'bb'),
	'fields' => array(
		array(
			'name' => __( '簡単にメルマガ登録フォームを追加できます。<br>
				※ 各ページにつき一度しか使用出来ません。', 'bb'),
			'id' => 'squeeze_desc',
			'type' => 'description',
		),
		array(
			'name' => __( 'タイトル', 'bb'),
			'id' => 'squeeze_title_text',
			'type' => 'text',
			'desc' => 'タイトルを入力してください。',
			'default' => __( '無料プレゼントの受け取りは下記のフォームから', 'bb'),
		),
		array(
			'name' => __( 'テキスト', 'bb'),
			'id' => 'squeeze_text',
			'type' => 'textarea',
			'desc' => 'フォーム上部に表示されるテキストを入力してください。',
			'default' => __( 'テキストを入力してください。', 'bb'),
		),
		array(
			'name' => __( 'カラータイプ', 'bb'),
			'id' => 'squeeze_design_type',
			'type' => 'color',
			'default' => __( '#002d5e', 'bb'),
			'desc' => 'フォームのカラーを選択してください。',
		),
		array(
			'name' => __( 'フォームタイプ', 'bb'),
			'id' => 'squeeze_optin_type',
			'type' => 'select',
			'desc' => '入力後のデータの送信先を指定してください。',
			'options' => array(
				'0' => __( '楽メールPro', 'bb'),
				'1' => __( 'ネット商人Pro', 'bb'),
				'2' => __( 'メール商人', 'bb'),
				'3' => __( 'Jcity', 'bb'),
				'4' => __( 'アスメル', 'bb'),
				'5' => __( 'オートビズ', 'bb'),
				'6' => __( 'その他1（name属性がemail、nameになってるフォーム）', 'bb'),
				'7' => __( 'その他1（name属性がemail、nameになってるフォーム）', 'bb'),
				'8' => __( '1ShoppingCart', 'bb'),
				'9' => __( 'aWeber', 'bb'),
				'10' => __( 'AutoResponsePlus', 'bb'),
				'11' => __( 'Email Aces', 'bb'),
				'12' => __( 'FreeAutobot', 'bb'),
				'13' => __( 'GetResponse', 'bb'),
				'14' => __( 'iContact', 'bb'),
				'15' => __( 'InfusionSoft', 'bb'),
				'23' => __( 'InfusionSoft(Old)', 'bb'),
				'16' => __( 'ListPing', 'bb'),
				'17' => __( 'MailChimp', 'bb'),
				'18' => __( 'ParaBots', 'bb'),
				'19' => __( 'ProSender', 'bb'),
				'20' => __( 'QuickPayPro', 'bb'),
				'21' => __( 'Turbo Autoresponders', 'bb'),
				'22' => __( 'GVO', 'bb'),
			),
		),
		array(
			'name' => __( 'HTMLフォーム', 'bb'),
			'id' => 'squeeze_optin_code',
			'type' => 'textarea',
			'desc' => '利用するメールサービスのフォームを入力してください。',
		),
		array(
			'name' => __( 'ボタンのテキスト', 'bb'),
			'id' => 'squeeze_button_text',
			'type' => 'text',
			'desc' => 'ボタンのテキストを入力してください。',
			'default' => __( '今すぐ応募する', 'bb'),
		),
		array(
			'name' => __( 'ボタンのフォントカラー', 'bb'),
			'id' => 'squeeze_button_text_color',
			'type' => 'color',
			'desc' => 'ボタンのフォントカラーを選択して下さい。',
			'default' => __( '#FFFFFF', 'bb'),
		),
		array(
			'name' => __( '背景画像をアップロード', 'bb'),
			'id' => 'squeeze_image_upload',
			'type' => 'file',
			'desc' => '背景画像を指定することもできます。',
		),
		array(
			'name' => __( 'カスタムクラス', 'bb'),
			'id' => 'custom_classes',
			'type' => 'text',
			'desc' => 'オプトインフォームにclass名を設定出来ます。CSSで調整したい場合などに活用してください。',
			'default' => __( '', 'bb'),
		),
	),
),

// 定型文
'template' => array(
	'title' => __( '定型文 ブロック', 'easythemes'),
	'fields' => array(
		array(
			'name' => __( '定型文を選択し、挿入することができます。', 'easythemes'),
			'id' => 'template-desc',
			'type' => 'description',
		),
		array(
			'name' => __( '定型文の選択', 'easythemes'),
			'id' => 'template_type',
			'type' => 'select',
			'desc' => '定型文を選択してください',
			'default' => 'one',
			'options' => array(
				'one' => $one,
				'two' => $two,
				'three' => $three,
				'four' => $four,
				'five' => $five,
				'six' => $six,
				'seven' => $seven,
				'eight' => $eight,
				'nine' => $nine,
				'ten' => $ten,
			),
		),
		array(
			'name' => __( 'テキストカラー', 'easythemes'),
			'id' => 'template_text_color',
			'type' => 'color',
			'desc' => '定型文のテキストカラーを選択して下さい。',
			'default' => __( '#0000FF', 'easythemes'),

		),
		array(
			'name' => __( '背景色', 'easythemes'),
			'id' => 'template_bg_color',
			'type' => 'color',
			'desc' => '定型文の背景色カラーを選択して下さい。',
			'default' => __( '#FFFF00', 'easythemes'),

		),
		array(
			'name' => __( 'カスタムクラス', 'easythemes'),
			'id' => 'custom_classes',
			'type' => 'text',
			'desc' => '定型文にclass名を設定出来ます。CSSで調整したい場合などに活用してください。',
			'default' => __( '', 'easythemes'),
		),
	),
),
//定型文
'choose_img' => array(
	'title' => __( '画像選択 ブロック', 'easythemes'),
	'fields' => array(
		array(
			'name' => __( '画像を選択し、挿入することができます。', 'easythemes'),
			'id' => 'choose_img-desc',
			'type' => 'description',
		),
		array(
			'name' => __( '挿入する画像の選択', 'easythemes'),
			'id' => 'choose_img_type',
			'type' => 'radio_images',
			'desc' => '画像を選択してください',
			'default' => 'crown-img-01.png',
			'options' => array(
				'crown-img-01.png' => $choose_img['one'],
				'crown-img-02.png' => $choose_img['two'],
				'crown-img-03.png' => $choose_img['three'],
				'crown-img-04.png' => $choose_img['four'],
				'crown-img-05.png' => $choose_img['five'],
				'badge-img-01.png' => $choose_img['six'],
				'badge-img-02.png' => $choose_img['seven'],
			),
		),
		array(
			'name' => __( '画像の位置', 'easythemes'),
			'id' => 'choose_img_align',
			'type' => 'select',
			'desc' => '挿入する画像の位置を指定して下さい。',
			'default' => __( '#0000FF', 'easythemes'),
			'options' => array(
				'right' => '左 (テキスト右)',
				'left' => '右 (テキスト左)',
				'center' => '上中央 (テキスト下)',
			),
		),
		array(
			'name' => __( '文章', 'easythemes'),
			'id' => 'choose_img_text',
			'type' => 'textarea',
			'desc' => '文章を入力して下さい。'
		),
		array(
			'name' => __( 'カスタムクラス', 'easythemes'),
			'id' => 'choose_img_classes',
			'type' => 'text',
			'desc' => '定型文にclass名を設定出来ます。CSSで調整したい場合などに活用してください。',
			'default' => __( '', 'easythemes'),
		),
	),
),
);
