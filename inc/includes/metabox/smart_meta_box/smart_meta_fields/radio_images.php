<?php
/**
 * radio image field
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
foreach ( $options as $opt_value => $opt_name ) : ?>
	<label class="radio-label radio_image <?php echo $id?>_<?php echo $opt_value ?>" <?php if($type == 'choose_img') { ?> style="background:url(<?php echo $opt_name; };?>) no-repeat; background-size: 100% auto;" >
		<input type="radio" name="<?php echo $id?>" id="<?php echo $id?>_<?php echo $opt_value ?>" value="<?php echo $opt_value?>" <?php checked($value, $opt_value)?> />
	</label>
<?php
endforeach;
