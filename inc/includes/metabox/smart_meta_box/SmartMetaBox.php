<?php
/**
 * Meta box generator for WordPress
 * Compatible with custom post types
 *
 * Support input types: text, textarea, checkbox, select, radio
 *
 * @author: Nikolay Yordanov <me@nyordanov.com> http://nyordanov.com
 * @version: 1.0
 *
 */

require_once 'smart_meta_fields/layout-editor/helpers/init.php';

if ( ! function_exists( 'add_smart_meta_box' ) ) {

	class SmartMetaBox {
		protected $meta_box;
		protected $id;
		static $prefix = '';

		/**
		 * __construct
		 * @param [type] $id   [description]
		 * @param [type] $opts [description]
		 */
		public function __construct( $id, $opts )
		{

			if ( ! is_admin() ){
				return;
			}

			$this->meta_box = $opts;
			$this->id = $id;
			add_action( 'add_meta_boxes', array( &$this, 'add' ) );
			add_action( 'save_post', array( &$this, 'save' ) );
		}

		/**
		 * Add method
		 */
		public function add()
		{
			foreach ( $this->meta_box['pages'] as $page ) {
				add_meta_box( $this->id, $this->meta_box['title'],
					array(
						&$this,
						'show'
					),
					$page, $this->meta_box['context'], $this->meta_box['priority'] );
			}
		}

		/**
		 * Show Method
		 * @param  [type] $post [description]
		 * @return [type]       [description]
		 */
		public function show( $post )
		{
			echo '<input type="hidden" name="' . $this->id . '_meta_box_nonce" value="', wp_create_nonce( 'smartmetabox' . $this->id ) , '" />';
			echo '<div class="form-table">';

			foreach ( $this->meta_box['fields'] as $field ) {

				extract( $field );

				if ( isset( $field['id'] ) ) {
					$id = self::$prefix . $id;
					$value = self::get($field['id']);
					if ( empty( $value ) && ! sizeof( self::get( $field['id'], false ) ) ) {
						$value = isset( $field['default'] ) ? $default : '';
					}
				}

				echo '<div class="smart-meta-field-wrp">';

					if ( isset( $field['name'] ) ) {
						echo '<h4><label for="'.$id.'">'.$name.'</label></h4>';
					}

					echo '<div class="smart-meta-field">';
						include "smart_meta_fields/$type.php";

						if ( isset( $field['desc'] ) ) {
							echo '&nbsp;<span class="description">' . $desc . '</span>';
						}
					echo '</div>';

				echo '</div>';
				echo '</td></tr>';
			}
			echo '</div>';
		}

		/**
		 * Save Data
		 * @param  [type] $post_id [description]
		 * @return [type]          [description]
		 */
		public function save( $post_id )
		{

			// verify nonce
			if ( ! isset( $_POST[$this->id . '_meta_box_nonce'] )
					|| ! wp_verify_nonce( $_POST[$this->id . '_meta_box_nonce'], 'smartmetabox' . $this->id ) ) {
				return $post_id;
			}

			// check autosave
			if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
				return $post_id;
			}

			// check permissions
			if ( 'post' == $_POST['post_type'] ) {
				if ( ! current_user_can( 'edit_post', $post_id ) ) {
					return $post_id;
				}
			} elseif ( ! current_user_can( 'edit_page', $post_id ) ) {
				return $post_id;
			}

			foreach ( $this->meta_box['fields'] as $field ) {
				if ( isset( $field['id'] ) ) {
					$name = self::$prefix . $field['id'];
					if ( isset( $_POST[$name] )
							 || isset( $_FILES[$name] ) ) {
						$old = self::get( $field['id'], true, $post_id );
						$new = $_POST[$name];
						if ( $new != $old ) {
							self::set( $field['id'], $new, $post_id );
						}
					} elseif ( $field['type'] == 'checkbox' ) {
						self::set( $field['id'], 'false', $post_id );
					} else {
						self::delete( $field['id'], $name );
					}
				}
			}
		}

		/**
		 * Get
		 * @param  [type]  $name    [description]
		 * @param  boolean $single  [description]
		 * @param  [type]  $post_id [description]
		 * @return [type]           [description]
		 */
		static function get( $name, $single = true, $post_id = null ) {
			global $post;
			return get_post_meta( isset( $post_id ) ? $post_id : $post->ID, self::$prefix . $name, $single );
		}

		/**
		 * Set
		 * @param [type] $name    [description]
		 * @param [type] $new     [description]
		 * @param [type] $post_id [description]
		 */
		static function set($name, $new, $post_id = null) {
			global $post;
			return update_post_meta(isset($post_id) ? $post_id : $post->ID, self::$prefix . $name, $new);
		}

		/**
		 * delete
		 * @param  [type] $name    [description]
		 * @param  [type] $post_id [description]
		 * @return [type]          [description]
		 */
		static function delete($name, $post_id = null) {
			global $post;
			return delete_post_meta(isset($post_id) ? $post_id : $post->ID, self::$prefix . $name);
		}

		/**
		 * parse
		 * @param  [type] $terms [description]
		 * @return [type]        [description]
		 */
		static function parse_terms($terms) {
			$res = array();
			foreach($terms as $t) {
				$res[$t->term_id] = $t->name;
			}
			return $res;
		}

	};

	function add_smart_meta_box( $id, $opts ) {
		new SmartMetaBox( $id, $opts );
	}

}
