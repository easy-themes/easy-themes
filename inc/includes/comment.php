<?php
/**
 * Repeater Field - Comment
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
?>

<!-- コメント -->
<?php comments_template( '/modules/comments.php' );?>
<!-- /コメント -->
