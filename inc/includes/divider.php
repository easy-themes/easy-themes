<?php
/**
 * Repeater Field - Divider Module
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
?>

<div class="clearfix"></div>
<div class="row-fluid divider">
<?php
if ( $divider_type ){ ?>
	<div class="<?php echo esc_attr( $divider_type );?>" style="border-width: <?php echo esc_attr( $divider_height );?>;border-color:<?php echo esc_attr( $divider_color );?>"></div>
<?php
}?>
</div>
<div class="clearfix"></div>
