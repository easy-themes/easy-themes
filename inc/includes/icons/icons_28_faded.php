			<div class="social">

<?php
/* Icons */
$icon_location = $data['icon_location']; //also defined in footer.php
$icon_bookmark = $data['icon_bookmark'];
$icon_creativecommons = $data['icon_creativecommons'];
$icon_delicious = $data['icon_delicious'];
$icon_digg = $data['icon_digg'];
$icon_ebay = $data['icon_ebay'];
$icon_email = $data['icon_email'];
$icon_facebook = $data['icon_facebook'];
$icon_feedburner = $data['icon_feedburner'];
$icon_flickr = $data['icon_flickr'];
$icon_forrst = $data['icon_forrst'];
$icon_github = $data['icon_github'];
$icon_google = $data['icon_google'];
$icon_gtalk = $data['icon_gtalk'];
$icon_lastfm = $data['icon_lastfm'];
$icon_linkedin = $data['icon_linkedin'];
$icon_msn = $data['icon_msn'];
$icon_myspace = $data['icon_myspace'];
$icon_pandora = $data['icon_pandora'];
$icon_paypal = $data['icon_paypal'];
$icon_reddit = $data['icon_reddit'];
$icon_rss = $data['icon_rss'];
$icon_skype = $data['icon_skype'];
$icon_squidoo = $data['icon_squidoo'];
$icon_technorati = $data['icon_technorati'];
$icon_tumblr = $data['icon_tumblr'];
$icon_twitter = $data['icon_twitter'];
$icon_vimeo = $data['icon_vimeo'];
$icon_yahoo = $data['icon_yahoo'];
$icon_youtube = $data['icon_youtube'];
?>
<?php if($icon_youtube != NULL){ ?><a class="fade" href="<?php echo $icon_youtube; ?>" title="youtube"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/youtube_28.png" alt="youtube" /></a><?php } ?>
<?php if($icon_yahoo != NULL){ ?><a class="fade" href="<?php echo $icon_yahoo; ?>" title="yahoo"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/yahoo_28.png" alt="yahoo" /></a><?php } ?>
<?php if($icon_vimeo != NULL){ ?><a class="fade" href="<?php echo $icon_vimeo; ?>" title="vimeo"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/vimeo_28.png" alt="vimeo" /></a><?php } ?>
<?php if($icon_twitter != NULL){ ?><a class="fade" href="<?php echo $icon_twitter; ?>" title="twitter"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/twitter_28.png" alt="twitter" /></a><?php } ?>
<?php if($icon_tumblr != NULL){ ?><a class="fade" href="<?php echo $icon_tumblr; ?>" title="tumblr"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/tumblr_28.png" alt="tumblr" /></a><?php } ?>
<?php if($icon_technorati != NULL){ ?><a class="fade" href="<?php echo $icon_technorati; ?>" title="technorati"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/technorati_28.png" alt="technorati" /></a><?php } ?>
<?php if($icon_squidoo != NULL){ ?><a class="fade" href="<?php echo $icon_squidoo; ?>" title="squidoo"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/squidoo_28.png" alt="squidoo" /></a><?php } ?>
<?php if($icon_skype != NULL){ ?><a class="fade" href="<?php echo $icon_skype; ?>" title="skype"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/skype_28.png" alt="skype" /></a><?php } ?>
<?php if($icon_rss != NULL){ ?><a class="fade" href="<?php echo $icon_rss; ?>" title="rss"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/rss_28.png" alt="rss" /></a><?php } ?>
<?php if($icon_reddit != NULL){ ?><a class="fade" href="<?php echo $icon_reddit; ?>" title="reddit"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/reddit_28.png" alt="reddit" /></a><?php } ?>
<?php if($icon_paypal != NULL){ ?><a class="fade" href="<?php echo $icon_paypal; ?>" title="paypal"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/paypal_28.png" alt="paypal" /></a><?php } ?>
<?php if($icon_pandora != NULL){ ?><a class="fade" href="<?php echo $icon_pandora; ?>" title="pandora"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/pandora_28.png" alt="pandora" /></a><?php } ?>
<?php if($icon_myspace != NULL){ ?><a class="fade" href="<?php echo $icon_myspace; ?>" title="myspace"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/myspace_28.png" alt="myspace" /></a><?php } ?>
<?php if($icon_msn != NULL){ ?><a class="fade" href="<?php echo $icon_msn; ?>" title="msn"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/msn_28.png" alt="msn" /></a><?php } ?>
<?php if($icon_linkedin != NULL){ ?><a class="fade" href="<?php echo $icon_linkedin; ?>" title="linkedin"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/linkedin_28.png" alt="linkedin" /></a><?php } ?>
<?php if($icon_lastfm != NULL){ ?><a class="fade" href="<?php echo $icon_lastfm; ?>" title="lastfm"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/lastfm_28.png" alt="lastfm" /></a><?php } ?>
<?php if($icon_gtalk != NULL){ ?><a class="fade" href="<?php echo $icon_gtalk; ?>" title="gtalk"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/gtalk_28.png" alt="gtalk" /></a><?php } ?>
<?php if($icon_google != NULL){ ?><a class="fade" href="<?php echo $icon_google; ?>" title="google"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/google_28.png" alt="google" /></a><?php } ?>
<?php if($icon_github != NULL){ ?><a class="fade" href="<?php echo $icon_github; ?>" title="github"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/github_28.png" alt="github" /></a><?php } ?>
<?php if($icon_forrst != NULL){ ?><a class="fade" href="<?php echo $icon_forrst; ?>" title="forrst"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/forrst_28.png" alt="forrst" /></a><?php } ?>
<?php if($icon_flickr != NULL){ ?><a class="fade" href="<?php echo $icon_flickr; ?>" title="flickr"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/flickr_28.png" alt="flickr" /></a><?php } ?>
<?php if($icon_feedburner != NULL){ ?><a class="fade" href="<?php echo $icon_feedburner; ?>" title="feedburner"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/feedburner_28.png" alt="feedburner" /></a><?php } ?>
<?php if($icon_facebook != NULL){ ?><a class="fade" href="<?php echo $icon_facebook; ?>" title="facebook"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/facebook_28.png" alt="facebook" /></a><?php } ?>
<?php if($icon_email != NULL){ ?><a class="fade" href="<?php echo $icon_email; ?>" title="email"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/email_28.png" alt="email" /></a><?php } ?>		
<?php if($icon_ebay != NULL){ ?><a class="fade" href="<?php echo $icon_ebay; ?>" title="ebay"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/ebay_28.png" alt="ebay" /></a><?php } ?>
<?php if($icon_digg != NULL){ ?><a class="fade" href="<?php echo $icon_digg; ?>" title="digg"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/digg_28.png" alt="digg" /></a><?php } ?>
<?php if($icon_deviantart != NULL){ ?><a class="fade" href="<?php echo $icon_deviantart; ?>" title="deviantart"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/deviantart_28.png" alt="deviantart" /></a><?php } ?>
<?php if($icon_delicious != NULL){ ?><a class="fade" href="<?php echo $icon_delicious; ?>" title="delicious"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/delicious_28.png" alt="delicious" /></a><?php } ?>
<?php if($icon_creativecommons != NULL){ ?><a class="fade" href="<?php echo $icon_creativecommons; ?>" title="creativecommons"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/creativecommons_28.png" alt="creativecommons" /></a><?php } ?>
<?php if($icon_bookmark != NULL){ ?><a class="fade" href="<?php echo $icon_bookmark; ?>" title="bookmark"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/28_black/bookmark_28.png" alt="bookmark" /></a><?php } ?>


			</div><!-- end social icons -->