<!-- ハーフブロック -->
<div class="half-block span12 clearfix <?php echo ' '.$custom_class; ?>">
		<<?php echo $title_type; ?> class="widget--title">
		 <?php if($half_link !== ""){?><a href="<?php echo $half_link;?>"><?php };?>
		 <?php echo $title;?>
		 <?php if($half_link !== ""){?></a><?php };?>
		 </<?php echo $title_type; ?>>
		<?php if( $half_image_upload ){ ?>
		<div class="thumbnails">
			<img src="<?php echo $half_image_upload;?>" alt=""/>
		</div>
		 <?php } ?>
		<p><?php echo $half_text;?></p>
<!-- /ハーフブロック -->
</div>
