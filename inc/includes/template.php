<!-- 定型文 -->
<div class="theme_radius row-fluid<?php echo ' '. $custom_class ; ?>template-text">
    <div class="row-fluid" style="background: <?php echo esc_attr( $template_bg_color ); ?>;color:<?php echo esc_attr( $template_text_color ); ?>; ">
    <?php
    echo easy_themes_setting( 'template_text_' . $template_type, false, false ); ?>
    </div>
<!-- /定型文 -->
</div>
