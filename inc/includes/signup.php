<?php
/**
 * Repeater Field - Sign Up
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
?>
<div class="row-fluid signup">
  <div class="span8">
  <?php if ( $signup_image_upload ) { ?>
    <img src="<?php echo $signup_image_upload;?>" alt="商品画像" />
  <?php } else { ?>
    <img src="<?php bloginfo('template_url');?>/images/package-img.jpg" alt="準備中です。">
  <?php }?>
  </div>
  <div class="span16">
    <?php if( $signup_text ) {?>
      <p><?php echo $signup_text;?></p>
    <?php } else { ?>
      <p>ここに説明テキストが入ります。</p>
    <?php } ?>

    <?php if( $signup_button_text ) {?>
      <p class="text-center"><a href="<?php echo isset( $signup_button_url ) ? esc_url( $signup_button_url ) : '';?>" style="
        background: <?php echo isset( $signup_button_color_top ) ? $signup_button_color_top : '' ;?>;
        background: -moz-linear-gradient(top, <?php echo isset( $signup_button_color_top ) ? $signup_button_color_top : '' ;?> 0%, <?php echo $signup_button_color_bottom;?> 100%);
        background: -webkit-gradient(left top, left bottom, color-stop(0%, <?php echo isset( $signup_button_color_top ) ? $signup_button_color_top : '' ;?>), color-stop(47%, <?php echo isset( $signup_button_color_top ) ? $signup_button_color_top : '' ;?>), color-stop(100%, <?php echo isset( $signup_button_color_bottom ) ? $signup_button_color_bottom : '' ;?> ));
        background: -webkit-linear-gradient(top, <?php echo isset( $signup_button_color_top ) ? $signup_button_color_top : '' ;?> 0%,  <?php echo $signup_button_color_bottom;?> 100%);
        background: -o-linear-gradient(top, <?php echo isset( $signup_button_color_top ) ? $signup_button_color_top : '' ;?> 0%, <?php echo $signup_button_color_bottom;?> 100%);
        background: -ms-linear-gradient(top, <?php echo isset( $signup_button_color_top ) ? $signup_button_color_top : '' ;?>, <?php echo $signup_button_color_bottom;?>  100%);" target="_blank">
        <?php echo $signup_button_text;?></a></p>

      <?php } else { ?>

      <p>ここに説明テキストが入ります。</p>

      <?php } ?>
  </div>


<!-- /お申込み -->
</div>
