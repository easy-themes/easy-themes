<div class="clear-fix"></div>
<!-- スクイーズページ -->
<div class="content">
  <div class="main-content">
    <div class="optin <?php echo $custom_classes;?>" id="opt">
      <div class="row-fluid" style="<?php if( $squeeze_image_upload ){ echo 'background: url('.$squeeze_image_upload.');'; }; ?>">
      <h3 class="squeeze-title" style="background: <?php echo $squeeze_design_type;?>"><?php echo $squeeze_title_text; ?></h3>
        <?php
          $optin_form_parsed = squeeze_parse_optin_form($optin_code);
          $optin_form_action = $optin_form_parsed['action'];
          unset($optin_form_parsed['action']);
          $optin_form_hidden_fields = $optin_form_parsed;
          $optin_form_name_emails_fields = squeeze_name_email_fields($optin_type);
          $optin_form_name = $optin_form_name_emails_fields['name'];
          $optin_form_email = $optin_form_name_emails_fields['email'];
        ?>
        <div style="color:#000;margin-bottom:10px;text-shadow:1px 1px #fff; border-color: <?php echo $squeeze_design_type;?>;" class="bold center font-16 squeeze-box">
        <p class="text-center">
          <strong><?php echo $squeeze_text; ?></strong>
        </p>
          <form action="<?php echo $optin_form_action?> " method="post">
            <?php
                  foreach ($optin_form_hidden_fields as $key =>
            $value) {
                    echo "
            <input type=\"hidden\" name=\"$key\" value=\"$value\" />
            ";
                  }
                  ?>
            <label for="<?php echo $optin_form_name?>" class="text form-icon">氏名 <input class="text" type="text" name="<?php echo $optin_form_name?>" id="name" value="お名前を入力して下さい" onclick="if(this.value=='お名前を入力して下さい')this.value='';" onblur="if(this.value=='')this.value='お名前を入力して下さい';"></label>
            <label for="<?php echo $optin_form_name?>" class="text form-icon">メールアドレス <input class="text" type="text" name="<?php echo $optin_form_email?>" id="email" value="メールアドレスを入力して下さい" onclick="if(this.value=='メールアドレスを入力して下さい')this.value='';" onblur="if(this.value=='')this.value='メールアドレスを入力して下さい';"></label>
            <p class="text-center">
            <input class="button btn" type="submit" alt="Submit Form" value=" <?php echo $squeeze_button_text;?>" style="background:<?php echo $squeeze_design_type;?>;color:<?php echo $squeeze_button_text_color;?>">
            </p>
          </form>
      </div>
    </div>
  </div>
</div>
</div>
<!-- スクイーズページ -->

<div class="clear-fix"></div>
