<?php
/**
 * Repeater Field - Blog Post
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
?>
<!-- 投稿記事ブロック -->

<div class="row-fluid">

<?php
if ( ! is_singular() ){
	echo '<div class="contentblock">';
}
$temp = $blog_post_query;
$blog_post_query = null;
$blog_post_query = new WP_Query(
	array(
		'category__in' => $blog_cat,
		'showposts' => $number_of_posts,
	)
);


$count = 0;
while ( $blog_post_query->have_posts() ) :
	$blog_post_query->the_post();

	$fullsize_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'fullsize-thumbnail' );
	$fullsize_thumbnail = $fullsize_thumb['0'];
	$external_link = SmartMetaBox::get( 'external_link' );

	// sets thumbnail according to how many posts there are per row
	switch ( $posts_per_row ) {
		case '1':
			$blog_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'whole-thumbnail' );
			$blog_thumbnail = $blog_thumb['0'];
			break;
		case '2':
			$blog_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'half-thumbnail' );
			$blog_thumbnail = $blog_thumb['0'];
			break;
		case '3':
			$blog_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'third-thumbnail' );
			$blog_thumbnail = $blog_thumb['0'];
			break;
		case '4':
			$blog_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'fourth-thumbnail' );
			$blog_thumbnail = $blog_thumb['0'];
			break;
		case '5':
			$blog_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'fourth-thumbnail' );
			$blog_thumbnail = $blog_thumb['0'];
			break;
		case '6':
			$blog_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'fourth-thumbnail' );
			$blog_thumbnail = $blog_thumb['0'];
			break;
		case '7':
			$blog_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'fourth-thumbnail' );
			$blog_thumbnail = $blog_thumb['0'];
			break;
		case '8':
			$blog_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'fourth-thumbnail' );
			$blog_thumbnail = $blog_thumb['0'];
			break;
		default:
			$blog_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'third-thumbnail' );
			$blog_thumbnail = $blog_thumb['0'];
			break;
	}

	$display_attributes = null;
	if ( $show_image == 'yes' ){
		if ( $title_location == 'above image' ){
			$display_attributes = ' title_above_image';
		}
		if ( $title_location == 'below image'
				&& $show_bar == 'yes' ){
			$display_attributes = ' image_and_bar';
		}
	}
	$count++;
	if ( $count > $posts_per_row ) {
		$count = 1;
	} ?>

	<div class="<?php echo esc_attr( $box_size ); ?><?php echo esc_attr( $display_attributes ); ?> margin<?php if ( $count == $posts_per_row ) { echo " last"; } ?> <?php if ( $custom_classes ){ echo esc_attr( $custom_classes );} ?>" id="post-<?php the_ID(); echo esc_attr( '-' . rand( 1,999 ) ); ?>">

<?php
	if ( $title_location == 'above image' ) {
		echo esc_attr( $b_header_type );
		if ( $image_location == 'left' ) {
			echo "class='half_image_left'";
		} elseif ( $image_location == 'right' ) {
			echo "class='half_image_right'";
		} ?>>
		<a href="<?php
		if ( $external_link ) {
			echo esc_url( $external_link );
		} else {
			the_permalink();
		} ?>" title="<?php the_title_attribute(); ?>">
		<?php
		if ( get_post_meta( $post->ID, 'alternate_title_value', true ) ) {
			echo get_post_meta( $post->ID, 'alternate_title_value', true );
		} else {
			the_title();
		} ?></a></<?php echo $b_header_type; ?>>
		<?php
		if ( $image_location == 'left'
				 || $image_location == 'right' ) {
			echo '<div class="clear"></div>';
		}
	}
	if ( $show_image !== 'no' ) {
		if ( ! SmartMetaBox::get( 'video_embed' ) ) {
			if ( $blog_thumbnail ) { ?>
				<div class="mag <?php
				if ( $image_location == 'left' ) {
					echo "half_image_left";
				} elseif ( $image_location == 'right' ) {
					echo "half_image_right";
				} ?>">
				<?php
				if ( SmartMetaBox::get( 'fullsize' ) == 'yes' ){ ?>
					<a href="<?php echo $fullsize_thumbnail; ?>" data-rel="prettyphoto" title="<?php the_title_attribute(); ?>"><?php } else { ?><a class="noimage" href="<?php if($external_link != NULL) {echo $external_link;} else{the_permalink();} ?>" title="<?php the_title_attribute(); ?>"><?php } ?>
					<img src="<?php echo $blog_thumbnail; ?>" alt="<?php the_title(); ?>" /></a>
				</div>
				<?php
				}
			}
		}
	if ( $title_location !== 'above image' ) { ?>
		<<?php echo esc_attr( $b_header_type ); ?> class="<?php
		if ( $image_location == 'left' ) {
			echo 'half_image_left';
		} elseif ( $image_location == 'right' ) {
			echo 'half_image_right';
		}
		if ( $meta == 'yes' ) {
			echo ' meta-title';
		} ?>">
			<a href="<?php
		if ( $external_link ) {
			echo esc_url( $external_link );
		} else {
			the_permalink();
		} ?>" title="<?php the_title_attribute(); ?>"><?php
		if ( get_post_meta( $post->ID, 'alternate_title_value', true ) ) {
			echo get_post_meta( $post->ID, 'alternate_title_value', true );
		} else {
			the_title();
		} ?></a></<?php echo esc_attr( $b_header_type ); ?>>
			<?php
		if ( $image_location == 'left'
				 || $image_location == 'right' ) {
			echo '<div class="clear"></div>';
		}
	}
	if ( $show_bar == 'yes' ){ ?>
		<div class="show_bar"></div>
		<?php
	}
	global $more;
	$more = false;
	if ( $show_text !== 'no' ) {
		if ( $read_more == 'no' ) {
			if ( $meta == 'yes' ) { ?>
				<p class="meta-editor"><?php the_time( 'M jS, Y' ); ?> in <?php the_category(', ') ?></p><?php
			}
			if ( ! empty( $post->post_excerpt ) ) {
				the_excerpt();
			} else {
				the_content( '' );
			}
		} else {
			if ( $meta == 'yes' ) { ?>
				<p class="meta-editor"><?php the_time( 'M jS, Y' ); ?> in <?php the_category(', ') ?></p><?php
			}
			if ( ! empty( $post->post_excerpt ) ) {
				the_excerpt(); ?>
				<a style="margin-top:5px;" class="more-link" href="<?php if ( $external_link ) {
					echo $external_link;
			} else {
				the_permalink();
			} ?>"><?php echo $read_more_text; ?></a><?php
		} else {
			if ( $external_link ) {
				the_content(); ?><a style="margin-top:5px;" class="more-link" href="<?php echo $external_link; ?>"><?php echo $read_more_text; ?></a><?php
		} else{
			the_content($read_more_text);
		}
	}
}
}
$more = true; ?>

	</div>
	<?php
if ( $count == $posts_per_row ) {
	echo '<div class="clear"></div>';
}
endwhile;
$blog_post_query = null;
$blog_post_query = $temp;
if ( ! is_singular() ){
	echo '</div>';
}
	wp_reset_query(); ?>
<!-- ブログ -->
</div>
<div class="clear-fix"></div>
