<?php
/**
 * Repeater Field - WordPress
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */

global $post,$easy_themes;

if ( is_page() ) { ?>

	<div class="row-fluid">
	<?php
			if ( $sidebar_layout == 'no_sidebar' ){
				$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'blog-fullwidth' );
			} else {
				$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'blog-large' );}
				$post_thumbnail = $post_thumb['0'];
				if ( $post_thumbnail !== NULL ) { ?>
				<div>
					<img class="mb0" src="<?php echo $post_thumbnail; ?>" alt="<?php the_title(); ?>" />
				</div>
				<div class="box_full mt15">
				<?php
				} else { ?>
			<div class="box_full mt5">
			<?php
				} ?>
				<?php
				$wordpress_exists = 1; ?>

				<?php
				the_content(); ?>

			</div>

	</div>

<?php
}

/**
 * 投稿
 */
if ( is_single() ){ ?>
	<div class="row-fluid">
		<div class="box_full mt30">
		<?php
		$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID) , 'blog-large' );
		$post_thumbnail = $post_thumb['0']; ?>
			<div class="post_wrap">
				<p class="meta">
				<?php
				$cat = get_the_category();
				$cat = isset( $cat[0] ) ? $cat[0] : '';
				if ( $cat ) {
					echo '<span class="btn"> <i class="icon-tags"></i>'. esc_attr( $cat->cat_name ) . '</span>';
				} ?>
					<span class="btn">
						<i class="icon-calendar"></i>
						<?php echo get_the_time( 'Y年Mj日 ') ?>
					</span>
					<span class="btn">
						<a href="<?php the_permalink(); ?>#comments" title="comments">
							<i class="icon-comment"></i>
							<?php comments_number( 'コメント無し','1コメント','% コメント' ); ?>
						</a>
					</span>
				</p>
			<?php
			the_content(); ?>
			</div>
		</div>
	</div>
<?php
}
