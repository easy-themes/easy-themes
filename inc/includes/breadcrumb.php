<?php
/**
 * Repeater Field - BreadCrumbs
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
?>
<!-- パンくず -->

<div class="row-fluid bread-crumb">

  <?php
  if ( function_exists( 'bcn_display' ) ) { bcn_display(); };?>
<!-- / パンくず -->
</div>
