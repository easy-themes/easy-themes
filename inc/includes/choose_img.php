<?php
/**
 * Repeater Field - Choose image
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
?>
<div class="clearfix"></div>
<!-- 画像選択 -->
<div class="row-fluid <?php echo esc_attr( $choose_img_classes ); ?> choose-img">
<?php
if ( $choose_img_align == 'right' ) : ?>
  <div class="span8" style="text-align:center">
    <img src="<?php echo get_template_directory_uri() .'/assets/images/' . $choose_img_type; ?>" alt="" />
  </div>
  <div class="span16">
    <p><?php echo $choose_img_text; ?></p>
  </div>

	<?php
elseif ( $choose_img_align == 'left' ) : ?>
  <div class="span16">
    <p><?php echo $choose_img_text; ?></p>
  </div>
  <div class="span8" style="text-align:center">
    <img src="<?php echo  get_template_directory_uri() . '/assets/images/' . $choose_img_type; ?>" alt="" />
  </div>
	<?php

elseif( $choose_img_align == 'center' ) : ?>
  <p style="text-align:center"><img src="<?php echo get_template_directory_uri() . '/assets/images/' . $choose_img_type; ?>" alt="" /></p>
  <p><?php echo $choose_img_text; ?></p>
<?php endif; ?>
<!-- /画像選択 -->
</div>
<div class="clearfix"></div>
