<?php
/**
 * Repeater Field - Headdings
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
?>
<div class="clearfix"></div>
  <div class="headline theme_radius <?php
if ( $text_align == 'left' ) {
	echo 'left';
}
if ( $border == 'no' ){
	echo ' no_border';
}
if ( $custom_classes ){
	echo ' ' . $custom_classes;
} ?>
"<?php
if ( $headline_image_upload ){
	echo ' style="background-image:url(' . $headline_image_upload . ')"';
} ?>>
    <<?php echo $header_type; ?> class="custom-title"<?php
if ( $border_color || $headline_font_color ){
	echo ' style="border-color:' . $border_color . ';
	color:' . $headline_font_color . ';
	text-align: ' . $text_align . '"';
} ?>><?php
if ( $headline_link ) {
	echo '<a href="' . $headline_link . '" title="headline">';
}
echo $headline_text;
if ( $headline_link ) {
	echo '</a>';
}
if ( $small_text ){ ?>
	<p><em><?php echo $small_text; ?></em></p>
<?php
}?>
</ <?php echo $header_type; ?>>
    </div>

<!-- /ヘッドライン -->
<div class="clear-fix"></div>
