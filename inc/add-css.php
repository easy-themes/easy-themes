<?php
/**
 * Custom Theme CSS
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */


require_once get_template_directory() . '/inc/color.php';

use phpColors\Color;

add_action( 'wp_head', 'easythemes_custom_css', 100 );

function easythemes_custom_css() {

	global $easy_themes;

	$output = '';

	/**
	 * Layout settings
	 * @var array
	 */
	$layout_settings = array(
		'.container' => array(
			// 'width'	=> $easy_themes['wrap_colmn_width'] . 'px',
			'width'			=> '100%',
			'max-width'	=> $easy_themes['wrap_colmn_width'] . 'px',
		),
		'.two-column .contents' => array(
			'width'			=> $easy_themes['main_colmn_width'] . '%',
			'max-width'	=> $easy_themes['main_colmn_width'] . '%',
		),
		'.two-column .sidebar' => array(
			'width'			=> $easy_themes['sidebar_width'] . '%',
			'max-width'	=> $easy_themes['sidebar_width'] . '%',
		),
		'.wrap.three-column .three-column.contents' => array(
			'width'			=> $easy_themes['three_main_colmn_width'] . '%',
			'max-width'	=> $easy_themes['three_main_colmn_width'] . '%',
		),
		'.wrap.three-column .sidebar.sidebar-left' => array(
			'width'			=> $easy_themes['three_left_sidebar_width'] . '%',
			'max-width'	=> $easy_themes['three_left_sidebar_width'] . '%',
		),
		'..wrap.three-column .sidebar.sidebar-right' => array(
			'width'			=> $easy_themes['three_right_sidebar_width'] . '%',
			'max-width'	=> $easy_themes['three_right_sidebar_width'] . '%',
		),
		'.header' => array(
			'height'			=> $easy_themes['header_height'] . 'px',
			'max-height'	=> $easy_themes['header_height'] . 'px',
		),
		'.theme-color.theme-color--bg' => array(
			'background-color' => $easy_themes['theme_colorpicker'],
		),
		'.theme-color.theme-color--color' => array(
			'color' => $easy_themes['theme_colorpicker'],
		),
		'.theme-color.theme-color--border' => array(
			'border-color' => $easy_themes['theme_colorpicker'],
		),
		'#wrapper, .widget--title, .widget--block ul li' => array(
			'border-radius' => $easy_themes['theme_radius'],
		),
		'.theme_radius' => array(
			'border-radius' => $easy_themes['theme_radius'] . 'px',
		),
		'#comments ul li h4,#comments #respond #reply-title,#trackback h3' => array(
			'background-color' => $easy_themes['theme_colorpicker'],
			'list-style' => 'none',
		),
	);

	/**
	 * Header settings
	 * @var array
	 */
	$header_settings = array(
		'header.header' => array(
			'height' => $easy_themes['header_height'] . 'px',
		),
	);


	/**
	 * Navbar settings
	 * @var array
	 */
	$nav_color_regular_object = new Color( $easy_themes['navigation_color']['regular'] );
	$nav_color_hover_object   = new Color( $easy_themes['navigation_color']['hover'] );
	$nav_color_active_object  = new Color( $easy_themes['navigation_color']['active'] );

	$navbar_settings = array(
		'.navigation--area' => array(
			'background' => $easy_themes['navigation_color']['regular'],
		),
		'.mobile .navbar' => array(
			'background' => $easy_themes['navigation_color']['regular'] . '',
		),
		'.navbar' => array(
			'background' => $easy_themes['navigation_color']['regular'] . '',
		),
		'.navbar ul.nav li a' => array(
			'background'		=> $easy_themes['navigation_color']['regular'] . '',
			'border-color'	=> '#' . $nav_color_regular_object->darken(),
			'color'					=> $easy_themes['navigation_font']['color'],
		),
		'.navbar ul.nav li a:hover' => array(
			'background' => $easy_themes['navigation_color']['hover'] . '',
			'border-color' => '#' . $nav_color_hover_object->darken() . '',
		),
		'.navbar ul.nav li a:active' => array(
			'background' => $easy_themes['navigation_color']['active'] . '',
			'border-color' => '#' . $nav_color_active_object->darken() . '',
		),
		'.navbar ul.nav li a .description' => array(
			'background' => '#' . $nav_color_regular_object->darken() . '',
		),
		'.navbar ul.nav li a:hover .description' => array(
			'background' => '#' . $nav_color_hover_object->darken() . '',
		),
		'.navbar ul.nav li a:active .description' => array(
			'background' => '#' . $nav_color_active_object->darken() . '',
		),
		'.navbar ul.nav .dropdown-menu' => array(
			'background' => $easy_themes['navigation_dropdown']['regular'],
		),
		'.navbar ul.nav .dropdown-menu:after' => array(
			'border-bottom-color' => $easy_themes['navigation_dropdown']['regular'],
		),
		'.navbar ul.nav .dropdown-menu li' => array(
			'background' => $easy_themes['navigation_dropdown']['regular'],
		),
		'.navbar ul.nav .dropdown-menu li:hover' => array(
			'background' => $easy_themes['navigation_dropdown']['hover'],
		),
		'.navbar ul.nav .dropdown-menu li:active' => array(
			'background' => $easy_themes['navigation_dropdown']['active'],
		)
	);

	/**
	 * Font Setting
	 */

	if ( $easy_themes['theme_colorpicker'] ) {
		$font_link_object = new Color( $easy_themes['theme_colorpicker'] );
	}


	$font_settings = array(
		'body' => array(
			'text-shadow' => '0px 0px ' . $easy_themes['font_shadow'] . 'px rgba(0,0,0,' . ( $easy_themes['font_shadow'] / 10 ) . ')',
		),
		'.widget--block ul li a:hover' => array(
			'background-color' => $easy_themes['font_link']['hover'],
			'color' => ( ( $font_link_object->isDark() ) ? '#FFF' : '#252525' ),
		),
		'.widget--block ul li a:active' => array(
			'background-color' => '#' . $font_link_object->darken( '10' ),
			'color' => ( ( $font_link_object->isDark() ) ? '#FFF' : '#252525' ),
		),
	);

	$settings = array_merge( $navbar_settings, $layout_settings, $header_settings, $font_settings );

	$settings = apply_filters( 'easythemes_add_css_settings', $settings );

	$output = '<style type="text/css">';

	foreach ( $settings as $selector => $setting ) {
		if ( ! $setting ) {
			continue;
		}
		$output .= $selector . ' {';
		if ( is_array( $setting ) ) {
			foreach ( $setting as $property => $value ) {
				$output .= $property . ' : ' . $value . ';';
			}
		}
		$output .= '}';
	}

	$output .= '</style>';

	echo $output;

}

add_filter( 'easythemes_add_css_settings', 'force_theme_color', 10, 1 );

function force_theme_color( $settings ){
	global $easy_themes;

	if ( 'true' !== $easy_themes['theme_color_force'] ) {
		return $settings;
	}

	$tc = $easy_themes['theme_colorpicker'];
	$tco = new color( $easy_themes['theme_colorpicker'] );

	/**
	 * Layout settings
	 * @var array
	 */
	$layout_settings = array(
		'.container' => array(
			// 'width'	=> $easy_themes['wrap_colmn_width'] . 'px',
			'width'			=> '100%',
			'max-width'	=> $easy_themes['wrap_colmn_width'] . 'px',
		),
		'.two-column .contents' => array(
			'width'			=> $easy_themes['main_colmn_width'] . '%',
			'max-width'	=> $easy_themes['main_colmn_width'] . '%',
		),
		'.two-column .sidebar' => array(
			'width'			=> $easy_themes['sidebar_width'] . '%',
			'max-width'	=> $easy_themes['sidebar_width'] . '%',
		),
		'.header' => array(
			'height'			=> $easy_themes['header_height'] . 'px',
			'max-height'	=> $easy_themes['header_height'] . 'px',
		),
		'.theme-color.theme-color--bg' => array(
			'background-color' => $tc,
		),
		'.theme-color.theme-color--color' => array(
			'color' => $tc,
		),
		'.theme-color.theme-color--border' => array(
			'border-color' => $tc,
		),
		'#wrapper, .widget--title, .widget--block ul li' => array(
			'border-radius' => $easy_themes['theme_radius'],
		),
		'.theme_radius' => array(
			'border-radius' => $easy_themes['theme_radius'] . 'px',
		),
	);

	/**
	 * Header settings
	 * @var array
	 */
	$header_settings = array(
		'header.header' => array(
			'height' => $easy_themes['header_height'] . 'px',
		),
	);


	/**
	 * Navbar settings
	 * @var array
	 */


	$navbar_settings = array(
		'.navigation--area' => array(
			'background' => $tc,
		),
		'.mobile .navbar' => array(
			'background' => $tc . '',
		),
		'.navbar' => array(
			'background' => $tc . '',
		),
		'.navbar ul.nav li a' => array(
			'background'		=> $tc . '',
			'border-color'	=> '#' . $tco->darken(),
			'color'					=> ( $tco->isDark() ) ? '#FFF': '#252525',
		),
		'.navbar ul.nav li a:hover' => array(
			'background'		=> '#' . $tco->lighten( '10' ) . '',
			'border-color'	=> '#' . $tco->darken() . '',
			'color'					=> ( $tco->isDark() ) ? '#FFF': '#252525',
		),
		'.navbar ul.nav li a:active' => array(
			'background' => '#' . $tco->darken( '20' ),
			'border-color' => '#' . $tco->darken() . '',
		),
		'.navbar ul.nav li a .description' => array(
			'background' => '#' . $tco->darken() . '',
			'color'      => ( $tco->isDark() ) ? '#FFF': '#252525',
		),
		'.navbar ul.nav li a:hover .description' => array(
			'background' => '#' . $tco->darken() . '',
			'color'      => ( $tco->isDark() ) ? '#FFF': '#252525',
		),
		'.navbar ul.nav li a:active .description' => array(
			'background' => '#' . $tco->darken() . '',
		),
		'.navbar ul.nav .dropdown-menu' => array(
			'background' => $tc,
		),
		'.navbar ul.nav .dropdown-menu:after' => array(
			'border-bottom-color' => $tc,
		),
		'.navbar ul.nav .dropdown-menu li' => array(
			'background' => $tc,
		),
		'.navbar ul.nav .dropdown-menu li:hover' => array(
			'background' => $tc,
		),
		'.navbar ul.nav .dropdown-menu li:active' => array(
			'background' => $tc,
		)
	);

	/**
	 * Font Setting
	 */

	// if ( $easy_themes['font_link']['hover'] ) {
	$font_link_object = new Color( $easy_themes['font_link']['hover'] );
	// }


	$font_settings = array(
		'body' => array(
			'text-shadow' => '0px 0px ' . $easy_themes['font_shadow'] . 'px rgba(0,0,0,' . ( $easy_themes['font_shadow'] / 10 ) . ')',
		),
		'body .header .header-description' => array(
			'background'	=> '#' . $tco->darken( '10' ),
			'color'				=> ( $tco->isDark() ) ? '#FFF': '#252525',
		),
		'.sidebar .widget--block .widget--title,.contents .widget--block .widget--title,.footer .widget--block .widget--title' => array(
			'background'	=> $tc,
			'color'				=> ( $tco->isDark() ) ? '#FFF': '#252525',
		),
		'.widget--block ul li a:hover' => array(
			'background-color'	=> $tc,
			'color'							=> ( ( $tco->isDark() ) ? '#FFF' : '#252525' ),
		),
		'.widget--block ul li a:active' => array(
			'background-color'	=> '#' . $tco->darken( '10' ),
			'color'							=> ( ( $tco->isDark() ) ? '#FFF' : '#252525' ),
		),
		'.contents .entry-title' => array(
			'background-color'	=> $tc,
			'color'							=> ( ( $tco->isDark() ) ? '#FFF' : '#252525' ),
			'border-color'			=> '#' . $tco->darken( '10' ),
		),
		'body .footer .copyright' => array(
			'background-color'	=> '#' . $tco->darken( '10' ),
			'color'							=> ( ( $tco->isDark() ) ? '#FFF' : '#252525' ),
		),
	);

	$settings = array_merge( $navbar_settings, $layout_settings, $header_settings, $font_settings );
	return $settings;
}
