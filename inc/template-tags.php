<?php

/**
 * Theme original template tags.
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */



/**
 * Theme mod
 * @param  string  $key      [description]
 * @param  boolean $echo     [description]
 * @param  boolean $sanitize [description]
 * @return [type]            [description]
 */
function easy_mod( $key = '', $echo = true, $sanitize = true ){

	global $easy_themes;

	$output = '';

	if ( ! $key ) {
		return $output;
	}

	if ( isset( $easy_themes[$key] ) && $easy_themes[$key] ) {
		$output = $easy_themes[$key];
	} else {
		return $output;
	}

	if ( $sanitize ) {
		$output = wp_kses_post( $output );
	} else {
		$output = $output;
	}

	if ( $echo  && $output ) {
		echo $output;
	} else {
		return $output;
	}

}

add_filter( 'get_search_form', 'easythemes_search_form' );

function easythemes_search_form( $form ) {
	ob_start();

	get_template_part( 'modules/searchform' );

	$searchform = ob_get_contents();
	ob_clean();
  return $searchform;
}

