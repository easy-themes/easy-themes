<?php
/**
 * =====================================================
 * @package    easy themes
 * @subpackage Tour
 * @license    http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */

/**
 * Theme Activation Tour
 *
 * This class handles the pointers used in the introduction tour.
 * @package Popup Demo
 *
 */
class WordImpress_Theme_Tour {

	private $pointer_close_id = 'wordimpress_tour'; //value can be cleared to retake tour

	/**
	 * Class constructor.
	 *
	 * If user is on a pre pointer version bounce out.
	 */
	public function __construct()
	{

		global $wp_version;

		if ( version_compare( $wp_version, '3.4', '<' ) ) {
			return false;
		}

		//version is updated ::claps:: proceed
		add_action( 'admin_enqueue_scripts', array( $this, 'tour_enqueue' ) );
	}

	/**
	 * Enqueue styles and scripts needed for the pointers.
	 */
	public function tour_enqueue()
	{

		if ( ! current_user_can( 'manage_options' ) ){
			return;
		}

		// Assume pointer shouldn't be shown
		$enqueue_pointer_script_style = false;

		// Get array list of dismissed pointers for current user and convert it to array
		$dismissed_pointers = explode( ',', get_user_meta( get_current_user_id(), 'dismissed_wp_pointers', true ) );

		// Check if our pointer is not among dismissed ones
		if ( ! in_array( $this->pointer_close_id, $dismissed_pointers ) ) {
			$enqueue_pointer_script_style = true;

				// Add footer scripts using callback function
			add_action( 'admin_print_footer_scripts', array( $this, 'intro_tour' ) );
			add_action( 'wp_footer', array( $this, 'intro_tour' ) );
		}

		// Enqueue pointer CSS and JS files, if needed
		if ( $enqueue_pointer_script_style ) {
			wp_enqueue_style( 'wp-pointer' );
			wp_enqueue_script( 'wp-pointer' );
		}

	}


	/**
	 * Load the introduction tour
	 */
	public function intro_tour()
	{
		$adminpages = array(
			//array name is the unique ID of the screen @see: http://codex.wordpress.org/Function_Reference/get_current_screen
			'themes' => array(
				array(
							'content'		=> '<h3>' . __( '0. ようこそ！Easy Themes へ', 'easythemes' ) . '</h3>'. '<p>' . __( 'このテーマの使い方をツアー形式で体験していただけます。', 'easythemes' ) . '</p>',
							'id'				=> '.theme-actions .customize.load-customize', //ID of element where the pointer will point
							'position'	=> array(
								'edge'			=> 'top',
								'align'			=> 'center',
							),
							'button2'		=> __( '次へ', 'easythemes' ),
							'function'	=> 'window.location="' . admin_url( 'admin.php?page=_options&tab=0&welcome_tour=1' ) . '";',
				),
			),
			'_options' => array(
				array(
					'content' => '<h3>' . __( '1.1 テーマ設定のカスタマイズ', 'easythemes' ) . '</h3>
						<p>' . __( 'テーマの詳細な設定をこのパネルから操作することができます。<br>左のナビゲーションから、項目をクリックし、それぞれの設定を操作してください。', 'easythemes' ) . '</p>',
					'id' => '#1_section_group_li',
					'position' => array(
						'edge' => 'left',
						'align' => 'top',
					),
					'button2' => __( '次へ', 'easythemes' ),
					'function' => 'page_scroller("#info-free_image_import");',
				),
				array(
						'content' => '<h3>' . __( '1.2 画像素材のインポート', 'easythemes' ) . '</h3>'
								. '<p>' . __( '当テーマでは、Web上のフリー素材を WordPress にインポートすることができます。<br>', 'easythemes' ) . '</p>',
						'id' => '#info-free_image_import .button.button-primary',
						'position' => array(
								'edge' => 'bottom',
								'align' => 'bottom',
						),
						'button2' => __( '次へ', 'easythemes' ),
						'function' => 'page_scroller("#info_bar .redux-action_bar");',
				),
				array(
					'content'		=> '<h3>' . __( '1.2 テーマ設定の保存', 'easythemes' ) . '</h3><p>
					' . __( 'テーマ設定編集後は必ず画面上部・下部の「変更を保存」ボタンをクリックしてください。', 'easythemes' ) . '</p>',
					'id'				=> '#info_bar .redux-action_bar',
					'position'	=> array(
						'edge'			=> 'right',
						'align'			=> 'right',
					),
					'button2'		=> __( '次へ', 'easythemes' ),
					'function'	=> 'window.location="' . admin_url( 'widgets.php?welcome_tour=2' ) . '";',
				),
			),
			'widgets' => array(
				array(
					'content' => '<h3>' . __( '2.1 ウィジェットのカスタマイズ', 'easythemes' ) . '</h3>
						<p>' . __( 'ウィジェットは、サイトのパーツを簡単に設置するためのWordPressの機能です。<br>当テーマでは<strong>9つ</strong>のウィジェットエリアを用意しています。<br>', 'easythemes' ) . '</p>',
					'id'				=> '#sidebar-primary',
					'position'	=> array(
						'edge'			=> 'top',
						'align'			=> 'left',
					),
					'button2'		=> __( '次へ', 'easythemes' ),
					'function'	=> 'page_scroller("#wp-pointer-1");',
				),
				array(
					'content' => '<h3>' . __( '2.2 ウィジェットのカスタマイズ', 'easythemes' ) . '</h3>
						<p>' . __( 'ウィジェットは、サイトのパーツを簡単に設置するためのWordPressの機能です。<br> 当テーマでは<strong>9つ</strong>のウィジェットエリアを用意しています。また、独自のウィジェットを組み込んでおりますのでご活用下さい。', 'easythemes' ) . '</p>',
					'id' => '#available-widgets',
					'position' => array(
							'edge' => 'top',
							'align' => 'left',
					),
					'button2' => __( '次へ', 'easythemes' ),
					'function' => 'window.location="' . admin_url( 'nav-menus.php?welcome_tour=3' ) . '";',
				),
			),
			'nav-menus' => array(
				array(
					'content' => '<h3>' . __( '3. ナビゲーションのカスタマイズ', 'easythemes' ) . '</h3>
						<p>' . __( 'ヘッダー部のグローバルナビゲーションは、このページから編集します。左のメニュー一覧からページを選択し、「メニューに追加」ボタンをクリックすることでナビゲーションに追加できます。', 'easythemes' ) . '</p>',
					'id' => '#side-sortables',
					'position' => array(
						'edge' => 'left',
						'align' => 'left',
					),
					'button2' => __( '次へ', 'easythemes' ),
					'function' => 'window.location="' . admin_url( 'post-new.php?welcome_tour=3' ) . '";',
				),
			),
			'post' => array(
				array(
					'content' => '<h3>' . __( '4. 投稿編集ページ', 'easythemes' ) . '</h3>'
						. '<p>' . __( '
						<h2 style="margin-left: 20px">レイアウトブロック機能</h2>
						<p>当テーマでは「レイアウトブロック機能」を使用しての記事投稿が可能です。<br>
						左下の「ブロックを追加」ボタンをクリックすると、使用できるブロックリストのポップアップが出現します。
						<br>
						<br>
						<br>
						<br></p>
						<h2 style="margin-left: 20px">SEO機能</h2>
						<p>各記事にSEOの詳細設定を施すことが可能です。<br>
						空の場合にはデフォルトのサイトの設定が優先されます。
						<h2 style="margin-left: 20px">OGP設定機能</h2>
						<p>Facebook でシェアされる際の画像・タイトル・説明文を設定することが可能です。
						</p>', 'easythemes' ) . '</p>',
					'id' => '#categorydiv',
					'position' => array(
						'edge' => 'right',
						'align' => 'left',
					),
					'button2' => __( '次へ', 'easythemes' ),
					'function' => 'window.location="' . admin_url( 'plugins.php?welcome_tour=7' ) . '";',
				),
			),
			'plugins' => array(
				array(
					'content' => '<h3>' . __( '5. プラグインについて', 'easythemes' ) . '</h3>'
						. '<p>' . __( '
							このテーマはプラグインインストーラーを搭載しています。<br>
							上記プラグインのインストールを始めるからインストールすることで、
							便利なプラグインを一気にインストールすることが可能です。
							', 'easythemes') . '</p>',
					'id' => '#setting-error-tgmpa',
					'position' => array(
						'edge' => 'top',
						'align' => 'left',
					),
					// 'button2' => __('次へ', 'easythemes'),
					'function' => 'window.location="' . admin_url( 'admin.php?page=affiliate_tools' ) . '";',
				),
			),
		);


		$page = '';
		$screen = get_current_screen();

		//Check which page the user is on
		if ( isset( $_GET[ 'page' ] ) ) {
			$page = $_GET[ 'page' ];
		}

		if ( empty( $page ) ) {
			$page = $screen->id;
		}

		$function = '';
		$button2 = '';
		$opt_arr = array();
		if ( ! $adminpages[$page] ){
			return;
		}
		foreach ( $adminpages[$page] as $key => $option ) {
			//Location the pointer points
			if ( ! empty( $adminpages[$page][$key]['id'] ) ) {
				$id[$key] = $adminpages[$page][$key]['id'];
			} else {
				$id[$key] = $screen->id;
			}
			//Options array for pointer used to send to JS
			if ( '' !== $page && in_array( $page, array_keys( $adminpages[$page] ) ) ) {

				$align = ( is_rtl() ) ? 'right' : 'left';
				$opt_arr[$key] = array(
					'content' => $adminpages[$page][$key]['content'],
					'position' => array(
							'edge' => ( ! empty( $adminpages[$page][$key]['position']['edge'] ) ) ? $adminpages[$page][$key]['position']['edge'] : 'left',
							'align' => ( ! empty( $adminpages[$page][$key]['position']['align'] ) ) ? $adminpages[$page][$key]['position']['align'] : $align,
					),
					'pointerWidth' => 500,
				 );
				if ( isset( $adminpages[$page][$key]['button2'] ) ) {
					$button2[$key] = ( ! empty( $adminpages[$page][$key]['button2'] ) ) ? $adminpages[$page][$key]['button2'] : '';
				}
				if ( isset( $adminpages[$page][$key]['function'] ) ) {
					$function[$key] = $adminpages[$page][$key]['function'];
				}
			}
		}
		$this->print_scripts( $id , $opt_arr, __( 'ツアーを終了' , 'easythemes' ), $button2, $function );
	}


	/**
	 * Prints the pointer script
	 *
	 * @param string $selector The CSS selector the pointer is attached to.
	 * @param array $options The options for the pointer.
	 * @param string $button1 Text for button 1
	 * @param string|bool $button2 Text for button 2 (or false to not show it, defaults to false)
	 * @param string $button2_function The JavaScript function to attach to button 2
	 * @param string $button1_function The JavaScript function to attach to button 1
	 */
	public function print_scripts( $selectors, $options, $button1, $button2 = array(), $button2_function = '', $button1_function = '' )
	{
		foreach ( $selectors as $key => $selector) { ?>
			<script type="text/javascript">
					//<![CDATA[
					(function ($) {

							var wordimpress_pointer_options_<?php echo $key ?> = <?php echo json_encode( $options[$key] ); ?>, setup_<?php echo $key ?>;
							//Userful info here
							wordimpress_pointer_options_<?php echo $key ?> = $.extend(wordimpress_pointer_options_<?php echo $key ?>, {
									buttons: function (event, t) {
											button = jQuery('<a id="pointer-close_<?php echo $key;?>" style="margin-left:5px" class="button-secondary">' + '<?php echo $button1; ?>' + '</a>');
											button.bind('click.pointer', function () {
													t.element.pointer('close');
											});
											return button;
									}
							});
							page_scroller = function( element ){
								var target = $(element).offset().top;
								$("html, body").animate({scrollTop:target}, 500);
								event.preventDefault();
								return false;
							}

							setup_<?php echo $key ?> = function () {
									$('<?php echo $selector; ?>').pointer(wordimpress_pointer_options_<?php echo $key ?>).pointer('open');
									<?php
									if ( $button2[$key] ) { ?>
									jQuery('#pointer-close_<?php echo $key;?>').after('<a id="pointer-primary_<?php echo $key;?>" class="button-primary">' + '<?php echo $button2[$key]; ?>' + '</a>');
									<?php } ?>
									jQuery('#pointer-primary_<?php echo $key;?>').click(function () {
											<?php echo $button2_function[$key]; ?>
									});
									jQuery('#pointer-close_<?php echo $key;?>').click(function () {
											<?php if ( $button1_function[$key] == '' ) { ?>
											$.post(ajaxurl, {
													pointer: '<?php echo $this->pointer_close_id; ?>', // pointer ID
													action: 'dismiss-wp-pointer'
											});
											<?php } else { ?>
											<?php echo $button1_function[$key]; ?>
											<?php } ?>
									});
							};
							if (wordimpress_pointer_options_<?php echo $key ?>.position && wordimpress_pointer_options_<?php echo $key ?>.position.defer_loading) {
									$(window).bind('load.wp-pointers', setup_<?php echo $key ?>);
							} else {
									$(document).ready(setup_<?php echo $key ?>);
							}

					})(jQuery);
					//]]>
			</script>
	<?php
		}
	}
}

$wordimpress_theme_tour = new WordImpress_Theme_Tour();

add_filter( 'admin_init', 'default_tour_start', 10, 1 );
function default_tour_start(){
	if ( ! isset( $_GET['default_tour_start'] ) ) {
		return;
	}
	$flg = $_GET['default_tour_start'];
	if ( 'on' !== $flg && is_admin() ) {
		return;
	}
	$meta = get_user_meta( get_current_user_id(), 'dismissed_wp_pointers', true );
	$dismissed_wp_pointers = isset( $meta ) ? explode(',' , $meta ) : array() ;

	if ( in_array( 'wordimpress_tour', $dismissed_wp_pointers ) ) {

		foreach ( $dismissed_wp_pointers as $key => $value ) {
			if ( $value !== 'wordimpress_tour' ) {
				$new_dismissed_wp_pointers[] = $value;
			}
		}

		update_usermeta( get_current_user_id() , 'dismissed_wp_pointers', implode(',', $new_dismissed_wp_pointers) );
	}

}
