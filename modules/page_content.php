<?php
/**
 * Repeater Field - theme module
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
global $page_layout, $global_layout, $post;
$page_layout = json_decode( SmartMetaBox::get( 'content-layout', true , $post->ID ) );
$sidebar_layout = SmartMetaBox::get( 'sidebar_layout' );
$show_page_title_text = SmartMetaBox::get( 'show_page_title_text' );
$content_count = null;
$row_width = 0;

if ( $page_layout ){

	foreach ( $page_layout as $page_content ) {

		switch ( $page_content->type ) {

			/**
			 * case "wordpress"
			 */
			case 'wordpress':
				$show_page_title = isset( $page_content->show_page_title ) ? $page_content->show_page_title : '';

				include get_template_directory() . '/inc/includes/wordpress.php';

				if ( $row_width > 1 ){
					$row_width = 0;
				} else if ( $row_width > .85 ) {
					echo '<div class="clearfix"></div>';
					$row_width = 0;}
			break;

			/**
			 * case "blog"
			 */
			case 'blog' :
				$blog_cat        = isset( $page_content->category ) ? $page_content->category : '';
				$number_of_posts = isset( $page_content->posts )    ? $page_content->posts : '';
				$posts_per_row   = isset( $page_content->posts_per_row ) ? $page_content->posts_per_row : '';

				if ( $number_of_posts == ''
						 || $number_of_posts == NULL ) {
					$number_of_posts = $posts_per_row;
				}

				$show_text        = isset( $page_content->show_text )      ? $page_content->show_text : '';
				$title_location   = isset( $page_content->title )          ? $page_content->title : '';
				$show_image       = isset( $page_content->show_image )     ? $page_content->show_image : '';
				$image_location   = isset( $page_content->image_location ) ? $page_content->image_location : '';
				$show_bar         = isset( $page_content->show_bar )       ? $page_content->show_bar : '';
				$meta             = isset( $page_content->meta )           ? $page_content->meta : '';
				$show_border      = isset( $page_content->show_border )    ? $page_content->show_border : '';
				$custom_classes   = isset( $page_content->custom_classes ) ? $page_content->custom_classes : '' ;
				$b_header_type    = isset( $page_content->b_header_type )  ? $page_content->b_header_type : '' ;

				if ( $b_header_type == '' || $b_header_type == NULL ) {
					$b_header_type = 'h4';
				}

				include get_template_directory() . '/inc/includes/blog_posts.php';

				if ( $row_width > 1 ){
					$row_width = 0;
				} else if ( $row_width > .85 ) {
					echo '<div class="clearfix"></div>';
					$row_width = 0;
				}

			break;

			/**
			 * case "headline"
			 */
			case 'headline' :

				$header_type = isset( $page_content->header_type ) ? $page_content->header_type : '' ;

				if ( $page_content->headline_text !== '' ) {
					$headline_text = $page_content->headline_text;
				} else {
					$headline_text = get_the_title();
				}

				$headline_link         = isset( $page_content->headline_link )         ? $page_content->headline_link : '';
				$small_text            = isset( $page_content->small_text )            ? $page_content->small_text : '';
				$text_align            = isset( $page_content->text_align )            ? $page_content->text_align : '';
				$border                = isset( $page_content->border )                ? $page_content->border : '';
				$border_color          = isset( $page_content->border_color )          ? $page_content->border_color : '';
				$headline_image_upload = isset( $page_content->headline_image_upload ) ? $page_content->headline_image_upload : '';
				$headline_font_color   = isset( $page_content->headline_font_color )   ? $page_content->headline_font_color : '';
				$custom_classes        = isset( $page_content->custom_classes )        ? $page_content->custom_classes : '';

				include get_template_directory() . '/inc/includes/headline.php';

				if ( $row_width > 1 ){
					$row_width = 0;
				} else if ( $row_width > .85 ) {
					echo '<div class="clearfix"></div>';
					$row_width = 0;
				}
			break;

			/**
			 * case "divider"
			 */
			case 'divider':

				$divider_type           = isset( $page_content->divider_type )   ? $page_content->divider_type   : '';
				$divider_color          = isset( $page_content->divider_color )  ? $page_content->divider_color  : '';
				$divider_type           = isset( $page_content->divider_type )   ? $page_content->divider_type   : '';
				$divider_height         = isset( $page_content->divider_height ) ? $page_content->divider_height : '';
				$divider_custom_classes = isset( $page_content->divider_custom_classes ) ? $page_content->divider_custom_classes : '' ;

				include get_template_directory() . '/inc/includes/divider.php';

				if ( $row_width > 1 ){
					$row_width = 0;
				} else if ( $row_width > .85 ) {
					echo '<div class="clearfix"></div>';
					$row_width = 0;
				}
			break;

			/**
			 * case "social"
			 */
			case 'social':

				include get_template_directory() . '/inc/includes/social.php';

			break;

			/**
			 * case "breadcrumb"
			 */
			case 'breadcrumb':

				include get_template_directory() . '/inc/includes/breadcrumb.php';

			break;

			/**
			 * case "signup"
			 */
			case 'signup':

				$signup_image_upload        = isset( $page_content->signup_image_upload )        ? $page_content->signup_image_upload : '';
				$signup_text                = isset( $page_content->signup_text )                ? $page_content->signup_text : '';
				$signup_button_text         = isset( $page_content->signup_button_text )         ? $page_content->signup_button_text : '';
				$signup_button_url          = isset( $page_content->signup_button_url )          ? $page_content->signup_button_url : '';
				$signup_button_color_top    = isset( $page_content->signup_button_color_top )    ? $page_content->signup_button_color_top : '';
				$signup_button_color_bottom = isset( $page_content->signup_button_color_bottom ) ? $page_content->signup_button_color_bottom : '';
				$signup_custom_classes      = isset( $page_content->signup_custom_classes )      ? $page_content->signup_custom_classes : '' ;
				include get_template_directory() . '/inc/includes/signup.php';

				if ( $row_width > 1 ){
					$row_width = 0;
				} else if ( $row_width > .85 ) {
					echo '<div class="clearfix"></div>';
					$row_width = 0;
				}

			break;

			/**
			 * case comment
			 */
			case 'comments':

				include get_template_directory() . '/inc/includes/comment.php';

			break;

			/**
			 * case image
			 */
			case 'image_block':

				$image_image_upload  = isset( $page_content->image_image_upload ) ? $page_content->image_image_upload : '';
				$image_link          = isset( $page_content->image_link )         ? $page_content->image_link : '';
				$custom_classes      = isset( $page_content->custom_classes )     ? $page_content->custom_classes : '';

				include get_template_directory() . '/inc/includes/custom.php';

				if ( $row_width > 1 ){
					$row_width = 0;
				} else if ( $row_width > .85 ) {
					echo '<div class="clearfix"></div>';
					$row_width = 0;
				}

			break;

			/**
			 * contact
			 */
			case 'contact':

				$contact_title             = isset( $page_content->contact_title )             ? $page_content->contact_title : '';
				$title_type                = isset( $page_content->contact_title_type )        ? $page_content->contact_title_type : '';
				$contact_submit_text_color = isset( $page_content->contact_submit_text_color ) ? $page_content->contact_submit_text_color : '';
				$contact_submit_text       = isset( $page_content->contact_submit_text )       ? $page_content->contact_submit_text : '';
				$custom_classes = isset( $page_content->custom_classes ) ? $page_content->custom_classes : '';

				include get_template_directory() . '/inc/includes/contact.php';

				if ( $row_width > 1 ) {
					$row_width = 0;
				} else if ( $row_width > .85 ){
					echo '<div class="clearfix"></div>';
					$row_width = 0;
				}

			break;


			/**
			 * case "video"
			 */
			case 'video_embed':

				$video_code     = isset( $page_content->video_embed_code ) ? $page_content->video_embed_code : '' ;
				$video_align    = isset( $page_content->video_align )      ? $page_content->video_align : '' ;
				$video_width     = isset( $page_content->video_width )       ? $page_content->video_width : '' ;
				$video_height     = isset( $page_content->video_height )       ? $page_content->video_height : '' ;
				$custom_classes = isset( $page_content->custom_classes )   ? $page_content->custom_classes : '' ;

				include get_template_directory() . '/inc/includes/video_embed.php';

			break;

			/**
			 * case "half"
			 */
			case 'half_block':

				$title             = isset( $page_content->half_title ) ? $page_content->half_title : '' ;
				$title_type        = isset( $page_content->half_title_type ) ? $page_content->half_title_type : '' ;
				$half_link         = isset( $page_content->half_link ) ? $page_content->half_link : '' ;
				$half_image_upload = isset( $page_content->half_image_upload ) ? $page_content->half_image_upload : '' ;
				$half_text         = isset( $page_content->half_text ) ? $page_content->half_text : '' ;
				$custom_classes    = isset( $page_content->custom_classes ) ? $page_content->custom_classes : '' ;

				include get_template_directory() . '/inc/includes/half_block.php';

			break;

			/**
			 * case "squeeze"
			 */
			case 'squeeze':

				$warnig_text                = isset( $page_content->squeeze_warning_text ) ? $page_content->squeeze_warning_text : '' ;
				$optin_type                 = isset( $page_content->squeeze_optin_type ) ? $page_content->squeeze_optin_type : '' ;
				$optin_code                 = isset( $page_content->squeeze_optin_code ) ? $page_content->squeeze_optin_code : '' ;
				$squeeze_title_text         = isset( $page_content->squeeze_title_text ) ? $page_content->squeeze_title_text : '' ;
				$squeeze_text               = isset( $page_content->squeeze_text ) ? $page_content->squeeze_text : '' ;
				$squeeze_header_footer_show = isset( $page_content->squeeze_header_footer_show ) ? $page_content->squeeze_header_footer_show : '' ;
				$squeeze_design_type        = isset( $page_content->squeeze_design_type ) ? $page_content->squeeze_design_type : '' ;
				$squeeze_button_text        = isset( $page_content->squeeze_button_text ) ? $page_content->squeeze_button_text : '' ;
				$squeeze_button_text_color  = isset( $page_content->squeeze_button_text_color ) ? $page_content->squeeze_button_text_color : '' ;
				$squeeze_button_color       = isset( $page_content->squeeze_button_color ) ? $page_content->squeeze_button_color : '' ;
				$squeeze_privacy_text       = isset( $page_content->squeeze_privacy_text ) ? $page_content->squeeze_privacy_text : '' ;
				$squeeze_image_upload       = isset( $page_content->squeeze_image_upload ) ? $page_content->squeeze_image_upload : '' ;
				$custom_classes             = isset( $page_content->custom_classes ) ? $page_content->custom_classes : '' ;

				include get_template_directory() . '/inc/includes/squeeze.php';

			break;

			/**
			 * case template
			 */
			case 'template':

				$template_type        = isset( $page_content->template_type ) ? $page_content->template_type : '' ;
				$template_text_color  = isset( $page_content->template_text_color ) ? $page_content->template_text_color : '' ;
				$template_bg_color    = isset( $page_content->template_bg_color ) ? $page_content->template_bg_color : '' ;
				$custom_classes       = isset( $page_content->custom_classes ) ? $page_content->custom_classes : '' ;

				include get_template_directory() . '/inc/includes/template.php';

			break;

			// 画像選択
			case 'choose_img':
				$choose_img_type = isset( $page_content->choose_img_type ) ? $page_content->choose_img_type : '' ;
				$choose_img_align = isset( $page_content->choose_img_align ) ? $page_content->choose_img_align : '' ;
				$choose_img_text = isset( $page_content->choose_img_text ) ? $page_content->choose_img_text : '' ;
				$choose_img_classes = isset( $page_content->choose_img_classes ) ? $page_content->choose_img_classes : '' ;
				include get_template_directory() . '/inc/includes/choose_img.php';
			break;

			// デフォルト
			default:

				echo isset( $page_content->type ) ? $page_content->type : ''  . 'ブロックでエラーが起きています。';

			break;

		}

		$content_count++;
	}
} else {
	$show_page_title = isset( $page_layout->show_page_title );
	include get_template_directory() . '/inc/includes/wordpress.php';
}
