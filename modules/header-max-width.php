<?php
/**
 * header-max-width.php - theme module
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
global $easythemes_layout, $easythemes; ?>
<header class="header">
	<div class="header-description">
		<div class="container">
			<span><?php easy_mod( 'header_description' ); ?></span>
		</div>
	</div>
	<div class="container">
		<h1 id="logo" class="header_logo_text"><?php easy_mod( 'header_logo_text' ); ?></h1>
	</div>
</header>
<div class="navigation--area navbar">
	<div class="container">
		<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse">
			<?php
			wp_nav_menu(
				array(
					'theme_location'  => 'navi_menu',
					'menu_class'      => 'nav navbar-nav',
					'container' => false,
					'fallback_cb'     => 'EasyThemesNavWalker::fallback',
					'walker'          => new EasyThemesNavWalker(),
				)
			); ?>
		</div>
	</div>
</div>
<div class="container">
		<?php
switch ( convert_option( 'infobar_display' ) )  {
	case '新着情報' : ?>
			<div class="info-bar">
				<div class="inner">
					<p><span class="red b">新着情報</span>： <?php echo $data['info_bar_news']; ?></p>
				</div>
			</div>
			<?php
			break;

	case '表示しない':
		break;
	} ?>
	</div>
</div>

	<div id="wrapper" class="container">
		<div class="wrap <?php echo $easythemes_layout->get_main_class(); ?>">
			<div class="row-fluid">
