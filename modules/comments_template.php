<?php

if ( have_comments() ) { ?>
	<ul>
	<?php
	wp_list_comments( 'callback=easy_theme_comment&type=comment' ); ?>
	</ul>

	<?php
	if ( ! empty( $comments_by_type['pings'] ) ) { ?>
	<ul>
		<?php
		wp_list_comments( 'callback=easy_theme_comment&type=pings' ); ?>
	</ul>
	<?php
	} ?>

	<?php
} ?>
<?php

$fields = array(
	'author' => '<p>' . '<label for="author">'.'ハンドルネーム'.( isset( $req ) ? '<span class="required">*</span>' : '' ) .'</label>'. '<input id="author" name="author" type="text" value="' . esc_attr( ( isset( $commenter['comment_author'] ) ) ? $commenter['comment_author'] : '' ) . '" size="30"' . ( isset( $aria_req ) ? $aria_req : '' ) . ' /></p>',
	'email'  => '<p class="comment-form-email">' . '<label for="email">E-mail'.( isset( $req ) ? '<span class="required">*</span>' : '' ) .'</label> ' . '<input id="email" name="email" type="text" value="' .  esc_attr( ( isset( $commenter['comment_author_email'] ) ) ? $commenter['comment_author_email'] : '' ) . '" size="30"' . ( isset( $aria_req ) ? $aria_req : '' )  . ' /></p>',
	'url'    => '<p class="comment-form-url"><label for="url">' . 'WEBサイト' . '</label>' . '' . '<input id="url" name="url" type="text" value="' . esc_attr( ( isset( $commenter['comment_author_url'] ) ) ? $commenter['comment_author_url'] : '' ) . '" size="30" />' . '</p>',
	'title'  => '<p><label>タイトル</label><input id="comment-title" name="comment-title" type="TEXT" size="0" maxlength="40"></p>',
);
$defaults = array(
	'fields'               => apply_filters( 'comment_form_default_fields', $fields ),
	'comment_field'        => '<p class="comment-form-comment"><label>コメント欄</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>',
	'comment_notes_before' => '' . ' <!-- メールアドレス非公開 --> ' . ( isset( $req ) ? ( isset( $required_text ) ? $required_text : '' ): '' ) . '',
	'comment_notes_after'  => '' . '<!-- HTMLタグ無効 -->' . '',
	'title_reply'          => 'コメントを残す',
	'title_reply_to'       => '%s にコメントを書く',
	'cancel_reply_link'    => 'キャンセル',
	'label_submit'         => '>> コメント送信' ,
);
?>
<?php
comment_form( $defaults ); ?>
<?php
if ( pings_open() ) : ?>
	<div id="trackback">
	<h3>トラックバックURL</h3>
	<input type="text" name="tburl" id="tburl" value="<?php trackback_url(); ?>" size="80" tabindex="1" />
	</div><!-- /#trackback -->
	<?php
endif; ?>
