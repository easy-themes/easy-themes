<?php
/**
 * Fotoer - theme module.
 * =====================================================
 *@package  Easy Themes
 *@license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */ ?>

		</div>
	</div>
</div>

<?php
if ( easy_themes_setting( 'page-top', false, false ) == 1 ) { ?>
	<p id="page-jump">
		<a href="#wrapper"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pagetop.png" alt="ページ上部に" class="linkimg png_bg" /></a>
	</p>
<?php
}?>

<div class="footer">
	<div class="container">
		<div class="row-fluid footer--area">
			<div class="span6 footer--column footer--column01 clearfix">
	<?php
	if ( ! dynamic_sidebar( 'footer-one' ) ) : ?>
		<div class="widget">
			<h2 class="widget--title">運営者プロフィール</h2>
			<a href="#" target="_self" class="widget_sp_image-image-link" title="運営者プロフィール">
				<img alt="運営者プロフィール" class="aligncenter" src="<?php bloginfo('template_url');?>/assets/images/default-about.jpg">
			</a>
			<div class="description">
				<p>ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。ここにテキストが入ります。</p>
			</div>
			<a href="#" class="button">プロフィール詳細はこちら</a>
		</div>
				<?php endif; ?>

			</div>
			<div class="span6 footer--column footer--column02 clearfix">

	<?php
	if ( dynamic_sidebar( 'footer-two' ) ) :
		// the_widget( 'WP_Widget_Pages' );
	endif; ?>

			</div>

			<div class="span6 footer--column footer--column03 clearfix">

	<?php
	if ( ! dynamic_sidebar( 'footer-three' ) ) :
		// the_widget( 'WP_Widget_Recent_Posts' );
	endif; ?>

			</div>

			<div class="span6 footer--column footer--column04 clearfix">

	<?php
	if ( dynamic_sidebar( 'footer-four' ) ) {
		// the_widget( 'WP_Widget_Categories' );
	}
	?>

			</div>
		</div>

		<div class="copyright">
			<p><?php
			echo easy_themes_setting( 'footer_copyright', false, true ) ?></p>
		</div>
	</div>
	</div>
</div>
<?php wp_footer();?>
<script type='text/javascript'>//<![CDATA[
document.write("<script async src='//HOST:3000/browser-sync-client.1.1.2.js'><\/script>".replace(/HOST/g, location.hostname));
//]]></script>
</body>
</html>
