<?php
/**
 * Head - theme module
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
?>
<!doctype html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta name="mobile-web-app-capable" content="yes">
<meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
<?php wp_head(); ?>
</head>
<?php
$mobile_class = ( easythemes_is_mobile() ) ? 'mobile' : 'pc';
?>
<body <?php body_class( $mobile_class );?>">
