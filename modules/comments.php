<?php
/**
 * Comments - theme module.
 * =====================================================
 *@package  Easy Themes
 *@license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */ ?>

<div class="clearfix"></div>

<div id="comments">
	<?php
	get_template_part( 'modules/comments_template' );
	?>
</div>
<div class="clearfix"></div>
