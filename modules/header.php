<?php
/**
 * header.php - theme module
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
global $easythemes_layout, $easythemes;

?>
<header class="header container">
		<div class="row-fluid">
			<h1 class="header-description">
				<span><?php easy_themes_setting( 'header_description' ); ?></span>
			</h1>
			<p id="logo" class="header_logo_text"><?php easy_themes_setting( 'header_logo_text' ); ?></p>
		</div>
</header>
<div class="container navigation--area">
	<div class="row-fluid navbar">
		<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse">
			<?php
			wp_nav_menu(
				array(
					'theme_location'  => 'navi_menu',
					'menu_class'      => 'nav navbar-nav',
					'container' => false,
					'fallback_cb'     => 'EasyThemesNavWalker::fallback',
					'walker'          => new EasyThemesNavWalker(),
				)
			); ?>
		</div>
	</div>
<!-- /ナビゲーション -->
</div>

<div id="wrapper" class="container">
	<div class="wrap <?php echo $easythemes_layout->get_main_class(); ?>">
		<div class="row-fluid">
