<?php
/**
 * For Top Page Sidebar
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */

global $easythemes_layout;

if ( $easythemes_layout->get_sidebar_setting() ) : ?>
<div class="sidebar <?php echo $easythemes_layout->get_sidebar_class(); ?>">

<?php
dynamic_sidebar( 'sidebar-primary' ); ?>

</div>

<?php
endif;
