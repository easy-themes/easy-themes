<?php
/**
 * Search form - theme module
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
?>

<form class="searchform" action="<?php echo site_url( '/' ); ?>/" method="get">
	<div class="form-search">
	<input name="s" class="input-long search-query" type="text" />
		<input class="btn btn-default brn-" value="検索する" type="submit" />
	</div>
</form>
