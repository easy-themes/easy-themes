<?php
/**
 * 右サイドバー用モジュール
 * ===================================
 * @package easy themes
 *
 */
global $easythemes_layout;

if ( $easythemes_layout->get_sidebar_right_setting() ) : ?>

	<div class="sidebar <?php echo $easythemes_layout->get_sidebar_right_class(); ?>">

	<?php
	dynamic_sidebar( 'sidebar-right' ) ?>

	</div>

<?php
endif;

