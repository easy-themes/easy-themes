<?php

/**
 * Home template.
 * =====================================================
 * @package  Easy Themes
 * @license  http://creativecommons.org/licenses/by/2.1/jp/
 * =====================================================
 */
global $easy_themes,$easythemes_layout;

get_template_part( 'modules/head' );

easythemes_header_path();

do_action( 'get_header' );

dynamic_sidebar( 'main-visual' );

get_template_part( 'modules/sidebar' ); ?>

<div class="contents <?php echo $easythemes_layout->get_main_class(); ?>">
	<h1>404 Not Found</h1>
	<h2>ページが見つかりませんでした。</h2>
	<?php
	get_search_form(); ?>
	<!-- /コンテンツ -->
</div>

<?php

get_template_part( 'modules/sidebar-right' );

easythemes_footer_path();
